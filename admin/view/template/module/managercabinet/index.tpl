<?php echo $header; ?>
<link type="text/css" rel="stylesheet" href="view/managercabinet/stylesheet/style.css" />
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
var text = false;
var token = '<?php echo $token; ?>';
var loading = '<img src="view/managercabinet/image/loading.gif" />';
--></script>
<div id="content">
 <div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
 </div>
 <div class="box">
  <div class="heading">
   <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
   <div class="buttons">
    <?php if ($this->user->getUserGroupId() == 1){ ?>
    <a class="button" onclick="location = '<?php echo $setting_link; ?>';"><?php echo $button_setting; ?></a>
    <a class="button" onclick="clearCache();"><?php echo $button_clear_cache; ?></a>
    <?php } ?>
    <a class="button" onclick="location = '<?php echo $cancel; ?>';"><?php echo $button_cancel; ?></a>
   </div>
  </div>
  <div class="content">
  <table cellpadding="0" cellspacing="0">
      <tr>
          <td width="35%" style="vertical-align: top;">
              <div id="tabs" class="htabs">
                  <a href="#tab-filter"><?php echo $button_filter; ?></a>
              </div>
              <div id="tab-filter">
                  <?php include ('filter.tpl'); ?></div>
              </div>
          </td>
      </tr>
      <tr>
          <td style="vertical-align: top;">
              <form id="product_container" style="display:none;"></form>
          </td>
      </tr>
  </table>
 </div>
 <div class="go_up" onclick="javascript:window.scrollTo(0, 0);" title="<?php echo $text_up; ?>"></div>
</div>
<script type="text/javascript"><!--
var attribute_row = 0;
var option_row = 0;
var option_value_row = 0;
var special_row = 0;
var discount_row = 0;
var image_row = 0;

function addOptionValue(form, option_row, store_id) {
	html  = '<tbody id="option-value-row-' + form + '-' + option_value_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><select name="product_options[' + option_row + '][product_option_value][' + option_value_row + '][option_value_id]">';
	html += $('#option-values-' + form + option_row).html();
	html += '</select><input type="hidden" name="product_options[' + option_row + '][product_option_value][' + option_value_row + '][product_option_value_id]" value="" /></td>';
	html += '    <td class="right"><input type="text" name="product_options[' + option_row + '][product_option_value][' + option_value_row + '][quantity]" value="" size="3" /></td>';
	html += '    <td class="left"><select name="product_options[' + option_row + '][product_option_value][' + option_value_row + '][subtract]">';
	html += '      <option value="1"><?php echo $text_yes; ?></option>';
	html += '      <option value="0"><?php echo $text_no; ?></option>';
	html += '    </select></td>';
	html += '    <td class="right"><select name="product_options[' + option_row + '][product_option_value][' + option_value_row + '][price_prefix]">';
	html += '      <option value="+">+</option>';
	html += '      <option value="-">-</option>';
	html += '    </select>';
	html += '    <input type="text" name="product_options[' + option_row + '][product_option_value][' + option_value_row + '][price]" value="" size="5" /></td>';
	html += '    <td class="right"><select name="product_options[' + option_row + '][product_option_value][' + option_value_row + '][points_prefix]">';
	html += '      <option value="+">+</option>';
	html += '      <option value="-">-</option>';
	html += '    </select>';
	html += '    <input type="text" name="product_options[' + option_row + '][product_option_value][' + option_value_row + '][points]" value="" size="5" /></td>';
	html += '    <td class="right"><select name="product_options[' + option_row + '][product_option_value][' + option_value_row + '][weight_prefix]">';
	html += '      <option value="+">+</option>';
	html += '      <option value="-">-</option>';
	html += '    </select>';
	html += '    <input type="text" name="product_options[' + option_row + '][product_option_value][' + option_value_row + '][weight]" value="" size="5" /></td>';
	html += '    <td class="left"><a onclick="$(\'#option-value-row-' + form + '-' + option_value_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#' + form + ' #option-value-' + form + '-' + option_row + ' tfoot').before(html);
	
	option_value_row++;
}

function toggle(id) {
    $('.dd_menu .dd_menu_container').removeClass('dd_menu_shadow').hide('low');
    $('.dd_menu_title').removeClass('dd_menu_shadow');
    $('#form_list').remove();
    $('#dialog').remove();

    if ($('#' + id + ' .dd_menu_container').css('display') == 'none') {
        $('#' + id + ' .dd_menu_container').addClass('dd_menu_shadow').show('fast');
        $('#' + id + ' .dd_menu_title').addClass('dd_menu_shadow');
    } else {
        $('#' + id + ' .dd_menu_container').removeClass('dd_menu_shadow').hide('low');
        $('#' + id + ' .dd_menu_title').removeClass('dd_menu_shadow');
    }
}

function addCKEDITOR() {
	<?php foreach ($languages as $language) { ?>
	CKEDITOR.replace('description_edit<?php echo $language["language_id"]; ?>', {
		filebrowserBrowseUrl:      'index.php?route=common/filemanager&token=' + token,
		filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=' + token,
		filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=' + token,
		filebrowserUploadUrl:      'index.php?route=common/filemanager&token=' + token,
		filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=' + token,
		filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=' + token
	});
	<?php } ?>
}

function delCKEDITOR() {
	<?php foreach ($languages as $language) { ?>
	<!--<?php $ckeditor = 'CKEDITOR.instances.description_edit' . $language['language_id']; ?>-->
	if (<?php echo $ckeditor; ?>) {
		<?php echo $ckeditor; ?>.destroy();
	}
	<?php } ?>
}

function clearCache() {
	$.ajax({
		type: 'get',
		dataType: 'json',
		url: 'index.php?route=module/managercabinet/clearCache&token=' + token,
		success: function(message) {
			creatMessage(message);
		}
	});
}

function getProducts(data) {
	$('#product_container').fadeOut('fast');
	
	$.post('index.php?route=module/managercabinet/getProducts&token=' + token, getFilterUrl() + data, function(html) {
		$('#product_container').html(html).fadeIn('low');
	});
}

function getProductList(id, path) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog"></div>');
	
	$('#dialog').dialog({
		height: 'auto',
		width:  '90%',
		modal: true,
		bgiframe: true,
		resizable: true,
		autoOpen: false
	});
	
	$.ajax({
		type: 'post',
		data: 'path=' + path + '&product_id=' + id,
		url: 'index.php?route=module/managercabinet/getProductList&token=' + token,
		success: function(data) {
			$('#dialog').html(data).dialog('open');
		}
	});
}

function editProducts(path, action) {
	if (path == 'delete') {
		if (!confirm('<?php echo $button_remove; ?>?')) {
			return false;
		}
	}
	
	var url = 'index.php?route=module/managercabinet/editProducts&token=' + token;
	
	if (path == 'related_to') {
		var data = 'path=related&action=' + path + '&product_related[]=' + action + '&' + $('#product_container').serialize();
	} else {
		var data = 'path=' + path + '&action=' + action + '&' + $('#form-' + path).serialize() + '&' + $('#product_container').serialize();
	}
	
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: data,
		url: url,
		beforeSend: function() {
			$('#overlay').fadeIn(700);
		},
		success: function(message) {
			if (message['success']) {
				if (path != 'related_to') {
					getProducts('');
				}
				
				creatMessage(message);
			} else {
				creatMessage(message);
			}
		}
	});
}

function editProductList(product_id, path, action) {
	if (path == 'descriptions') {
		<?php foreach ($languages as $language) { ?>
		<!--<?php $ckeditor = "CKEDITOR.instances.description_edit" . $language['language_id'] . ".getData()"; ?>-->
		$('#description<?php echo $language["language_id"]; ?>').html(<?php echo $ckeditor; ?>);
		<?php } ?>
	}
	
	$.ajax({
		type: 'post',
		dataType: 'json',
		url: 'index.php?route=module/managercabinet/editProducts&token=' + token,
		data: 'path=' + path + '&action=' + action + '&selected[]=' + product_id + '&' + $('#form_list').serialize(),
		beforeSend: function() {
			$('#overlay').fadeIn(700);
		},
		success: function(message) {
			creatMessage(message);
		}
	});
}

function getFilterUrl() {
	url = 'index.php?route=module/managercabinet/getProducts&token=' + token;
	
	if ($('input[name=\'filter_keyword\']').val() != '') {
		url += '&' + $("#filter_keyword").serialize();
	}
	
	if (!$('input[name*=\'filter_search_in\']:checked').val() && $('input[name=\'filter_keyword\']').val()) {
		url += '&filter_search_in[name]=1';
		$('input[name=\'filter_search_in[name]\']').attr('checked', 'checked');
	}
	
	var limit = $('select[name=\'limit\']').val();
	if (limit != '' && limit != 20) { url += '&limit=' + limit; }
	
	var filter_status = $('select[name=\'filter_status\']').val();
	if (filter_status && filter_status != '*') { url += '&filter_status=' + filter_status; }
	
	var filter_subtract = $('select[name=\'filter_subtract\']').val();
	if (filter_subtract && filter_subtract != '*') { url += '&filter_subtract=' + filter_subtract; }
	
	var filter_shipping = $('select[name=\'filter_shipping\']').val();
	if (filter_shipping && filter_shipping != '*') { url += '&filter_shipping=' + filter_shipping; }
	
	var filter_price_min = $('input[name=\'filter_price[min]\']').val();
	if (filter_price_min) { url += '&filter_price[min]=' + filter_price_min; }
	
	var filter_price_max = $('input[name=\'filter_price[max]\']').val();
	if (filter_price_max) { url += '&filter_price[max]=' + filter_price_max; }
	
	var filter_quantity_min = $('input[name=\'filter_quantity[min]\']').val();
	if (filter_quantity_min) { url += '&filter_quantity[min]=' + filter_quantity_min; }
	
	var filter_quantity_max = $('input[name=\'filter_quantity[max]\']').val();
	if (filter_quantity_max) { url += '&filter_quantity[max]=' + filter_quantity_max; }
	
	var filter_sort_order_min = $('input[name=\'filter_sort_order[min]\']').val();
	if (filter_sort_order_min) { url += '&filter_sort_order[min]=' + filter_sort_order_min; }
	
	var filter_sort_order_max = $('input[name=\'filter_sort_order[max]\']').val();
	if (filter_sort_order_max) { url += '&filter_sort_order[max]=' + filter_sort_order_max; }
	
	var filter_minimum_min = $('input[name=\'filter_minimum[min]\']').val();
	if (filter_minimum_min) { url += '&filter_minimum[min]=' + filter_minimum_min; }
	
	var filter_minimum_max = $('input[name=\'filter_minimum[max]\']').val();
	if (filter_minimum_max) { url += '&filter_minimum[max]=' + filter_minimum_max; }
	
	var filter_points_min = $('input[name=\'filter_points[min]\']').val();
	if (filter_points_min) { url += '&filter_points[min]=' + filter_points_min; }
	
	var filter_points_max = $('input[name=\'filter_points[max]\']').val();
	if (filter_points_max) { url += '&filter_points[max]=' + filter_points_max; }
	
	var filter_weight_min = $('input[name=\'filter_weight[min]\']').val();
	if (filter_weight_min) { url += '&filter_weight[min]=' + filter_weight_min; }
	
	var filter_weight_max = $('input[name=\'filter_weight[max]\']').val();
	if (filter_weight_max) { url += '&filter_weight[max]=' + filter_weight_max; }
	
	var filter_length_min = $('input[name=\'filter_length[min]\']').val();
	if (filter_length_min) { url += '&filter_length[min]=' + filter_length_min; }
	
	var filter_length_max = $('input[name=\'filter_length[max]\']').val();
	if (filter_length_max) { url += '&filter_length[max]=' + filter_length_max; }
	
	var filter_width_min = $('input[name=\'filter_width[min]\']').val();
	if (filter_width_min) { url += '&filter_width[min]=' + filter_width_min; }
	
	var filter_width_max = $('input[name=\'filter_width[max]\']').val();
	if (filter_width_max) { url += '&filter_width[max]=' + filter_width_max; }
	
	var filter_height_min = $('input[name=\'filter_height[min]\']').val();
	if (filter_height_min) { url += '&filter_height[min]=' + filter_height_min; }
	
	var filter_height_max = $('input[name=\'filter_height[max]\']').val();
	if (filter_height_max) { url += '&filter_height[max]=' + filter_height_max; }
	
	var fc = $("#filter_category").serialize();
	if (fc) { url += '&' + fc };
	
	var fa = $("#filter_attribute").serialize();
	if (fa) { url += '&' + fa };
	
	var fm = $("#filter_manufacturer").serialize();
	if (fm) { url += '&' + fm };
	
	var fss = $("#filter_stock_status").serialize();
	if (fss) { url += '&' + fss };
	
	var ftc = $("#filter_tax_class").serialize();
	if (ftc) { url += '&' + ftc };
	
	var flc = $("#filter_length_class").serialize();
	if (flc) { url += '&' + flc };
	
	var fwc = $("#filter_weight_class").serialize();
	if (fwc) { url += '&' + fwc };
	
	var product_id = $("#product_container").serialize();
	if (product_id) { url += '&' + product_id };
	
	if ($('#product_container .sort a.asc').attr('href')) {
		url += '&sort=' + $('#product_container .sort a.asc').attr('href') + '&order=ASC';
	} else if ($('#product_container .sort a.desc').attr('href')) {
		url += '&sort=' + $('#product_container .sort a.desc').attr('href') + '&order=DESC';
	} else {
		url += '&sort=pd.name&order=ASC';
	}
	
	if ($('#product_container .pagination b').html()) {
		url += '&page=' + $('#product_container .pagination b').html();
	}
	
	var filter_column = $("#filter_column").serialize();
	if (filter_column) { url += '&' + filter_column };
	
	return url;
}

function creatMessage(message) {
	if (message['success']) {
		$('#message').removeClass('warning').addClass('success').html(message['success']);
	} else {
		$('#message').removeClass('success').addClass('warning').html(message['warning']);
	}
	
	$('#message').fadeIn(700).fadeOut(1500);
	
	$('#overlay').fadeOut(1500);
}

$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function(ul, items) {
		var self = this, currentCategory = '';
		
		$.each(items, function(index, item) {
			if (item.category != currentCategory) {
				ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
				currentCategory = item.category;
			}
			
			self._renderItem(ul, item);
		});
	}
});

$(document).ready(function() {
    $('#product input, #product select').live('keypress', function(e) {
		if (e.keyCode == 13) {
			$(this).trigger('blur');
			return false;
		}
	});
	
	$('#form-copy input[name=\'product_copy\']').live('keypress', function(e) {
		if (e.keyCode == 13) return false;
	});
	
	$('#product tbody input[name=\'selected[]\']').live('change', function() {
		if ($(this).attr('checked') == 'checked') {
			$('#product .selected_row-' + $(this).val()).addClass('selected');
		} else {
			$('#product .selected_row-' + $(this).val()).removeClass('selected');
		}
	});
	
	$('#product tbody input, #product tbody textarea, #product tbody select').live('blur', function() {
		var arr = $(this).attr('class').match(/^([a-z]*)\-([a-z0-9\_]*)\-([0-9]*)$/);
		
		var html = '<span class="' + arr[0] + '">';
		
		if (arr[1] == 'select') {
			var text_1 = $('#product .' + arr[0] + ' :selected').text();
		} else {
			var text_1 = $(this).val();
		}
		
		if (text != text_1) {
			var value = $(this).val();
			
			$.ajax({
				type: 'post',
				dataType: 'json',
				data: 'product_id=' + arr[3] + '&value=' + value + '&field=' + arr[2],
				url:  'index.php?route=module/managercabinet/quickEditProduct&token=' + token,
				success: function(data) {
					if (data['warning']) {
						$('#product .' + arr[0]).replaceWith(html + text + '</span>');
						creatMessage(data);
					} else {
							if (arr[2] == 'status') {
								if (data['value'] == 0) {
									$('#product .' + 'td_' + arr[2] + arr[3]).removeClass('enabled').addClass('disabled');
								} else {
									$('#product .' + 'td_' + arr[2] + arr[3]).removeClass('disabled').addClass('enabled');
								}
							} else if (arr[2] == 'quantity') {
								if (data['value'] <= 0) {
									$('#product .' + 'td_' + arr[2] + arr[3]).removeClass('quantity').addClass('quantity_0');
								} else {
									$('#product .' + 'td_' + arr[2] + arr[3]).removeClass('quantity_0').addClass('quantity');
								}
							} else {
								if (!data['value']) {
									$('#product .' + 'td_' + arr[2] + arr[3]).addClass('attention');
								} else {
									$('#product .' + 'td_' + arr[2] + arr[3]).removeClass('attention');
								}
							}
							
							if (arr[1] == 'select') {
								$('#product .' + arr[0]).replaceWith(html + text_1 + '</span>');
							} else if ((arr[2] == 'name' && data['value'] == '') || (arr[2] == 'model' && data['value'] == '')) {
								$('#product .' + arr[0]).replaceWith(html + text + '</span>');
							} else {
								$('#product .' + arr[0]).replaceWith(html + data['value'] + '</span>');
							}
						}
					}
				});
			} else {
				$(this).replaceWith(html + text_1 + '</span>');
			}
		});
	
	$('#product_container .pagination a').live('click', function() {
		var data = '&page=' + $(this).attr('href');
		
		getProducts(data);
		
		return false;
	});
	
	$('#product_container .sort a').live('click', function() {
		var data = '&sort=' + $(this).attr('href');
		
		if ($(this).attr('class') == 'asc') {
			data += '&order=DESC';
		} else {
			data += '&order=ASC';
		}
		
		getProducts(data);
		
		return false;
	});
	
	$('.dd_menu').click(function() {
		var count = $('#' + $(this).attr('id') + ' input[type=\'checkbox\']:checked').length;
		
		if (count > 0) {
			count = '<b style="color:green;">(' + count + ')</b>';
		} else {
			count = '<b style="color:red;">(' + count + ')</b>';
		}
		
		$('#' + $(this).attr('id') + ' .dd_menu_title b').replaceWith(count);
	});
	
	$('#attribute tbody').each(function(index, element) {
		attributeautocomplete('attribute', index);
	});
	
	$('#product tr').live('mouseover', function(e) {
		$(this).addClass('hover');
	});
	
	$('#product tr').live('mouseout', function(e) {
		$(this).removeClass('hover');
	});
	
	$('input[name="filter_keyword"]').autocomplete({
		delay: 0,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/product/autocomplete&token=' + token + '&filter_name=' + encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return { label: item.name, value: item.product_id}
					}));
				}
			});
		},
		select: function(event, ui) {
			$('input[name=\'filter_keyword\']').val(ui.item.label);
			return false;
		}
	});
	
	$('#tabs a').tabs();
	$('#tabs-tools a').tabs();
	
	$('body').after('<div id="message"></div>').after('<div id="overlay" class="ui-widget ui-widget-overlay"></div>');;
	
	//optionautocomplete('form-options', 0);
	
	<?php if ($success) { ?>
	creatMessage({'success' : '<?php echo $success; ?>'});
	<?php } ?>
	
	getProducts('');
});
//--></script>
<?php echo $footer; ?>