<?php if ($products) { ?>
    <div class="pagination"><?php echo $pagination; ?></div>
    <br />
    <br />
    <table id="product" class="list">
        <thead>
            <tr class="sort">
                <?php foreach ($setting['fields'] as $name=>$field) { ?>
                    <?php if ($name == 'points') { ?>
                        <td class="left" colspan="2" width="1%">
                    <?php }else{ ?>
                        <td class="left">
                    <?php } ?>

                    <?php if (${'sort_' . $name}) { ?>
                        <a href="<?php echo ${'sort_' . $name}; ?>" class="<?php echo ($sort == ${'sort_' . $name}) ? strtolower($order) : ''; ?>"><?php echo ${'column_' . $name}; ?></a>
                    <?php } else { ?>
                        <?php echo ${'column_' . $name}; ?>
                    <?php } ?>
                        </td>
                <?php } ?>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product) { ?>
                <?php $class = ($product['selected']) ? 'selected' : ''; ?>
                <tr class="selected_row-<?php echo $product['product_id']; ?> <?php echo $class; ?>">
                    <?php foreach ($setting['fields'] as $name=>$field) { ?>
                    <?php if ($name == 'image') { ?>
                    <td class="center">
                        <div class="image_edit">
                            <img id="thumb-<?php echo $product['product_id']; ?>"
                                 src="<?php echo $product['thumb']; ?>"
                                 alt=""
                                 title="<?php echo $text_edit; ?>" />
                            <input type="hidden"
                                   id="image-<?php echo $product['product_id']; ?>"
                                   value="<?php echo $product['image']; ?>" />
                        </div>
                    </td>
                    <?php } else if ($name == 'points') { ?>
                    <td class="center">
                        <div class="dd_menu" id="product_rewards_<?php echo $product['product_id']; ?>">
                            <div class="dd_menu_title" onclick="toggle_1('rewards', <?php echo $product['product_id']; ?>);"></div>
                            <div class="dd_menu_container"></div>
                        </div>
                    </td>
                    <td class="left">
                        <span class="input-points-<?php echo $product['product_id']; ?>"><?php echo $product['points']; ?></span>
                    </td>
                    <?php } else if ($name == 'name') { ?>
                    <td class="left product_name">
                        <span class="input-name-<?php echo $product['product_id']; ?>"><?php echo $product['name']; ?></span>
                    </td>
                    <?php } else { ?>
                        <?php $class = (!$product[$name]) ? 'attention' : ''; ?>
                        <?php if (!isset ($field['link']) || in_array ($name, $product_description)) { ?>
                            <?php $type = 'input'; ?>
                            <?php if ($name == 'quantity') { ?>
                                <?php $class = (0 < $product['quantity']) ? 'quantity' : 'quantity_0'; ?>
                            <?php } ?>
                        <?php } else { ?>
                            <?php $type = 'select'; ?>
                        <?php } ?>

                        <?php if (preg_match ('/^tinyint/', $field['type'])) { ?>
                            <?php $type = 'select'; ?>
                            <?php if ($name == 'status') { ?>
                                <?php $class = ($product['status']) ? 'enabled' : 'disabled'; ?>
                                <?php $product['status'] = ($product['status']) ? $text_enabled : $text_disabled; ?>
                            <?php } else { ?>
                                <?php $product[$name] = ($product[$name]) ? $text_yes : $text_no; ?>
                            <?php } ?>
                        <?php } ?>

                        <?php if ($name == 'price' || $name == 'quantity') { ?>
                        <?php $field_path = 'prices'; ?>
                            <td width="1" class="left">
                                <table>
                                <?php foreach($product[$name] as $info){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $stores[$info['store_id']]; ?>
                                        </td>
                                        <td>
                                            <?php echo $info['store_'.$name]; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </table>
                            </td>
                        <?php } else { ?>
                            <td width="1"
                                class="left <?php echo $class; ?> td_<?php echo $name; ?><?php echo $product['product_id']; ?>">
                                <span class="<?php echo $type; ?>-<?php echo $name; ?>-<?php echo $product['product_id']; ?>">
                                    <?php echo $product[$name]; ?>
                                </span>
                            </td>
                        <?php } ?>
                    <?php } ?>
                    <?php } ?>
<!--                    <td>
                        <a onclick="getProductList(<?php echo $product['product_id']; ?>, 'options')"><?php echo $column_options; ?></a><?php echo ($setting['counter']) ? '(' . $product['options'] . ')' : ''; ?>
                    </td>
                    <td>
                        <a onclick="getProductList(<?php echo $product['product_id']; ?>, 'specials')"><?php echo $column_specials; ?></a><?php echo ($setting['counter']) ? '(' . $product['specials'] . ')' : ''; ?>
                    </td>
                    <td>
                        <a onclick="getProductList(<?php echo $product['product_id']; ?>, 'discounts')"><?php echo $column_discounts; ?></a><?php echo ($setting['counter']) ? '(' . $product['discounts'] . ')' : ''; ?>
                    </td>-->
                    <td colspan="2">
                        <a onclick="getProductList(<?php echo $product['product_id']; ?>, 'prices')">
                            <!--<?php echo $text_change; ?>-->Добавление товара и наличие
                        </a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <div class="pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
    <div align="center" class="attention"><?php echo $text_no_results; ?></div>
<?php } ?>