<table class="list">
    <tbody>
    <tr>
        <td class="left"><?php echo $column_keyword; ?></td>
        <td class="left">
            <form id="filter_keyword">
                <input name="filter_keyword" type="text" value="" style="width:98%;" /><br />
                <input name="filter_search_in[exact_entry]" type="checkbox" value="1" /> <b><?php echo $text_exact_entry; ?></b>
                <input name="filter_search_in[name]" type="checkbox" value="1" /> <?php echo $text_in_name; ?>
                <input name="filter_search_in[description]" type="checkbox" value="1" /> <?php echo $text_in_description; ?>
                <input name="filter_search_in[model]" type="checkbox" value="1" /> <?php echo $text_in_model; ?>
            </form>
        </td>
        <td colspan="2">
            <form class="dd_menu" id="filter_column">
                <div class="dd_menu_title" onclick="toggle('filter_column');"><?php echo $column_columns; ?> <b>(0)</b></div>
                <div class="dd_menu_container">
                    <?php $i = 0; ?>
                    <?php $class = 'even'; ?>
                    <?php foreach ($setting['fields'] as $name => $data) { ?>
                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    <div class="<?php echo $class; ?>">
                        <?php if (isset ($data['status'])) { ?>
                        <input type="checkbox" name="filter_fields[]" value="<?php echo $name; ?>" checked="checked" />
                        <?php $i++; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="filter_fields[]" value="<?php echo $name; ?>" />
                        <?php } ?>
                        <?php echo $data['alias']; ?>
                    </div>
                    <?php } ?>
                </div>
            </form>
        </td>
    </tr>
    <tr>
        <td class="left">
            <form class="dd_menu" id="filter_manufacturer">
                <div class="dd_menu_title" onclick="toggle('filter_manufacturer');"><?php echo $column_manufacturer_id; ?> <b style="color:red;">(0)</b></div>
                <div class="dd_menu_container">
                    <p><input type="checkbox" name="filter[fm_not]" value="1" /> <?php echo $text_no; ?></p>
                    <?php $class = 'odd'; ?>
                    <div class="<?php echo $class; ?>"><input type="checkbox" name="fm[]" value="0" /> <?php echo $text_none; ?></div>
                    <?php foreach ($manufacturer_id as $manufacturer) { ?>
                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    <div class="<?php echo $class; ?>"><input type="checkbox" name="fm[]" value="<?php echo $manufacturer['manufacturer_id']; ?>" /> <?php echo $manufacturer['name']; ?></div>
                    <?php } ?>
                </div>
            </form>
        </td>
        <td class="left">
            <form class="dd_menu" id="filter_category">
                <div class="dd_menu_title" onclick="toggle('filter_category');"><?php echo $column_categories; ?> <b style="color:red;">(0)</b></div>
                <div class="dd_menu_container">
                    <p><input type="checkbox" name="filter[fc_not]" value="1" /><?php echo $text_no; ?></p>
                    <?php $class = 'even'; ?>
                    <?php foreach ($categories as $category) { ?>
                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    <div class="<?php echo $class; ?>"><input type="checkbox" name="fc[]" value="<?php echo $category['category_id']; ?>" /> <?php echo $category['name']; ?></div>
                    <?php } ?>
                </div>
            </form>
        </td>
        <td class="left">
        </td>
        <td class="left">
        </td>
    </tr>

    <tr>
        <td class="center"><?php echo $column_price; ?></td>
        <td class="center">
            <?php echo $text_min;?><input type="text" name="filter_price[min]" value="" size="3" />
            -
            <?php echo $text_max;?><input type="text" name="filter_price[max]" value="" size="3" />
        </td>
        <td class="center"><?php echo $column_quantity; ?></td>
        <td class="center"><?php echo $text_min;?>
            <input type="text" name="filter_quantity[min]" value="" size="3" />
            -
            <?php echo $text_max;?><input type="text" name="filter_quantity[max]" value="" size="3" />
        </td>
    </tr>
    </tbody>

    <tbody>
    <tr class="filter">
        <td class="center" colspan="6"><a class="button" onclick="resetForm();"><?php echo $button_reset; ?></a> <a id="button-filter" onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
    </tr>
    </tbody>
</table>

<script type="text/javascript"><!--//
$(document).ready(function() {
    $('#tab-filter input').keypress(function(e) {
        if (e.keyCode == 13) {
            $('#button-filter').trigger('click');
            return false;
        }
    });
});

function filter() {
    getProducts('');

    $('#tab-filter .dd_menu .dd_menu_container').hide('low');
    $('#tab-filter .dd_menu .dd_menu_title').removeClass('dd_menu_shadow');
}

function resetForm() {
    $('#tab-filter input[type=text]').attr('value', '');
    $('#tab-filter select option:selected').attr('selected', false);
    $('#tab-filter .dd_menu .dd_menu_container').hide('low');
    $('#tab-filter .dd_menu .dd_menu_title').removeClass('dd_menu_shadow');

    $('#tab-filter .dd_menu .dd_menu_title b:not(#tab-filter #filter_column .dd_menu_title b)').replaceWith('<b style="color:red;">(0)</b>');
    $('#tab-filter input[type=checkbox]:checked:not(#tab-filter #filter_column input)').attr('checked', false);
}
//--></script>
<script type="text/javascript"><!--//
var count = <?php echo $i; ?>;
if (count > 0) {
    html = '<b style="color:green;">(' + count + ')</b>';
} else {
    html = '<b style="color:red;">(' + count + ')</b>';
}

$('#tab-filter #filter_column .dd_menu_title b').replaceWith(html);
//--></script>