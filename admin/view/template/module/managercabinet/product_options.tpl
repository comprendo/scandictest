<div class="content">
    <table class="form">
        <tr>
            <td width="1%"><img src="<?php echo $product_image; ?>" /></td>
            <td width="99%"><h3><?php echo $product_name; ?></h3></td>
        </tr>
    </table>

    <form id="form_list">
        <div id="store-tabs" class="htabs">
            <?php foreach($stores as $store_id => $store){ ?>
            <a href="#store-<?php echo $store_id; ?>"><?php echo $store; ?></a>
            <?php } ?>
        </div>

        <?php foreach($stores as $store_id => $store){ ?>
        <div id="store-<?php echo $store_id; ?>">
            <div id="vtab-options-form_list-<?php echo $store_id; ?>" class="vtabs">
                <?php foreach ($product_options['product_options'] as $option_index => $product_option) { ?>
                <?php if ($product_option['store_id'] == $store_id){ ?>
                    <a href="#tab-option-form_list-<?php echo $option_index; ?>"
                       id="option-form_list-<?php echo $option_index; ?>">
                        <?php echo $product_option['name']; ?>
                        &nbsp;
                        <img src="view/image/delete.png"
                             alt=""
                             onclick="$('#form_list #vtab-options-form_list a:first').trigger('click'); $('#option-form_list-<?php echo $option_index; ?>').remove(); $('#tab-option-form_list-<?php echo $option_index; ?>').remove(); return false;" />
                    </a>
                    <?php } ?>
                <?php } ?>
                <span id="option-add-form_list">
                    <input name="option" value="" style="width:130px;" data-store-id="<?php echo $store_id; ?>"/>
                    &nbsp;
                    <img src="view/image/add.png"
                         alt="<?php echo $button_insert; ?>"
                         title="<?php echo $button_insert; ?>" />
                </span>
            </div>

            <?php $option_value_row = 0; ?>
            <?php foreach ($product_options['product_options'] as $option_index => $product_option) { ?>
                <?php if ($product_option['store_id'] == $store_id){ ?>
                <div id="tab-option-form_list-<?php echo $option_index; ?>" class="vtabs-content">
                    <input type="hidden" name="product_options[<?php echo $option_index; ?>][product_option_id]" value="<?php echo $product_option['product_option_id']; ?>" />
                    <input type="hidden" name="product_options[<?php echo $option_index; ?>][name]" value="<?php echo $product_option['name']; ?>" />
                    <input type="hidden" name="product_options[<?php echo $option_index; ?>][option_id]" value="<?php echo $product_option['option_id']; ?>" />
                    <input type="hidden" name="product_options[<?php echo $option_index; ?>][type]" value="<?php echo $product_option['type']; ?>" />
                    <input type="hidden" name="product_options[<?php echo $option_index; ?>][store_id]" value="<?php echo $product_option['store_id']; ?>" />
                    <table class="form">
                        <tr>
                            <td><?php echo $entry_required; ?></td>
                            <td>
                                <select name="product_options[<?php echo $option_index; ?>][required]">
                                    <?php if ($product_option['required']) { ?>
                                        <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                        <option value="0"><?php echo $text_no; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_yes; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>

                        <?php if ($product_option['type'] == 'textarea') { ?>
                        <tr>
                            <td><?php echo $entry_option_value; ?></td>
                            <?php $class = ""; ?>
                            <td>
                                <textarea name="product_options[<?php echo $option_index; ?>][option_value]"
                                          cols="40"
                                          rows="5">
                                    <?php echo $product_option['option_value']; ?>
                                </textarea>
                            </td>
                        </tr>
                        <?php } ?>

                        <?php if ($product_option['type'] == 'text' || $product_option['type'] == 'file' || $product_option['type'] == 'date' || $product_option['type'] == 'datetime' || $product_option['type'] == 'time'){ ?>
                        <tr>
                            <td>
                                <?php if ($product_option['type'] == 'text')        $class=""; ?>
                                <?php if ($product_option['type'] == 'file')        $class=""; ?>
                                <?php if ($product_option['type'] == 'date')        $class="date"; ?>
                                <?php if ($product_option['type'] == 'datetime')    $class="datetime"; ?>
                                <?php if ($product_option['type'] == 'time')        $class="time"; ?>
                                <input type="text"
                                       name="product_options[<?php echo $option_index; ?>][option_value]"
                                       value="<?php echo $product_option['option_value']; ?>"
                                       class="<?php echo $class; ?>"/>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>

                    <?php if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') { ?>
                        <table id="option-value-form_list-<?php echo $option_index; ?>" class="list">
                            <thead>
                                <tr>
                                    <td class="left"><?php echo $entry_option_value; ?></td>
                                    <td class="right"><?php echo $entry_quantity; ?></td>
                                    <td class="left"><?php echo $entry_subtract; ?></td>
                                    <td class="right"><?php echo $entry_price; ?></td>
                                    <td class="right"><?php echo $entry_option_points; ?></td>
                                    <td class="right"><?php echo $entry_weight; ?></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <?php foreach ($product_option['product_option_value'] as $product_option_value) { ?>
                                <tbody id="option-value-row-form_list-<?php echo $option_value_row; ?>">
                                    <tr>
                                        <td class="left">
                                            <select name="product_options[<?php echo $option_index; ?>][product_option_value][<?php echo $option_value_row; ?>][option_value_id]">
                                                <?php if (isset($product_options['option_values'][$product_option['option_id']])) { ?>
                                                    <?php foreach ($product_options['option_values'][$product_option['option_id']] as $option_value) { ?>
                                                        <?php if ($option_value['option_value_id'] == $product_option_value['option_value_id']) { ?>
                                                            <option value="<?php echo $option_value['option_value_id']; ?>" selected="selected"><?php echo $option_value['name']; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <input type="hidden"
                                                   name="product_options[<?php echo $option_index; ?>][product_option_value][<?php echo $option_value_row; ?>][product_option_value_id]"
                                                   value="<?php echo $product_option_value['product_option_value_id']; ?>" />
                                        </td>
                                        <td class="right">
                                            <input type="text"
                                                   name="product_options[<?php echo $option_index; ?>][product_option_value][<?php echo $option_value_row; ?>][quantity]"
                                                   value="<?php echo $product_option_value['quantity']; ?>" size="3" /></td>
                                        <td class="left">
                                            <select name="product_options[<?php echo $option_index; ?>][product_option_value][<?php echo $option_value_row; ?>][subtract]">
                                                <?php if ($product_option_value['subtract']) { ?>
                                                    <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                                    <option value="0"><?php echo $text_no; ?></option>
                                                <?php } else { ?>
                                                    <option value="1"><?php echo $text_yes; ?></option>
                                                    <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td class="right">
                                            <select name="product_options[<?php echo $option_index; ?>][product_option_value][<?php echo $option_value_row; ?>][price_prefix]">
                                                <?php if ($product_option_value['price_prefix'] == '+') { ?>
                                                    <option value="+" selected="selected">+</option>
                                                <?php } else { ?>
                                                    <option value="+">+</option>
                                                <?php } ?>

                                                <?php if ($product_option_value['price_prefix'] == '-') { ?>
                                                    <option value="-" selected="selected">-</option>
                                                <?php } else { ?>
                                                    <option value="-">-</option>
                                                <?php } ?>
                                            </select>
                                            <input type="text" name="product_options[<?php echo $option_index; ?>][product_option_value][<?php echo $option_value_row; ?>][price]" value="<?php echo $product_option_value['price']; ?>" size="5" />
                                        </td>
                                        <td class="right">
                                            <select name="product_options[<?php echo $option_index; ?>][product_option_value][<?php echo $option_value_row; ?>][points_prefix]">
                                                <?php if ($product_option_value['points_prefix'] == '+') { ?>
                                                    <option value="+" selected="selected">+</option>
                                                <?php } else { ?>
                                                    <option value="+">+</option>
                                                <?php } ?>
                                                <?php if ($product_option_value['points_prefix'] == '-') { ?>
                                                    <option value="-" selected="selected">-</option>
                                                <?php } else { ?>
                                                    <option value="-">-</option>
                                                <?php } ?>
                                            </select>
                                            <input type="text" name="product_options[<?php echo $option_index; ?>][product_option_value][<?php echo $option_value_row; ?>][points]" value="<?php echo $product_option_value['points']; ?>" size="5" />
                                        </td>
                                        <td class="right">
                                            <select name="product_options[<?php echo $option_index; ?>][product_option_value][<?php echo $option_value_row; ?>][weight_prefix]">
                                                <?php if ($product_option_value['weight_prefix'] == '+') { ?>
                                                    <option value="+" selected="selected">+</option>
                                                <?php } else { ?>
                                                    <option value="+">+</option>
                                                <?php } ?>

                                                <?php if ($product_option_value['weight_prefix'] == '-') { ?>
                                                    <option value="-" selected="selected">-</option>
                                                <?php } else { ?>
                                                    <option value="-">-</option>
                                                <?php } ?>
                                            </select>
                                            <input type="text" name="product_options[<?php echo $option_index; ?>][product_option_value][<?php echo $option_value_row; ?>][weight]" value="<?php echo $product_option_value['weight']; ?>" size="5" />
                                        </td>
                                        <td class="left">
                                            <a onclick="$('#form_list #option-value-row-form_list-<?php echo $option_value_row; ?>').remove();"
                                               class="button">
                                                <?php echo $button_remove; ?>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                                <?php $option_value_row++; ?>
                            <?php } ?>
                            <tfoot>
                                <tr>
                                    <td colspan="6"></td>
                                    <td class="left">
                                        <a onclick="addOptionValue('form_list', '<?php echo $option_index; ?>', '<?php echo $store_id; ?>');"
                                           class="button">
                                            <?php echo $button_insert; ?>
                                        </a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <select id="option-values-form_list<?php echo $option_index; ?>" style="display: none;">
                            <?php if (isset ($product_options['option_values'][$product_option['option_id']])) { ?>
                                <?php foreach ($product_options['option_values'][$product_option['option_id']] as $option_value) { ?>
                                    <option value="<?php echo $option_value['option_value_id']; ?>">
                                        <?php echo $option_value['name']; ?>
                                    </option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    <?php } ?>
                </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php } ?>

        <div class="before" style="clear:both;"></div>
        <table class="list">
            <tr>
                <td class="center">
                    <a class="button"
                       onclick="editProductList(<?php echo $product_id; ?>, 'options', 'upd');">
                        <?php echo $button_save; ?>
                    </a>
                    <a class="button"
                       onclick="$('#dialog').dialog('close');">X
                    </a>
                </td>
            </tr>
        </table>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        optionautocomplete('form_list', 1000);


        $('#store-tabs a').tabs();
        <?php foreach($stores as $store_id => $store){ ?>
            $('#form_list #vtab-options-form_list-<?php echo $store_id; ?> a').tabs();
        <?php } ?>


        $('#form_list .date').datepicker({dateFormat: 'yy-mm-dd'});
        $('#form_list .datetime').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'h:m'});
        $('#form_list .time').timepicker({timeFormat: 'h:m'});

        $('#dialog').dialog('option', 'title', '<?php echo $column_options; ?>');
    });

    var option_row = 1000;
    var option_value_row = 1000;

    function optionautocomplete(form, option_row) {
        $('#' + form + ' input[name=\'option\']').catcomplete({
            delay: 0,
            source: function(request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/option/autocomplete&token=' + token + '&filter_name=' +  encodeURIComponent(request.term),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                category: item.category,
                                label: item.name,
                                value: item.option_id,
                                type: item.type,
                                option_value: item.option_value
                            }
                        }));
                    }
                });
            },
            select: function(event, ui) {
                html  = '<div id="tab-option-' + form + '-' + option_row + '" class="vtabs-content">';
                html += ' <input type="hidden" name="product_options[' + option_row + '][product_option_id]" value="" />';
                html += ' <input type="hidden" name="product_options[' + option_row + '][name]" value="' + ui.item.label + '" />';
                html += ' <input type="hidden" name="product_options[' + option_row + '][option_id]" value="' + ui.item.value + '" />';
                html += ' <input type="hidden" name="product_options[' + option_row + '][type]" value="' + ui.item.type + '" />';
                html += ' <input type="hidden" name="product_options[' + option_row + '][store_id]" value="' + $(this).data('store-id') + '" />';
                html += ' <table class="form">';
                html += '  <tr>';
                html += '   <td><?php echo $entry_required; ?></td>';
                html += '   <td><select name="product_options[' + option_row + '][required]">';
                html += '    <option value="1"><?php echo $text_yes; ?></option>';
                html += '    <option value="0"><?php echo $text_no; ?></option>';
                html += '   </select></td>';
                html += '  </tr>';

                if (ui.item.type == 'text') {
                    html += '  <tr>';
                    html += '   <td><?php echo $entry_option_value; ?></td>';
                    html += '   <td><input type="text" name="product_options[' + option_row + '][option_value]" value="" /></td>';
                    html += '  </tr>';
                }

                if (ui.item.type == 'textarea') {
                    html += '  <tr>';
                    html += '   <td><?php echo $entry_option_value; ?></td>';
                    html += '   <td><textarea name="product_options[' + option_row + '][option_value]" cols="40" rows="5"></textarea></td>';
                    html += '  </tr>';
                }

                if (ui.item.type == 'file') {
                    html += ' <tr style="display: none;">';
                    html += '  <td><?php echo $entry_option_value; ?></td>';
                    html += '  <td><input type="text" name="product_options[' + option_row + '][option_value]" value="" /></td>';
                    html += ' </tr>';
                }

                if (ui.item.type == 'date') {
                    html += ' <tr>';
                    html += '  <td><?php echo $entry_option_value; ?></td>';
                    html += '  <td><input type="text" name="product_options[' + option_row + '][option_value]" value="" class="date" /></td>';
                    html += ' </tr>';
                }

                if (ui.item.type == 'datetime') {
                    html += '  <tr>';
                    html += '   <td><?php echo $entry_option_value; ?></td>';
                    html += '   <td><input type="text" name="product_options[' + option_row + '][option_value]" value="" class="datetime" /></td>';
                    html += '  </tr>';
                }

                if (ui.item.type == 'time') {
                    html += '  <tr>';
                    html += '   <td><?php echo $entry_option_value; ?></td>';
                    html += '   <td><input type="text" name="product_options[' + option_row + '][option_value]" value="" class="time" /></td>';
                    html += '  </tr>';
                }

                html += ' </table>';

                if (ui.item.type == 'select' || ui.item.type == 'radio' || ui.item.type == 'checkbox' || ui.item.type == 'image') {
                    html += ' <table id="option-value-' + form + '-' + option_row + '" class="list">';
                    html += '  <thead>';
                    html += '   <tr>';
                    html += '    <td class="left"><?php echo $entry_option_value; ?></td>';
                    html += '    <td class="right"><?php echo $entry_quantity; ?></td>';
                    html += '    <td class="left"><?php echo $entry_subtract; ?></td>';
                    html += '    <td class="right"><?php echo $entry_price; ?></td>';
                    html += '    <td class="right"><?php echo $entry_option_points; ?></td>';
                    html += '    <td class="right"><?php echo $entry_weight; ?></td>';
                    html += '    <td></td>';
                    html += '   </tr>';
                    html += '  </thead>';
                    html += '  <tfoot>';
                    html += '   <tr>';
                    html += '    <td colspan="6"></td>';
                    html += '    <td class="left"><a onclick="addOptionValue(\'' + form + '\', ' + option_row + ');" class="button"><?php echo $button_insert; ?></a></td>';
                    html += '   </tr>';
                    html += '  </tfoot>';
                    html += ' </table>';
                    html += ' <select id="option-values-' + form + option_row + '" style="display: none;">';

                    for (i = 0; i < ui.item.option_value.length; i++) {
                        html += '  <option value="' + ui.item.option_value[i]['option_value_id'] + '">' + ui.item.option_value[i]['name'] + '</option>';
                    }

                    html += ' </select>';
                    html += '</div>';
                }

                $('#' + form + ' #store-' + $(this).data('store-id')).append(html);

                $('#option-add-' + form,'#vtab-options-' + form + '-' + $(this).data('store-id')).before('<a href="#tab-option-' + form + '-' + option_row + '" id="option-' + form + '-' + option_row + '">' + ui.item.label + '&nbsp;<img src="view/image/delete.png" alt="" onclick="$(\'#' + form + ' #vtab-options-' + form + ' a:first\').trigger(\'click\'); $(\'#option-' + form + '-' + option_row + '\').remove(); $(\'#tab-option-' + form + '-' + option_row + '\').remove(); return false;" /></a>');

                <?php foreach($stores as $store_id => $store){ ?>
                    $('#vtab-options-' + form + '-<?php echo $store_id;?> a').tabs();
                <?php } ?>

                $('#' + form + ' #option-' + form + '-' + option_row).trigger('click');

                $('#' + form + ' .datetime').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'h:m'});
                $('#' + form + ' .date').datepicker({dateFormat: 'yy-mm-dd'});
                $('#' + form + ' .time').timepicker({timeFormat: 'h:m'});

                option_row++;

                return false;
            }
        });
    }
</script>