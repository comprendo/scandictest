<link type="text/css" rel="stylesheet" href="view/managercabinet/stylesheet/style.css" />
<div class="content">
    <table class="form">
        <tr>
            <td width="1%"><img src="<?php echo $product_image; ?>" /></td>
            <td width="99%"><h3><?php echo $product_name; ?></h3></td>
        </tr>
    </table>
    <form id="form_list">
        <div id="stores" class="htabs">
            <?php foreach ($stores as $store_id => $store) { ?>
            <a href="#store-<?php echo $store_id; ?>">
                <?php echo $store; ?>
            </a>
            <?php } ?>
        </div>
        <?php foreach ($stores as $store_id => $store) { ?>
        <div id="store-<?php echo $store_id; ?>">
            <table class="form">
                <tr>
                    <td><?php echo $entry_use_store; ?></td>
                    <td>
                        <?php if (isset($product_prices[$store_id])) { ?>
                            <input id="checkbox-store-<?php echo $store_id; ?>" type="checkbox" checked="checked" name="product_prices[<?php echo $store_id; ?>][use_store]">
                        <?php } else { ?>
                            <input id="checkbox-store-<?php echo $store_id; ?>" type="checkbox" name="product_prices[<?php echo $store_id; ?>][use_store]">
                        <?php } ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><?php echo $entry_price; ?></td>
                    <td>
                        <!--<input onchange="this.value = this.value<32475 ?(alert('Введено значение ниже минимального. При сохранении товар будет отключен.'), 0): this.value;" type='number'
                               name="product_prices[<?php echo $store_id; ?>][store_price]" 
                               maxlength="20" 
                               size="20" value="<?php echo isset($product_prices[$store_id]) ? $product_prices[$store_id]['store_price'] : ''; ?>" />-->
                        <input type="text" 
                               name="product_prices[<?php echo $store_id; ?>][store_price]" 
                               maxlength="20" 
                               size="20" value="<?php echo isset($product_prices[$store_id]) ? $product_prices[$store_id]['store_price'] : ''; ?>" />
                    </td>
                    <td><?php echo $entry_quantity; ?></td>
                    <td>
                        <input type="text"
                               name="product_prices[<?php echo $store_id; ?>][store_quantity]"
                               maxlength="20"
                               size="20"
                               value="<?php echo isset($product_prices[$store_id]) ? $product_prices[$store_id]['store_quantity'] : ''; ?>" />
                    </td>
                    <td><div style="display:none"><?php echo $entry_store_sku; ?></div></td>
                    <td><div style="display:none">
                        <input type="text"
                               name="product_prices[<?php echo $store_id; ?>][store_sku]"
                               maxlength="20"
                               size="20"
                               value="<?php echo isset($product_prices[$store_id]) ? $product_prices[$store_id]['store_sku'] : ''; ?>" />
                        </div>
                    </td>
                </tr>
                <!--<tr><td colspan="4">Минимальное доступное значение цены - <span style="color: red; font-weight: bold;">32475</span>. Оно соответствует цене в регионе Санкт-Петербург.<?php echo $product_sku; ?></td>
                </tr>-->
                <tr style="display:none">
                    <td><?php echo $entry_store_location; ?></td>
                    <td>
                        <input type="text"
                               name="product_prices[<?php echo $store_id; ?>][store_location]"
                               maxlength="20"
                               size="20"
                               value="<?php echo isset($product_prices[$store_id]) ? $product_prices[$store_id]['store_location'] : ''; ?>" />
                    </td>
                    <td><?php echo $entry_store_stock_status_id; ?></td>
                    <td>
                        <select name="product_prices[<?php echo $store_id; ?>][store_stock_status_id]">
                            <?php foreach ($stock_statuses as $stock_status) { ?>
                                <?php if (isset($product_prices[$store_id])) { ?>
                                    <?php if ($stock_status['stock_status_id'] == $product_prices[$store_id]['store_stock_status_id']) { ?>
                                    <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <?php if ($stock_status['stock_status_id'] == $default_store_status) { ?>
                                    <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </td>
                    <td><?php echo $entry_store_shipping; ?></td>
                    <td>
                        <?php if (isset($product_prices[$store_id])) { ?>
                            <?php if ($product_prices[$store_id]['store_shipping']) { ?>
                                <input type="radio" name="product_prices[<?php echo $store_id; ?>][store_shipping]" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="product_prices[<?php echo $store_id; ?>][store_shipping]" value="0" />
                                <?php echo $text_no; ?>
                                <?php } else { ?>
                                <input type="radio" name="product_prices[<?php echo $store_id; ?>][store_shipping]" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="product_prices[<?php echo $store_id; ?>][store_shipping]" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        <?php }else{ ?>
                            <input type="radio" name="product_prices[<?php echo $store_id; ?>][store_shipping]" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <input type="radio" name="product_prices[<?php echo $store_id; ?>][store_shipping]" value="0" />
                            <?php echo $text_no; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr style="display:none">
                    <td><?php echo $entry_store_points; ?></td>
                    <td>
                        <input type="text"
                               name="product_prices[<?php echo $store_id; ?>][store_points]"
                               maxlength="20"
                               size="20"
                               value="<?php echo isset($product_prices[$store_id]) ? $product_prices[$store_id]['store_points'] : ''; ?>" />
                    </td>
                    <td><?php echo $entry_store_tax_class_id; ?></td>
                    <td>
                        <select name="product_prices[<?php echo $store_id; ?>][store_tax_class_id]">
                            <option value="0"><?php echo $text_none; ?></option>
                            <?php foreach ($tax_classes as $tax_class) { ?>
                            <?php if ($tax_class['tax_class_id'] == $product_prices[$store_id]['store_tax_class_id']) { ?>
                            <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </td>
                    <td><?php echo $entry_store_date_available; ?></td>
                    <td>
                        <input type="text"
                               name="product_prices[<?php echo $store_id; ?>][store_date_available]"
                               size="12"
                               value="<?php echo isset($product_prices[$store_id]) ? $product_prices[$store_id]['store_date_available'] : ''; ?>"
                               class="date" />
                    </td>
                </tr>
                <tr style="display:none">
                    <td><?php echo $entry_subtract; ?></td>
                    <td>
                        <select name="product_prices[<?php echo $store_id; ?>][store_subtract]">
                            <?php if ($product_prices[$store_id]['store_subtract']) { ?>
                            <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                            <option value="0"><?php echo $text_no; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_yes; ?></option>
                            <option value="0" selected="selected"><?php echo $text_no; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td colspan="4"></td>
                </tr>
            </table>
        </div>
        <?php } ?>
        <table class="list">
            <tr>
                <td class="center"><a class="button" onclick="editProductList(<?php echo $product_id; ?>, 'prices', 'upd');"><?php echo $button_save; ?></a> <a class="button" onclick="$('#dialog').dialog('close');">X</a></td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#form_list #stores a').tabs();
        $('.date').datepicker({dateFormat: 'yy-mm-dd'});
        $('#dialog').dialog('option', 'title', '<?php echo $column_prices; ?>');
    });
</script>