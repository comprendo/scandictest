<div class="content">
    <table class="form">
        <tr>
            <td width="1%"><img src="<?php echo $product_image; ?>" /></td>
            <td width="99%"><h3><?php echo $product_name; ?></h3></td>
        </tr>
    </table>
    <form id="form_list">
        <div id="store-tabs" class="htabs">
            <?php foreach($stores as $store_id => $store){ ?>
            <a href="#store-<?php echo $store_id; ?>"><?php echo $store; ?></a>
            <?php } ?>
        </div>

        <?php foreach($stores as $store_id => $store){ ?>
        <div id="store-<?php echo $store_id; ?>">
            <table id="discount_list-<?php echo $store_id;?>" class="list">
                <thead>
                    <tr>
                        <td class="left" width="1%" ></td>
                        <td class="left" width="19%"><?php echo $column_customer_group; ?></td>
                        <td class="left" width="10%"><?php echo $column_quantity; ?></td>
                        <td class="left" width="10%"><?php echo $column_priority; ?></td>
                        <td class="left" width="20%"><?php echo $column_discount; ?></td>
                        <td class="left" width="20%"><?php echo $column_date_start; ?></td>
                        <td class="left" width="20%"><?php echo $column_date_end; ?></td>
                    </tr>
                </thead>
                <?php if ($product_discounts) { ?>
                    <?php foreach ($product_discounts as $discount_row => $product_discount) { ?>
                        <?php if ($product_discount['store_id'] == $store_id){ ?>
                        <tbody id="discount-row-<?php echo $discount_row; ?>">
                            <tr class="filter">
                                <td class="center"><a onclick="$('#discount_list-<?php echo $store_id; ?> #discount-row-<?php echo $discount_row; ?>').remove();"><img alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" src="view/image/delete.png"/></a></td>
                                <td class="left">
                                    <select name="product_discounts[<?php echo $discount_row; ?>][customer_group_id]">
                                    <?php foreach ($customer_groups as $customer_group) { ?>
                                    <?php if ($customer_group['customer_group_id'] == $product_discount['customer_group_id']) { ?>
                                    <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                    </select>
                                </td>
                                <td class="left"><input type="text" name="product_discounts[<?php echo $discount_row; ?>][quantity]" value="<?php echo $product_discount['quantity']; ?>" size="2" /></td>
                                <td class="left"><input type="text" name="product_discounts[<?php echo $discount_row; ?>][priority]" value="<?php echo $product_discount['priority']; ?>" size="2" /></td>
                                <td class="left"><input type="text" name="product_discounts[<?php echo $discount_row; ?>][discount]" value="<?php echo $product_discount['price']; ?>" /></td>
                                <td class="left"><input type="text" name="product_discounts[<?php echo $discount_row; ?>][date_start]" value="<?php echo $product_discount['date_start']; ?>" class="date" /></td>
                                <td class="left"><input type="text" name="product_discounts[<?php echo $discount_row; ?>][date_end]" value="<?php echo $product_discount['date_end']; ?>" class="date" />
                                                 <input type="hidden" name="product_discounts[<?php echo $discount_row; ?>][store_id]" value="<?php echo $product_discount['store_id']; ?>" /></td>
                            </tr>
                        </tbody>
                        <?php } ?>
                    <?php } ?>
                <?php } else { ?>
                        <tbody class="no_results">
                            <tr>
                                <td class="center" colspan="7"><?php echo $text_no_results; ?></td>
                            </tr>
                        </tbody>
                <?php } ?>
                        <tfoot>
                            <tr>
                                <td class="left"><a onclick="addDiscount('discount_list-<?php echo $store_id?>', <?php echo $store_id?>);"><img alt="<?php echo $button_insert; ?>" title="<?php echo $button_insert; ?>" src="view/image/add.png"/></a></td>
                                <td class="center" colspan="6"><a class="button" onclick="editProductList(<?php echo $product_id; ?>, 'discounts', 'upd');"><?php echo $button_save; ?></a> <a class="button" onclick="$('#dialog').dialog('close');">X</a></td>
                            </tr>
                        </tfoot>
            </table>
        </div>
        <?php } ?>
    </form>
</div>

<script type="text/javascript">
$(document).ready(function() {

    <?php foreach($stores as $store_id => $store){ ?>
        $('#discount_list-<?php echo $store_id; ?> tbody .date').datepicker({dateFormat: 'yy-mm-dd'});
    <?php } ?>

    $('#store-tabs a').tabs();
	$('#dialog').dialog('option', 'title', '<?php echo $column_discounts; ?>');
});

var discount_row = 1000;

function addDiscount(table, store_id) {
    html  = '<tbody id="discount-row-' + discount_row + '">';
    html += '<tr class="filter">';
    html += '<td class="center"><a onclick="$(\'#' + table + ' #discount-row-' + discount_row + '\').remove();"><img alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" src="view/image/delete.png"/></a></td>';
    html += '<td class="left"><select name="product_discounts[' + discount_row + '][customer_group_id]">';
<?php foreach ($customer_groups as $customer_group) { ?>
        html += '<option value="<?php echo $customer_group["customer_group_id"]; ?>"><?php echo $customer_group["name"]; ?></option>';
    <?php } ?>
    html += '</select></td>';
    html += '<td class="left"><input type="text" name="product_discounts[' + discount_row + '][quantity]" value="" size="2" /></td>';
    html += '<td class="left"><input type="text" name="product_discounts[' + discount_row + '][priority]" value="" size="2" /></td>';
    html += '<td class="left">';

    html += ' <input type="text" name="product_discounts[' + discount_row + '][discount]" value="" /></td>';
    html += '<td class="left"><input type="text" name="product_discounts[' + discount_row + '][date_start]" value="" class="date" /></td>';
    html += '<td class="left"><input type="text" name="product_discounts[' + discount_row + '][date_end]" value="" class="date" />';
    html += '<input type="hidden" name="product_discounts[' + discount_row + '][store_id]" value="' + store_id + '"/></td>';
    html += '</tr>';
    html += '</tbody>';

    $('#' + table + ' tbody.no_results').replaceWith('');

    $('#' + table + ' tfoot').before(html);

    $('#' + table + ' #discount-row-' + discount_row + ' .date').datepicker({dateFormat: 'yy-mm-dd'});

    discount_row++;
}

</script>