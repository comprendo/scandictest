<div class="content">
    <table class="form">
        <tr>
            <td width="1%"><img src="<?php echo $product_image; ?>" /></td>
            <td width="99%"><h3><?php echo $product_name; ?></h3></td>
        </tr>
    </table>
    <form id="form_list">
        <div id="store-tabs" class="htabs">
            <?php foreach($stores as $store_id => $store){ ?>
            <a href="#store-<?php echo $store_id; ?>"><?php echo $store; ?></a>
            <?php } ?>
        </div>

        <?php foreach($stores as $store_id => $store){ ?>
        <div id="store-<?php echo $store_id; ?>">
            <table id="special_list-<?php echo $store_id; ?>" class="list">
                <thead>
                    <tr>
                        <td class="left" width="1%" ></td>
                        <td class="left" width="19%"><?php echo $column_customer_group; ?></td>
                        <td class="left" width="20%"><?php echo $column_priority; ?></td>
                        <td class="left" width="20%"><?php echo $column_discount; ?></td>
                        <td class="left" width="20%"><?php echo $column_date_start; ?></td>
                        <td class="left" width="20%"><?php echo $column_date_end; ?></td>
                    </tr>
                </thead>
                <?php if ($product_specials) { ?>
                    <?php foreach ($product_specials as $special_row => $product_special) { ?>
                        <?php if ($product_special['store_id'] == $store_id){ ?>
                        <tbody id="special-row-<?php echo $special_row; ?>">
                            <tr class="filter">
                                <td class="center"><a onclick="$('#special_list-<?php echo $store_id; ?> #special-row-<?php echo $special_row; ?>').remove();"><img alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" src="view/image/delete.png"/></a></td>
                                <td class="left">
                                    <select name="product_specials[<?php echo $special_row; ?>][customer_group_id]">
                                        <?php foreach ($customer_groups as $customer_group) { ?>
                                        <?php if ($customer_group['customer_group_id'] == $product_special['customer_group_id']) { ?>
                                        <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td class="left"><input type="text" name="product_specials[<?php echo $special_row; ?>][priority]" value="<?php echo $product_special['priority']; ?>" size="2" /></td>
                                <td class="left"><input type="text" name="product_specials[<?php echo $special_row; ?>][special]" value="<?php echo $product_special['price']; ?>" /></td>
                                <td class="left"><input type="text" name="product_specials[<?php echo $special_row; ?>][date_start]" value="<?php echo $product_special['date_start']; ?>" class="date" /></td>
                                <td class="left">
                                    <input type="text" name="product_specials[<?php echo $special_row; ?>][date_end]" value="<?php echo $product_special['date_end']; ?>" class="date" />
                                    <input type="hidden" name="product_specials[<?php echo $special_row; ?>][store_id]" value="<?php echo $product_special['store_id']; ?>" class="date" />
                                </td>

                            </tr>
                        </tbody>
                        <?php } ?>
                    <?php } ?>
                <?php } else { ?>
                    <tbody class="no_results">
                        <tr>
                            <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
                        </tr>
                    </tbody>
                <?php } ?>
                    <tfoot>
                        <tr>
                            <td class="left"><a onclick="addSpecial('special_list-'+<?php echo $store_id; ?>, <?php echo $store_id; ?>);"><img alt="<?php echo $button_insert; ?>" title="<?php echo $button_insert; ?>" src="view/image/add.png"/></a></td>
                            <td class="center" colspan="5"><a class="button" onclick="editProductList(<?php echo $product_id; ?>, 'specials', 'upd');"><?php echo $button_save; ?></a> <a class="button" onclick="$('#dialog').dialog('close');">X</a></td>
                        </tr>
                    </tfoot>
            </table>
        </div>
        <?php } ?>
    </form>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#store-tabs a').tabs();
    <?php foreach($stores as $store_id => $store){ ?>
	$('#special_list-<?php echo $store_id; ?> tbody .date').datepicker({dateFormat: 'yy-mm-dd'});
    <?php } ?>
	
	$('#dialog').dialog('option', 'title', '<?php echo $column_specials; ?>');
});

var special_row = 1000;

function addSpecial(table, store_id) {
    html  = '<tbody id="special-row-' + special_row + '">';
    html += '<tr class="filter">';
    html += '<td class="center"><a onclick="$(\'#' + table + ' #special-row' + special_row + '\').remove();"><img alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" src="view/image/delete.png"/></a></td>';
    html += '<td class="left"><select name="product_specials[' + special_row + '][customer_group_id]">';
<?php foreach ($customer_groups as $customer_group) { ?>
        html += '<option value="<?php echo $customer_group["customer_group_id"]; ?>"><?php echo $customer_group["name"]; ?></option>';
    <?php } ?>
    html += '</select></td>';
    html += '<td class="left"><input type="text" name="product_specials[' + special_row + '][priority]"   value="" size="2" /></td>';
    html += '<td class="left">';

    html += ' <input type="text" name="product_specials[' + special_row + '][special]" value="" /></td>';
    html += '<td class="left"><input type="text" name="product_specials[' + special_row + '][date_start]" value="" class="date" /></td>';
    html += '<td class="left"><input type="text" name="product_specials[' + special_row + '][date_end]"   value="" class="date" />';
    html += '<input type="hidden" name="product_specials[' + special_row + '][store_id]" value="' + store_id + '" /></td>';
    html += '</tr>';
    html += '</tbody>';

    $('#' + table + ' tbody.no_results').replaceWith('');

    $('#' + table + ' tfoot').before(html);
    $('#' + table + ' #special-row-' + special_row + ' .date').datepicker({dateFormat: 'yy-mm-dd'});
    special_row++;
}

</script>