# Настройка проекта
1. Создайте свой файлы конфигурации на основе .example файлов
- /.htaccess.example ==> /.htaccess 
- /.htpasswd.example ==> /.htpasswd 
- /robots.txt.example ==> /robots.txt
- /admin/php.ini.example ==> /admin/php.ini 
- /php.ini.example ==> /php.ini 
- /admin/config.php.example ==> /admin/config.php 
- /config.php.example ==> /config.php

2. Отредактируйте файлы /config.php и /admin/config.php
3. Импортируйте базу данных 
4. Запросите необходимые файлы
- /image
- /img
- /system
- /vqmod
- /download
- /files
- /catalog/view/theme/default/image
- /catalog/view/theme/default/stylesheet/*.(png|jpg|jpeg|svg)
- /admin/view/**/image