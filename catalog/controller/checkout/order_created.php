<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class ControllerCheckoutOrderCreated extends Controller
{
    function index()
    {
        if (!isset($this->session->data['previous_order_id'])) {
            $this->redirect($this->url->link('checkout/cart'));
        }

        $this->load->language('checkout/cart');
        $this->load->model('tool/image');
        // $this->load->model('tool/upload');
        $this->load->model('checkout/order');

        $this->document->setTitle($this->language->get('order_created_heading_title'));

        $this->data['column_image'] = $this->language->get('column_image');
        $this->data['column_name'] = $this->language->get('column_name');
        $this->data['column_model'] = $this->language->get('column_model');
        $this->data['column_quantity'] = $this->language->get('column_quantity');
        $this->data['column_price'] = $this->language->get('column_price');

        $this->data['products'] = $this->getOrderedProducts();
        $this->data['total_price'] = $this->getTotalOrderPrice($this->data['products']);

        $order_info = $this->getOrderInfo();
        $this->data['order_num'] = $order_info['id'];
        $this->data['full_name'] =
            $order_info['last_name'] . ' ' .
            $order_info['first_name'] . ' ' .
            $order_info['middle_name'];
        $this->data['email'] = $order_info['email'];
        $this->data['total'] = $order_info['total'];

        $this->data['contact_phone'] = $order_info['phone'];
        $this->data['comment'] = $order_info['comment'];
        $this->data['order_pay'] = $order_info['order_pay'];
        $this->data['shipping_method_name'] =
            $order_info['shipping_method'] == 'pickup' ? 'Самовывоз' : 'Доставка';
        $this->data['shipping_method'] = $order_info['shipping_method'];
        $this->data['pickup_point'] = $order_info['pickup_point'];
        $this->data['street'] = $order_info['street'];
        $this->data['subway'] = $order_info['subway'];
        $this->data['postal_code'] = $order_info['postal_code'];
        $this->data['house_number'] = $order_info['house_number'];
        $this->data['building_number'] = $order_info['building_number'];
        $this->data['building_entrance'] = $order_info['building_entrance'];
        $this->data['building_floor'] = $order_info['building_floor'];
        $this->data['flat'] = $order_info['flat'];

        $this->data['contact_us_phone'] = $this->config->get('config_telephone');

        $this->load->language('checkout/checkout');
        $this->data['text_delivery_street'] = $this->language->get('text_delivery_street');
        $this->data['text_delivery_subway'] = $this->language->get('text_delivery_subway');
        $this->data['text_delivery_postal_code'] = $this->language->get('text_delivery_postal_code');
        $this->data['text_delivery_house_number'] = $this->language->get('text_delivery_house_number');
        $this->data['text_delivery_building_number'] = $this->language->get('text_delivery_building_number');
        $this->data['text_delivery_building_entrance'] = $this->language->get('text_delivery_building_entrance');
        $this->data['text_delivery_building_floor'] = $this->language->get('text_delivery_building_floor');
        $this->data['text_delivery_flat_number'] = $this->language->get('text_delivery_flat_number');

        // Summary Page
        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'href' => $this->url->link('common/home'),
            'text' => $this->language->get('text_home')
        );
        // $this->data['breadcrumbs'][] = array('text' => 'Спасибо за заказ');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/order_created.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/checkout/order_created.tpl';
        } else {
            $this->template = 'default/template/checkout/order_created.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        // $this->model_checkout_order->confirmMail($order_info['id']);

        if ($order_info['confirmed'] == 0) {
            $this->sendMessages();
            $this->model_checkout_order->confirmMail($order_info['id']);
        }

        $this->response->setOutput($this->render());
    }

    private function getOrderInfo()
    {
        $order = $this->model_checkout_order->getOrder($this->session->data['previous_order_id']);

        return array(
            'id' => $order['order_id'],
            'last_name' => $order['lastname'],
            'middle_name' => $order['middlename'],
            'first_name' => $order['firstname'],
            'email' => $order['email'],
            'phone' => $order['telephone'],
            'comment' => $order['comment'],
            'order_pay' => $order['order_pay'],
            'total' => $order['total'],
            'shipping_method' => $order['shipping_method'],
            'pickup_point' => $order['pickup_point'],
            'street' => $order['street'],
            'subway' => $order['subway'],
            'postal_code' => $order['postal_code'],
            'house_number' => $order['house_number'],
            'building_number' => $order['building_number'],
            'building_entrance' => $order['building_entrance'],
            'building_floor' => $order['building_floor'],
            'flat' => $order['flat'],
            'date_added' => $order['date_added'],
            'confirmed' => $order['confirmed']
        );
    }

    private function getTotalOrderPrice($products)
    {
        $total_price = 0;
        foreach ($products as $product) {
            $total_price += $product['price'];
        }
        return $total_price;
    }

    private function getOrderedProducts()
    {
        $products = array();
        $ordered_products = $this->model_checkout_order->getOrderedProducts($this->session->data['previous_order_id']);

        foreach ($ordered_products as $product) {
            if ($product['image']) {
                $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
            } else {
                $image = '';
            }
            $products[] = array(
                'thumb' => $image,
                'name' => $product['name'],
                'model' => $product['model'],
                'quantity' => $product['quantity'],
                'price' => $product['price'],
                'total' => $product['total'],
                'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
            );
        }

        return $products;
    }

    private function sendMessages()
    {
        require 'catalog/controller/tool/PHPMailer.php';
        require 'catalog/controller/tool/Exception.php';
        require 'catalog/controller/tool/SMTP.php';

        // Mail Info

        $order_information = $this->getOrderInfo();
        $order_products = $this->getOrderedProducts();
        $total_price = $this->getTotalOrderPrice($order_products);

        $mailProducts = '<ol>';
        foreach ($order_products as $product) {
            $mailProducts .= '<li>' . $product['name'] . ', цена: ' . $product['price'] . ' руб., количество: ' . $product['quantity'] . ' шт.</li>';
        }
        $mailProducts .= '</ol>';
        $mailProducts .= 'Итого: ' . $total_price . ' руб.';

        $shipping_method = ($order_information['shipping_method'] == 'pickup') ? 'Самовывоз' : 'Доставка';
        $shipping = 'Способ доставки: ' . $shipping_method . '<br>';
        if ($order_information['shipping_method'] == 'pickup') {
            $shipping .= 'Выдача товара: ' . $order_information['pickup_point'];
        } else {
            $shipping .= 'Улица: ' . $order_information['street'] . '<br>Метро: ' . $order_information['subway'] . '<br>Индекс: ' . $order_information['postal_code'] . '<br>Дом: ' . $order_information['house_number'] . '<br>Корпус: ' . $order_information['building_number'] . '<br>Подъезд: ' . $order_information['building_entrance'] . '<br>Этаж: ' . $order_information['building_floor'] . '<br>Квартира: ' . $order_information['flat'];
        }

        // Mail to User

        $mailToUser = new PHPMailer(true);
        try {
            $mailToUser->setFrom('scandic@elisanet.fi', 'Scandic Style');
            $mailToUser->addAddress($order_information['email']);
            $mailToUser->CharSet = 'UTF-8';
            $mailToUser->isHTML(true);
            $mailToUser->Subject = 'Ваш заказ № ' . $order_information['id'] . ' на сайте scandicstyle.ru';
            $mailToUser->Body = '<h1>Заказ № ' . $order_information['id'] . '</h1><p>Вы оформили заказ на сайте scandicstyle.ru.</p><p>Состав заказа:<br>' . $mailProducts . '</p><p>Дата оформления заказа: ' . $order_information['date_added'] . '</p><p>ФИО: ' . $order_information['last_name'] . ' ' . $order_information['first_name'] . ' ' . $order_information['middle_name'] . '</p><p>E-Mail: ' . $order_information['email'] . '</p><p>Контактный телефон: ' . $order_information['phone'] . '</p><p>Комментарии к заказу: ' . $order_information['comment'] . '</p><p>' . $shipping . '</p><p>В ближайшее время наш менеджер свяжется с Вами для подтверждения заказа.</p>';
            $mailToUser->send();
        } catch (Exception $e) {
            echo 'Не удалось отправить сообщение. Ошибка: ' . $mailToUser->ErrorInfo;
        }

        // Mail to Manager

        $mailToManager = new PHPMailer(true);
        try {
            $mailToManager->setFrom('scandic@elisanet.fi', 'Scandic Style');
            $mailToManager->addAddress($this->config->get('config_email'));
            $mailToManager->addAddress('kanjon@scandicstyle.ru');
            $mailToManager->CharSet = 'UTF-8';
            $mailToManager->isHTML(true);
            $mailToManager->Subject = 'Оформлен заказ № ' . $order_information['id'] . ' на сайте scandicstyle.ru';
            $mailToManager->Body = '<h1>Заказ № ' . $order_information['id'] . '</h1><p>Оформлен заказ на сайте scandicstyle.ru.</p><p>Состав заказа:<br>' . $mailProducts . '</p><p>Дата оформления заказа: ' . $order_information['date_added'] . '</p><p>ФИО: ' . $order_information['last_name'] . ' ' . $order_information['first_name'] . ' ' . $order_information['middle_name'] . '</p><p>E-Mail: ' . $order_information['email'] . '</p><p>Контактный телефон: ' . $order_information['phone'] . '</p><p>Комментарии к заказу: ' . $order_information['comment'] . '</p><p>' . $shipping . '</p>';
            $mailToManager->send();
        } catch (Exception $e) {
            echo 'Не удалось отправить сообщение. Ошибка: ' . $mailToUser->ErrorInfo;
        }
    }
}
