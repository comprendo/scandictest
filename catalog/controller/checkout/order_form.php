<?php
class ControllerCheckoutOrderForm extends Controller
{
    function index()
    {
        if (!$this->cart->hasProducts()) {
            $this->response->redirect($this->url->link('checkout/cart'));
        }

        $this->data = array();

        $this->load->language('checkout/checkout');
        $language = $this->language;

        $this->data['text_checkout_common'] = $language->get('text_checkout_common');
        $this->data['text_checkout_shipping'] = $language->get('text_checkout_shipping');
        $this->data['text_order_form_validation_error'] = $language->get('text_order_form_validation_error');

        $this->data['entry_lastname'] = $language->get('entry_lastname');
        $this->data['entry_firstname'] = $language->get('entry_firstname');
        $this->data['entry_middlename'] = $language->get('entry_middlename');
        $this->data['entry_contact_phone'] = $language->get('entry_contact_phone');
        $this->data['entry_card_number'] = $language->get('entry_card_number');
        $this->data['entry_email'] = $language->get('entry_email');

        $this->data['text_shipping_method'] = $language->get('text_shipping_method');
        $this->data['text_pickup_shipping'] = $language->get('text_pickup_shipping');
        $this->data['text_delivery_shipping'] = $language->get('text_delivery_shipping');
        $this->data['text_pickup_point'] = $language->get('text_pickup_point');
        $this->data['text_pickup_point_select_placeholder'] = $language->get('text_pickup_point_select_placeholder');
        $this->data['text_delivery_street'] = $language->get('text_delivery_street');
        $this->data['text_delivery_subway'] = $language->get('text_delivery_subway');
        $this->data['text_delivery_postal_code'] = $language->get('text_delivery_postal_code');
        $this->data['text_delivery_house_number'] = $language->get('text_delivery_house_number');
        $this->data['text_delivery_building_number'] = $language->get('text_delivery_building_number');
        $this->data['text_delivery_building_entrance'] = $language->get('text_delivery_building_entrance');
        $this->data['text_delivery_building_floor'] = $language->get('text_delivery_building_floor');
        $this->data['text_delivery_flat_number'] = $language->get('text_delivery_flat_number');

        $this->data['text_loading'] = $this->language->get('text_loading');
        $this->data['button_do_checkout'] = $language->get('button_do_checkout');

        $this->data['pickup_points'] = array();

        // Список пунктов выдачи заказов

        $this->data['pickup_points'][] = array(
            'name' => 'Санкт-Петербург, ул. Чугунная, д. 14',
            'value' => 'Санкт-Петербург, ул. Чугунная, д. 14'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Архангельск, пр. Троицкий 37',
            'value' => 'Архангельск, пр. Троицкий 37'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Белгород, ул. Белгородского полка 62',
            'value' => 'Белгород, ул. Белгородского полка 62'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Барнаул, пр. Космонавтов, д. 6 Г, ТВК Республика',
            'value' => 'Барнаул, пр. Космонавтов, д. 6 Г, ТВК Республика'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Чебоксары, ул. Пирогова, д.1 корпус 1',
            'value' => 'Чебоксары, ул. Пирогова, д.1 корпус 1'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Челябинск, ул. Советская, д. 38',
            'value' => 'Челябинск, ул. Советская, д. 38'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Череповец, ул. Ленина 58',
            'value' => 'Череповец, ул. Ленина 58'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Екатеринбург, ул. Студенческая 11',
            'value' => 'Екатеринбург, ул. Студенческая 11'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Геленджик, ул. Киевская, д. 45Б',
            'value' => 'Геленджик, ул. Киевская, д. 45Б'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Хабаровск, пер. Спортивный дом 4',
            'value' => 'Хабаровск, пер. Спортивный дом 4'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Иркутск, ул. Партизанская, д. 63',
            'value' => 'Иркутск, ул. Партизанская, д. 63'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Иваново, ул.Красногвардейская д.13/7',
            'value' => 'Иваново, ул.Красногвардейская д.13/7'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Ижевск, ул. Пушкинская, д. 116',
            'value' => 'Ижевск, ул. Пушкинская, д. 116'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Кемерово, ул. Терешковой, 41/6',
            'value' => 'Кемерово, ул. Терешковой, 41/6'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Курск, ул. Кати Зеленко 26',
            'value' => 'Курск, ул. Кати Зеленко 26'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Красноярск, ул. Авиаторов, д. 41',
            'value' => 'Красноярск, ул. Авиаторов, д. 41'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Казань, Рахимова 8',
            'value' => 'Казань, Рахимова 8'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Мурманск, Скальная, д. 4',
            'value' => 'Мурманск, Скальная, д. 4'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Новороссийск, пр. Ленина 95 Б корпус 1',
            'value' => 'Новороссийск, пр. Ленина 95 Б корпус 1'
        );
        $this->data['pickup_points'][] = array(
            'name' => 'Новосибирск, д. 50, ул. Светлановская, д. 50',
            'value' => 'Новосибирск, ул. Светлановская, д. 50'
        );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Санкт-Петербург, ул. Кантемировская, д. 37, МЦ "Мебель Сити 2"',
        //     'value' => 'Санкт-Петербург, ул. Кантемировская, д. 37, МЦ "Мебель Сити 2"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Санкт-Петербург, ул. Большая Пушкарская, д. 60',
        //     'value' => 'Санкт-Петербург, ул. Большая Пушкарская, д. 60'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Санкт-Петербург, ул. Полевая Сабировская, д. 54 А, ТВК "Интерио"',
        //     'value' => 'Санкт-Петербург, ул. Полевая Сабировская, д. 54 А, ТВК "Интерио"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Санкт-Петербург, ул. Шостаковича, д. 8/1 А, МЦ "Гранд Каньон"',
        //     'value' => 'Санкт-Петербург, ул. Шостаковича, д. 8/1 А, МЦ "Гранд Каньон"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Санкт-Петербург, ул. Варшавская, д. 3, корп 3, МЦ "Мебельный Континент"',
        //     'value' => 'Санкт-Петербург, ул. Варшавская, д. 3, корп 3, МЦ "Мебельный Континент"'
        // );
        $this->data['pickup_points'][] = array(
            'name' => 'Нижний Новгород, ул. Ларина, д. 7/3, ТЦ "Открытый Материк"',
            'value' => 'Нижний Новгород, ул. Ларина, д. 7/3, ТЦ "Открытый Материк"'
        );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Москва, ЮАО, ул. Ленинская Слобода, д. 26',
        //     'value' => 'Москва, ЮАО, ул. Ленинская Слобода, д. 26'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Москва, 1-й Щипковский переулок, д. 4',
        //     'value' => 'Москва, 1-й Щипковский переулок, д. 4'
        // );
        $this->data['pickup_points'][] = array(
            'name' => 'Химки, ул. Бутакова, д. 4, МЦ "Гранд 2"',
            'value' => 'Химки, ул. Бутакова, д. 4, МЦ "Гранд 2"'
        );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Белгород, ул. Белгородского полка, д. 62',
        //     'value' => 'Белгород, ул. Белгородского полка, д. 62'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Барнаул, пр. Космонавтов, д. 6 Г, ТВК "Республика"',
        //     'value' => 'Барнаул, пр. Космонавтов, д. 6 Г, ТВК "Республика"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Великий Новгород, ул. Большая Московская, д. 59',
        //     'value' => 'Великий Новгород, ул. Большая Московская, д. 59'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Волгоград, пр. Ленина, д. 65 К, ТЦ "Стройград"',
        //     'value' => 'Волгоград, пр. Ленина, д. 65 К, ТЦ "Стройград"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Вологда, Окружное шоссе, д. 11А, ТЦ "Мебель Маркет"',
        //     'value' => 'Вологда, Окружное шоссе, д. 11А, ТЦ "Мебель Маркет"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Геленджик, ул. Киевская, д. 45Б',
        //     'value' => 'Геленджик, ул. Киевская, д. 45Б'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Екатеринбург, МЦ Галерея 11, 5 этаж, ул. Студенческая, д. 11',
        //     'value' => 'Екатеринбург, МЦ Галерея 11, 5 этаж, ул. Студенческая, д. 11'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Иваново, ул. Красногвардейская д. 13/7',
        //     'value' => 'Иваново, ул. Красногвардейская д. 13/7'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Ижевск, ул. Пушкинская, д. 116, ТЦ "Пушкинская 116"',
        //     'value' => 'Ижевск, ул. Пушкинская, д. 116, ТЦ "Пушкинская 116"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Иркутск, ул. Партизанская, д. 63',
        //     'value' => 'Иркутск, ул. Партизанская, д. 63'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Кемерово, ул. Терешковой, д. 41/6 ТВК «Сити Дом»',
        //     'value' => 'Кемерово, ул. Терешковой, д. 41/6 ТВК «Сити Дом»'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Красноярск, ул. Авиаторов, д. 41',
        //     'value' => 'Красноярск, ул. Авиаторов, д. 41'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Курск, ул. Кати Зеленко, д. 26',
        //     'value' => 'Курск, ул. Кати Зеленко, д. 26'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Мурманск, ул. Скальная, д. 4',
        //     'value' => 'Мурманск, ул. Скальная, д. 4'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Мурманск, ул. Воровского, д. 17',
        //     'value' => 'Мурманск, ул. Воровского, д. 17'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Нижний Тагил, ул. Октябрьской революции, д. 66, ЦУМ',
        //     'value' => 'Нижний Тагил, ул. Октябрьской революции, д. 66, ЦУМ'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Новороссийск, ул. Пионерская, д. 23 А, МЦ "Дриада"',
        //     'value' => 'Новороссийск, ул. Пионерская, д. 23 А, МЦ "Дриада"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Новосибирск, ул. Светлановская, д. 50, ТВК "Большая Медведица"',
        //     'value' => 'Новосибирск, ул. Светлановская, д. 50, ТВК "Большая Медведица"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Новый Уренгой, ул. Промысловая, д. 2, ТВЗ "Опилки"',
        //     'value' => 'Новый Уренгой, ул. Промысловая, д. 2, ТВЗ "Опилки"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Орел, ул. 8 марта, д. 8',
        //     'value' => 'Орел, ул. 8 марта, д. 8'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Первоуральск, ул. Данилова, д. 2-а',
        //     'value' => 'Первоуральск, ул. Данилова, д. 2-а'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Пермь, ул. Чернышевского, д. 28, Галерея Мебели "АртЭго"',
        //     'value' => 'Пермь, ул. Чернышевского, д. 28, Галерея Мебели "АртЭго"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Самара, Московское шоссе 16 км, здание 1 В, корпус 2',
        //     'value' => 'Самара, Московское шоссе 16 км, здание 1 В, корпус 2'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Саратов, ул. Большая Горная, д. 336, ТЦ "Viva Decor"',
        //     'value' => 'Саратов, ул. Большая Горная, д. 336, ТЦ "Viva Decor"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Сочи, ТЦ "Эксперт Хоум", ул. Виноградная, д. 55/1',
        //     'value' => 'Сочи, ТЦ "Эксперт Хоум", ул. Виноградная, д. 55/1'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Старый Оскол, мкр. Восточный, д. 13, Мебельный салон Нефертити',
        //     'value' => 'Старый Оскол, мкр. Восточный, д. 13, Мебельный салон Нефертити'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Сургут, пр. Ленина, д. 23',
        //     'value' => 'Сургут, пр. Ленина, д. 23'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Уфа, ул. Менделеева, д. 158, ВДНХ ЭКСПО',
        //     'value' => 'Уфа, ул. Менделеева, д. 158, ВДНХ ЭКСПО'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Хабаровск, Восточное шоссе, д. 16, Авто-сити',
        //     'value' => 'Хабаровск, Восточное шоссе, д. 16, Авто-сити'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Чебоксары, ул. Пирогова, д. 1 корпус 1, Салон Финской мебели "Pohjanmaan"',
        //     'value' => 'Чебоксары, ул. Пирогова, д. 1 корпус 1, Салон Финской мебели "Pohjanmaan"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Челябинск, МЦ "На Чичерина", ул. Чичерина, д. 22, секция 45',
        //     'value' => 'Челябинск, МЦ "На Чичерина", ул. Чичерина, д. 22, секция 45'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Череповец, ул. К.Белова, д. 36 , "Салон финской мебели "Pohjanmaan"',
        //     'value' => 'Череповец, ул. К.Белова, д. 36 , "Салон финской мебели "Pohjanmaan"'
        // );
        // $this->data['pickup_points'][] = array(
        //     'name' => 'Южно-Сахалинск, ул. Дзержинского, д. 40/1',
        //     'value' => 'Южно-Сахалинск, ул. Дзержинского, д. 40/1'
        // );

        // -------------------------------------------

        // if ($this->city->getCityId() == 'sam') {
        //     $this->data['pickup_points'][] = array(
        //         'name' => 'Санкт-Петербург, Чугунная, 14',
        //         'value' => 'Санкт-Петербург, Чугунная, 14'
        //     );
        //     $this->data['pickup_points'][] = array(
        //         'name' => 'Химки, Московская обл, Бутаково ул, 4',
        //         'value' => 'Химки, Московская обл, Бутаково ул, 4'
        //     );
        //     $this->data['pickup_points'][] = array(
        //         'name' => 'Пермь, Пермский край, ул. Чернышевского, д. 28',
        //         'value' => 'Пермь, Пермский край, ул. Чернышевского, д. 28'
        //     );
        // } else if ($this->city->getCityId() == 'spb') {
        //     $this->data['pickup_points'][] = array(
        //         'name' => 'Санкт-Петербург, Чугунная, 14',
        //         'value' => 'Санкт-Петербург, Чугунная, 14'
        //     );
        //     $this->data['pickup_points'][] = array(
        //         'name' => 'Химки, Московская обл, Бутаково ул, 4',
        //         'value' => 'Химки, Московская обл, Бутаково ул, 4'
        //     );
        //     $this->data['pickup_points'][] = array(
        //         'name' => 'Пермь, Пермский край, ул. Чернышевского, д. 28',
        //         'value' => 'Пермь, Пермский край, ул. Чернышевского, д. 28'
        //     );

        // } else {
        //     $this->data['pickup_points'][] = array(
        //         'name' => 'Санкт-Петербург, Чугунная, 14',
        //         'value' => 'Санкт-Петербург, Чугунная, 14'
        //     );
        //     $this->data['pickup_points'][] = array(
        //         'name' => 'Химки, Московская обл, Бутаково ул, 4',
        //         'value' => 'Химки, Московская обл, Бутаково ул, 4'
        //     );
        //     $this->data['pickup_points'][] = array(
        //         'name' => 'Пермь, Пермский край, ул. Чернышевского, д. 28',
        //         'value' => 'Пермь, Пермский край, ул. Чернышевского, д. 28'
        //     );
        // }

        $this->data['data_submission_url'] = $this->url->link('checkout/order_form/submit', '', 'SSL');
        $this->data['policy_link'] = 'personal-data-processing-agreement';

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/order_form.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/checkout/order_form.tpl';
        } else {
            $this->template = 'default/template/checkout/order_form.tpl';
        }

        // return $this->load->view('checkout/order_form', $this->data);
        $this->response->setOutput($this->render());
    }

    function submit()
    {
        if (!$this->cart->hasProducts()) {
            $this->response->setOutput(
                $this->url->link('checkout/cart')
            );
        } else {
            $this->load->model('checkout/order');

            $orderData = array(
                'lastname' => $_POST['lastname'],
                'middlename' => $_POST['middlename'],
                'firstname' => $_POST['firstname'],
                'email' => $_POST['email'],
                'telephone' => $_POST['telephone'],
                'comment' => $_POST['comment'],
                'order_pay' => $_POST['order_pay'],
                'total' => $this->getTotalOrderPrice(),
                'shipping_method' => $_POST['shipping_method'],
                'pickup_point' => $_POST['pickup_point'],
                'street' => $_POST['street'],
                'subway' => $_POST['subway'],
                'postal_code' => $_POST['postal_code'],
                'house_number' => $_POST['house_number'],
                'building_number' => $_POST['building_number'],
                'building_entrance' => $_POST['building_entrance'],
                'building_floor' => $_POST['building_floor'],
                'flat' => $_POST['flat'],
                'products' => $this->getOrderedProducts()
            );

            $order_id = $this->model_checkout_order->addOrder($orderData);
            $this->session->data['previous_order_id'] = $order_id;

            $this->cart->clear();
            $this->response->setOutput(
                $this->url->link('checkout/order_created')
            );
        }
    }

    private function getOrderedProducts()
    {
        $products = array();

        foreach ($this->cart->getProducts() as $product) {
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));

                $price = $this->currency->format($unit_price, $this->session->data['currency']);
                $total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
            } else {
                $price = false;
                $total = false;
            }

            $products[] = array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'image' => $product['image'],
                'model' => $product['model'],
                'quantity' => $product['quantity'],
                'price' => $product['price'],
                'total' => $product['price'] * $product['quantity']
            );
        }

        return $products;
    }

    private function getTotalOrderPrice()
    {
        $total_price = 0;

        foreach ($this->cart->getProducts() as $product) {
            $total_price += $product['price'] * $product['quantity'];
        }

        return $total_price;
    }
}
