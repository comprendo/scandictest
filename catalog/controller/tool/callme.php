<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'Exception.php';
require 'PHPMailer.php';
require 'SMTP.php';

$name = $_REQUEST['name'];
$phone = $_REQUEST['phone'];

$mail = new PHPMailer(true);

try {
    //Recipients
    $mail->CharSet = "UTF-8";
    $mail->setFrom('scandicstyle@yandex.ru', 'Scandic Style');
    $mail->addAddress('kanjon@scandicstyle.ru');

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Обратный звонок (Scandic Style)';
    $mail->Body    = 'Имя: ' . $name . '<br>Телефон: <a href="tel:' . $phone . '">' . $phone . '</a>';

    $mail->send();
    echo 'Ваше сообщение отправлено. Мы перезвоним в течении 10 минут.';
} catch (Exception $e) {
    echo "При отправке сообщение произошла ошибка. Попробуйте еще раз позже.";
}
