<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'Exception.php';
require 'PHPMailer.php';
require 'SMTP.php';

$name = $_REQUEST['name'];
$phone = $_REQUEST['phone'];
$product = $_REQUEST['product'];
$link = $_REQUEST['link'];
$price = $_REQUEST['price'];

$mail = new PHPMailer(true);

try {
    //Recipients
    $mail->CharSet = "UTF-8";
    $mail->setFrom('scandicstyle@yandex.ru', 'Scandic Style');
    $mail->addAddress('kanjon@scandicstyle.ru');

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Заказ в один клик (Scandic Style)';
    $mail->Body    = 'Имя: ' . $name . '<br>Телефон: <a href="tel:' . $phone . '">' . $phone . '</a><br>Товар: <a href="' . $link . '">' . $product . '</a><br>Цена: ' . $price;

    $mail->send();
    echo 'Ваше сообщение отправлено. Мы перезвоним в течении 10 минут.';
} catch (Exception $e) {
    echo "При отправке сообщение произошла ошибка. Попробуйте еще раз позже.";
}
