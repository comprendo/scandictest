<?php

class ControllerCommonCity extends Controller
{
    public function index()
    {
        $cityId = $this->city->getCityId();
        $this->load->language('common/city');

        // specify selected city
        $this->data['selected_city'] = $cityId;
        // compute set city action link
        $this->data['action'] = $this->url->link('common/city/city', '',
            isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1')));
        // load cities
        $this->data['cities'] = array();
        foreach ($this->city->getCities() as $k => $v) {
            $this->data['cities'][$k] = array('name' => $v);
        }

        if (!isset($this->request->get['route'])) {
            $this->data['redirect'] = $this->url->link('common/home');
        } else {
            $url_data = $this->request->get;
            unset($url_data['_route_']);
            $route = $url_data['route'];
            unset($url_data['route']);
            $url = '';
            if ($url_data) {
                $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            }
            $this->data['redirect'] = $this->url->link($route, $url, isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1')));
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/city.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/common/city.tpl';
        } else {
            $this->template = 'default/template/common/city.tpl';
        }

        // return $this->load->view('common/city', $this->data);
        $this->render();
    }

    public function city()
    {
        if (isset($this->request->post['cityId'])) { // set selected city id
            $this->city->setCityId($this->request->post['cityId']);
        }

        if (isset($this->request->post['redirect'])) {
            $this->response->redirect($this->city->fixUrlForCitySpecificPages($this->request->post['redirect']));

        } else {
                        $this->response->redirect($this->url->link('common/home'));
            //$this->response->redirect($this->city->fixUrlForCityBlogPages($this->request->post['redirect']));

        }
    }
}