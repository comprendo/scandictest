<?php 
class ControllerInformationDelivery extends Controller {
	public function index() {  
    	$this->language->load('information/delivery');
		
		$this->data['breadcrumbs'] = array();
		
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	);

  		// $this->data['breadcrumbs'][] = array(
    	// 	'text'      => $this->language->get('heading_title'),
		// 	'href'      => $this->url->link('information/delivery'),      		
    	// 	'separator' => $this->language->get('text_separator')
  		// );	
		
		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setDescription($information_info['meta_description']);
		$this->document->setKeywords($information_info['meta_keyword']);

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['address'] = $this->config->get('config_address');
		$this->data['email'] = $this->config->get('config_email');
		$this->data['telephone'] = $this->config->get('config_telephone');
		$this->data['working_time'] = $this->config->get('config_fax');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/delivery.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/information/delivery.tpl';
		} else {
			$this->template = 'default/template/information/delivery.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);
					
  		$this->response->setOutput($this->render());
  	}
}
