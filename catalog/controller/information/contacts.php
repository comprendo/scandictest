<?php
class ControllerInformationContacts extends Controller
{
    public function index()
    {
        $this->language->load('information/contacts');

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home'),
            'separator' => false
        );

        // $this->data['breadcrumbs'][] = array(
        // 	'text'      => $this->language->get('heading_title'),
        // 	'href'      => $this->url->link('information/contacts'),      		
        // 	'separator' => $this->language->get('text_separator')
        // );	

        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->setDescription($information_info['meta_description']);
        $this->document->setKeywords($information_info['meta_keyword']);

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['address'] = $this->config->get('config_address');
        $this->data['email'] = $this->config->get('config_email');
        $this->data['telephone'] = $this->config->get('config_telephone');
        $this->data['working_time'] = $this->config->get('config_fax');

        $this->data['additional_contacts'] = array();

        if ($_SERVER['HTTP_HOST'] == 'mr1.scandicstyle.ru') {
            $this->data['additional_contacts'] = array(
                'mebel_city' => array(
                    'name' => 'Scandic Style',
                    'address' => 'ул. Воровского, д. 17',
                    'work_time' => '11:00-19:00',
                    'email' => 'mr1@scandicstyle.ru'
                )
            );

            $this->data['map_script'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A118d1e6a568d99eb96849ca80836a82c26890341e974767150defab6a2c4ab19&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }

        if ($_SERVER['HTTP_HOST'] == 'scandicstyle.ru') {
            $this->data['additional_contacts'] = array(
                'scandicstyle' => array(
                    'name' => 'Интерьерный салон',
                    'address' => 'Большая Пушкарская улица, 60',
                    'work_time' => '11:00-21:00',
                    'email' => 'zakaz@scandicstyle.ru'
                ),
                'scandicstyle2' => array(
                    'name' => 'Оптовый склад',
                    'address' => 'Чугунная улица, 14',
                    'work_time' => '9:00-20:00',
                    'email' => 'zakaz@scandicstyle.ru'
                ),
                'scandicstyle3' => array(
                    'name' => 'МЦ Гранд Каньон',
                    'address' => 'улица Шостаковича, 8к1, 1 этаж',
                    'work_time' => '11:00-21:00',
                    'email' => 'kanjon@scandicstyle.ru'
                ),
                'scandicstyle4' => array(
                    'name' => 'МЦ Кубатура',
                    'address' => 'улица Фучика, 9, стр. А, 1 этаж',
                    'work_time' => '11:00-18:00',
                    'email' => 'kubatura@scandicstyle.ru'
                )
            );

            $this->data['map_script'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A0f551a24907fd0c8420a1d810bf28fe7446dd5af46ce3d41f5209f4996f63ef5&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        
        if ($_SERVER['HTTP_HOST'] == 'msk.scandicstyle.ru') {
            $this->data['additional_contacts'] = array(
                'scandicstyle' => array(
                    'name' => 'Салон Pohjanmaan МЦ "Roomer"',
                    'address' => 'ул. Ленинская слобода, д.26, 3 этаж, секция 342',
                    'work_time' => '10:00-22:00',
                    'email' => 'roomer@scandicstyle.ru'
                ),
                'scandicstyle2' => array(
                    'name' => 'Салон Pohjanmaan в МЦ "ГРАНД 2"',
                    'address' => 'г. Химки, ул. Бутакова, д. 4, 2 этаж',
                    'work_time' => '10:00-21:00',
                    'email' => 'grand@scandicstyle.ru'
                )
            );
            
            $this->data['map_script'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A0b5ea34dea5645a8fb3130f8e9e0a60a68e11dc63b08c3e083a26d6cb917c6ba&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        

        if ($_SERVER['HTTP_HOST'] == 'scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A0f551a24907fd0c8420a1d810bf28fe7446dd5af46ce3d41f5209f4996f63ef5&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'arh.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aece0040c257ff26a3494804fe9c4ce14daa25d22bcd760b18d3a6f0d7e70f947&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'blg.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A27cca05260fcaf1302eaaabc824bb25e9720a0bf085180ffc81ccdb5a8c80075&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'brn.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A88b06733e917428fc88da574a2ed363eb61abe8ebabe8b393f54477a86b759a7&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'cbk.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A19ee3050a9e37073c01d2b6369122c7110115bedbc05792282350b59b773dd9d&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'chb.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Af00eeb5e4a8285a8ece9e011856b77534becd76e0f1dd09adb0c9b58f65ab2c1&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'crk.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A5279998f7b3d374c74a72faf881601d5067d5a9ba6d0a31f6e1417a8b5ca7615&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'ekb.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Adf8ba8ea1f7828ab690e626c94c3417aabcb69a4293c38ec75e0f27da612ed5a&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'glz.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A101a028c17ebcb3107557e237b227d7d74f23c3a55b30558b274399c683d1773&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'hbk.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A8f39ff235f89feed028a55a8825bb148924790af218e9fff0b21ac2125a2a5f4&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'irk.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A8c7314f41c2abe8660f1925414f63eabb1b5c1a4184c023611d59c7708ad3342&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'ivn.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa46c7cd29f4803a2785f576fd2339308ff26d0f087776356c0896dce6da09fcb&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'izh.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Acee29318451849e274510a85a9a3ce1bfb5b79de624cfe1b34c24e50346a33dd&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'kmr.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A46307968efbeaa8646cbb114f8eb5992d2139401cf3968bcdea4033ad1eb4ae7&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'krk.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A70310373acca6ca6fd4e317a6b6fe7e51c2b2747aa34a54a9dea3017ae652caa&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'ksk.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A9feb4f5148a700f3863937db5bf67e9ffcb6b2e323bb3a8b2196a1169da90eac&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'kzn.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ab5537b5bb2a288c69c58cf2e5d090c7ab0337caea263f49b5f1575463c41bf62&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'mns.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A3578812554e422b4acfa6664df71963bb59d3e13802a3187a352c177ed2a2d2f&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'mr1.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A7dfc884bd52105d6217b3fc0b57d51c317e5d6776b32203f6883ace930e51479&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'msk.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aaac131db52cb5411018e7c7d0c19846e8522680ba81c1f133f20526b30fafc19&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'nvr.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Afd4b30b495aeaba44002e8ea971cd501bfbd507a235961b824a166103f3f63ff&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'nvs.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Af8642d8a4640f3ae56863d2ba7d7bae3992d7fbbd57dc486888112cba8a8f3dd&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'nzt.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Af941448a1558663f18ebf370d35fb654936c3485e2b6bc7f8486ef1046726a87&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'orl.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A134d354bc2cd72e2933c59f43b733b93cf1df8fa9d056da4500f15666e6618f4&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'prm.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A505c294e350622f529ec0774133b908bf1b9ad09d21f581ec84e9d79315959c0&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'ptg.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A79c847192d92eb69e45cfd3be785cbfce45744ba6b66bb0835cceb94ae632428&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'sam.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A2b0d650ec9ef7699cc04a3865b82d4865e414e059631a61a8b314e5a3e8f787f&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'sar.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ae56f0305da176861ec528daf5bd30a542be6893f01d5f2fc660e8c753db0abb6&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'srt.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Af0248350ac516f53ac8ba8417fa30f7bbf558fb1faf27ca184e2a7413f292992&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'sto.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A7651f84f0678e0622408353268911e4655297e5c5adc2dcced931bb3b00e7d5f&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'tmk.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A96a40c8c502a455058e8cebd2731f4c44658b5e4328125710217a82d6b01355b&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'tmn.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A68bee3414a84026a73ade886ef1fe5ceb5000fa623890bb2c1f8e515ab883fb6&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'ufa.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A28c2d2eb98972f2d19198b0a30476ea800da03c539d02a7242bd68deaeb13b38&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'vlg.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A59c7b697eba5398645a5a436887a91fe107fb9cff1b9cbb78d368d2cc9755521&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'vol.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A5b42d0576a49bc2fa015afe0774cc49f2d29798b77b1b9bf0762aa5ced1466d4&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'mnk.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ac6acde1d356c227555c6cdb1d9a496e7dc540a58934118d08634d8182cfc7365&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'bel.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A2bc8fae3de9a2bc76d8e39b61caddf108fbf1171037d4646f8782696e00e32fe&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'rst.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A16665e208701b2920a4a25e5e91e5e6bd5e403a7466afff75d275196e5ed2ea3&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }
        if ($_SERVER['HTTP_HOST'] == 'nnv.scandicstyle.ru') {
            $this->data['map'] = '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A2221a438b9953606636aff9c90db3bae36fb4179e7948df13598b66188d8b2af&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>';
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/contacts.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/information/contacts.tpl';
        } else {
            $this->template = 'default/template/information/contacts.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }
}
