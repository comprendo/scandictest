class Timer {
    constructor(name, date) {
        this.timerName = name;
        this.endDate = date;
    }

    initializeTimer() {
        var currentDate = new Date();
        var seconds = (this.endDate - currentDate) / 1000;
        var timerId;
        var timerName = this.timerName;

        if (seconds > 0) {
            var minutes = seconds / 60;
            var hours = minutes / 60;
            var days = hours / 24;
            minutes = (hours - Math.floor(hours)) * 60;
            days = Math.floor(days);
            hours = Math.floor(hours) - days * 24;

            seconds = Math.floor((minutes - Math.floor(minutes)) * 60);
            minutes = Math.floor(minutes);

            setTimePage(timerName, days, hours, minutes, seconds);

            function secOut() {
                if (seconds == 0) {
                    if (minutes == 0) {
                        if (hours == 0) {
                            if (days == 0) {
                                showMessage(timerId);
                            } else {
                                days--;
                                hours = 24;
                                minutes = 59;
                                seconds = 59;
                            }
                        } else {
                            hours--;
                            minutes = 59;
                            seconds = 59;
                        }
                    } else {
                        minutes--;
                        seconds = 59;
                    }
                } else {
                    seconds--;
                }
                setTimePage(timerName, days, hours, minutes, seconds);
            }
            timerId = setInterval(secOut, 1000);
        } else {
            var timerRemove = document.querySelectorAll('.' + timerName + ' div');

            var timer = document.querySelector('.' + timerName);

            timerRemove.forEach(function (elem) {
                elem.remove();
            });

            timer.innerHTML = '<div id="stock-ended"><span>Акция окончена!</span></div>';
        }
    }
}

function setTimePage(timerClass, d, h, m, s) {
    var days = document.querySelector("." + timerClass + " div #days");
    var hours = document.querySelector("." + timerClass + " div #hours");
    var minutes = document.querySelector("." + timerClass + " div #minutes");
    var seconds = document.querySelector("." + timerClass + " div #seconds");

    days.innerHTML = d;
    hours.innerHTML = h;
    minutes.innerHTML = m;
    seconds.innerHTML = s;
}

window.onload = function () {
    var recliners = new Timer('recliners', new Date(2020, 7, 31));
    recliners.initializeTimer();

    var module = new Timer('module', new Date(2020, 6, 31));
    module.initializeTimer();

    var welcome = new Timer('welcome', new Date(2020, 3, 30));
    welcome.initializeTimer();

    var blackfriday = new Timer('blackfriday', new Date(2020, 10, 30));
    blackfriday.initializeTimer();
    
    var yanvar = new Timer('yanvar', new Date(2021, 0, 31));
    yanvar.initializeTimer();
}