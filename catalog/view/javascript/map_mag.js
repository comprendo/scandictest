function initialize() {
    //настройка карты
  var map_options = {
     //уровень увеличения
    zoom: 7,
    //центрируем в СПБ
    center: new google.maps.LatLng(59.990745,30.291595),
    //тип карты
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map_options2 = {
     //уровень увеличения
    zoom: 3,
    //центрируем в Москва
    center: new google.maps.LatLng(55.754941,37.619476),
    //тип карты
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map_options3 = {
     //уровень увеличения
    zoom: 3,
    //центрируем в Самара
    center: new google.maps.LatLng(53.233989,50.164719),
    //тип карты
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  //показываем карту в элементе с id="map"
  var map = new google.maps.Map(document.getElementById("map"), map_options);
  var map2 = new google.maps.Map(document.getElementById("map2"), map_options2);
  var map3 = new google.maps.Map(document.getElementById("map3"), map_options3);
//распределяем маркеры магазинов
  set_markers(map, shops);
  set_markers(map2, shops2);
  set_markers(map3, shops3);
}


//массив магазинов, название, широта, долгота, индекс если маркеры перекрываются полезный, описание для инфоокна
var shops = [
 ['ул. Бабушкина', 59.874563,30.445352, 1,'<span style="color: #1e92cf;">VESTA</span><br />Санкт-Петербург, ул. Бабушкина, д.77<br>Пн-Пт: 9-20; Сб: 10-20; Вс: 11-18<br /><span style="color: #1e92cf;">+7 (812) 244-46-53</span>'],
 ['Колтушское шоссе', 60.034116,30.637325, 1,'<span style="color: #1e92cf;">VESTA</span><br />Санкт-Петербург, Всеволожск, Колтушское шоссе, д.39<br>Пн-Пт: 9-20; Сб: 9-19; Вс: 11-18<br /><span style="color: #1e92cf;">(81370) 31-444</span>'],
 ['Шлиссельбургский пр', 59.835883,30.510561, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Санкт-Петербург Шлиссельбургский пр., 24,корп.1<br>Пн-Пт: 9-20; Сб: 10-20; Вс: 11-18<br /><span style="color: #1e92cf;">(812) 244-1745</span>'],
 ['Славы пр., д.15', 59.854788,30.374501, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Санкт-Петербург Славы пр., д.15<br>Пн-Пт: 9-20; Сб: 9-19; Вс: 9-18 <br /><span style="color: #1e92cf;">(812) 244-1734 </span>'],
 ['пр. Большевиков', 59.912536,30.475881, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Санкт-Петербург пр. Большевиков д.11, к.2<br>Пн-Пт: 9-20; Сб: 9-19; Вс: 11-18<br /><span style="color: #1e92cf;">(812) 244-1735</span>'],
 ['Луначарского пр', 60.043867,30.324073, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Санкт-Петербург Луначарского пр., д.7, корп. 1<br>Пн-Пт: 9-20; Сб: 9-19; Вс: 11-18<br /><span style="color: #1e92cf;">(812) 244-1729</span>'],
 ['Б.Пороховская ул', 59.953248,30.418105, 1, '<span style="color: #1e92cf;">VESTA</span><br />Санкт-Петербург, Б.Пороховская ул, д.25<br>Пн-Сб: 9-20; Вс: 9-18<br /><span style="color: #1e92cf;">(812) 244-1746</span>'],
 ['Гражданский пр', 60.019558,30.403007, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Санкт-Петербург Гражданский пр, д.92, корп.1<br>Пн-Пт: 9-20; Сб: 9-19; Вс: 11-18<br /><span style="color: #1e92cf;">(812) 244-4795</span>'],
 ['Малоохтинский пр', 59.9351320,30.4033319, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Санкт-Петербург Малоохтинский пр-кт., д.36<br>Пн-Пт: 9-18; Сб-Вс: выходной<br /><span style="color: #1e92cf;">(812) 244-244-1</span>'],
 ['Пулковская', 59.834963,30.350206, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Санкт-Петербург Пулковская ул.,д.19<br>Пн-Пт: 9-20; Сб: 10-20; Вс: 11-18<br /><span style="color: #1e92cf;">(812) 244-6326</span>'],
 ['ул. Детскосельская', 59.68828,30.432165, 1, '<span style="color: #1e92cf;">VESTA</span><br />Санкт-Петербург, г. Павловск, ул. Детскосельская д.6<br>Пн-Сб: 9-20; Вс: 9-18<br /><span style="color: #1e92cf;">(812) 244-1739</span>'],
 ['ул. Воскова', 59.957739,30.307324, 1, '<span style="color: #1e92cf;">VESTA</span><br />Санкт-Петербург, ул. Воскова, дом 15<br>Пн-Пт: 9-20; Сб: 9-19; Вс: 11-18<br /><span style="color: #1e92cf;">(812) 244-2351</span>'],
 ['пр. Сизова', 60.014162,30.277963, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Санкт-Петербург пр. Сизова 21 к.2<br>Пн-Пт: 9-20; Сб: 9-19; Вс: 11-18<br /><span style="color: #1e92cf;">(812) 244-1742</span>'],
 ['Ленинский пр', 59.851384,30.251556, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Санкт-Петербург Ленинский пр., д.117, корп.1<br>Пн-Пт: 9-20; Сб: 10-20; Вс: 11-18<br /><span style="color: #1e92cf;">(812) 244-1736</span>'],
 ['Ленина пр', 59.738152,30.088776, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Санкт-Петербург, Красное Село, Ленина пр., 59<br>Пн-Пт: 9-20; Сб: 10-20; Вс: 11-18<br /><span style="color: #1e92cf;">(812) 244-1728</span>'],
 ['Ленинский пр', 59.854556,30.207281, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Санкт-Петербург Ленинский пр., д.91<br>Пн-Пт: 9-20; Сб: 10-20; Вс: 11-18<br /><span style="color: #1e92cf;">(812) 244-1737</span>']
];
var shops2 = [
 ['Cело Мамоново ул .Колхозная', 55.687237,37.31606, 1, '<span style="color: #1e92cf;">VESTA</span><br />Одинцовский район село Мамоново ул .Колхозная д.89А<br>Пн-Сб: 9-19; Вс: 9-17<br /><span style="color: #1e92cf;">(495) 544-97-60; (926)094-76-02</span>'],
 ['г. Мытищи, Ярославское шоссе', 55.91005,37.779315, 1, '<span style="color: #1e92cf;">VESTA</span><br />г. Мытищи, Ярославское шоссе, д.114/1А<br> Пн-Сб: 9-19; Вс: 9-17<br /><span style="color: #1e92cf;">(495) 544-97-92; (926)915-41-32</span>'],
 ['г. Люберцы, ул. Инициативная', 55.686471,37.898681, 1, '<span style="color: #1e92cf;">VESTA</span><br />г. Люберцы, Инициативная ул, дом № 7Б<br>Пн-Сб: 9-19; Вс: 9-17<br /><span style="color: #1e92cf;">(495) 580-58-57; (926)926-76-15</span>'],
 ['г. Подольск ул. Кирова', 55.424312,37.522145, 1, '<span style="color: #1e92cf;">VESTA</span><br />г. Подольск ул. Кирова, дом 60<br>Пн-Пт: 9:30-19; Сб: 10-18; Вс: 10-16<br /><span style="color: #1e92cf;">(495) 669-28-45; (926)915-41-15</span>']
];
var shops3 = [
 ['г.Самара Энтузиастов ул', 53.201660,50.200086, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Самара Энтузиастов ул, д.27<br>Пн-Пт: 9-19; Сб: 10-17; Вс: 10-14<br /><span style="color: #1e92cf;">(846) 979-74-29</span>'],
 ['г.Самара Масленникова пр', 53.210606,50.157383, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Самара Масленникова пр, дом.14<br>Пн-Пт: 9-19; Сб: 10-17; Вс: 10-14<br /><span style="color: #1e92cf;">(846) 201-18-03</span>'],
 ['г.Самара Победы ул', 53.222505,50.270752, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Самара Победы ул., д.125<br>Пн-Пт: 9-19; Сб: 10-17; Вс: 10-14<br /><span style="color: #1e92cf;">(846) 993-49-34</span>'],
 ['Ново Вокзальная', 53.241886,50.209466, 1, '<span style="color: #1e92cf;">VESTA</span><br />г.Самара Ново Вокзальная ул. д.146 А<br>Пн-Пт: 9-19; Сб: 10-17; Вс: 10-14<br /><span style="color: #1e92cf;">(846) 993-49-35</span>'] 
];

//функция для добавления маркеров из массива
function set_markers(map, locations) {

  //картинка маркера
  var image = new google.maps.MarkerImage('http://www.vestatrade.ru/flag.png',
      //размер маркера
      new google.maps.Size(32, 30),
      //точка на маркера для карты
      new google.maps.Point(0,0),
      //точка на маркере дле инфоокна
      new google.maps.Point(0, 32));
  //картинка тени маркера
  var shadow = new google.maps.MarkerImage('http://www.vestatrade.ru/flag.png',
      //размер тени
      new google.maps.Size(32, 30),
      new google.maps.Point(0,0),
      new google.maps.Point(0, 32));
  
  //объект для центрирования по всем маркерам
  var point_bound = new google.maps.LatLngBounds();
  
  //цикл по всем магазинам
  for (var i = 0; i < locations.length; i++) {
      
    var shop = locations[i];
    //точка для маркера
    var myLatLng = new google.maps.LatLng(shop[1], shop[2]);
    //добавляем точку в объект центрирования
    point_bound.extend(myLatLng);
    //создаём объект маркера и добавляем на карту
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        shadow: shadow,
        icon: image,
        title: shop[0],
        zIndex: shop[3]
    });
    //данные инфоокна для каждого маркера
    marker.infowindow_content = shop[4];
    //клик на маркере открывает инфоокно
    marker.infowindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marker, 'click', function() {
        this.infowindow.open(map, this);
        this.infowindow.setContent(this.infowindow_content);
    });
    
  }
  //изменяем карту по всем точкам, чтоб на карте были видны все маркеры
  map.fitBounds(point_bound);
}

$(function() {
    var tab = $('#tabs .tabs-items > div'); 
    tab.hide().filter(':first').show(); 
    
    // Клики по вкладкам.
    $('#tabs .tabs-nav a').click(function(){
        tab.hide(); 
        tab.filter(this.hash).show(); 
        $('#tabs .tabs-nav a').removeClass('active');
        $(this).addClass('active');
        initialize();
        return false;
    }).filter(':first').click();
    

    // Клики по якорным ссылкам.
    $('.tabs-target').click(function(){
        $('#tabs .tabs-nav a[href=' + $(this).data('id')+ ']').click();
    });
});