function getURLVar(key) {
    var value = [];

    var query = document.location.search.split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

$(document).ready(function () {
    var isScrolled = false;
    var isMenuOpen = false;
    var isWhite = false;

    /* Mobile Header Menu Background */
    $('.navbar-toggle').click(function () {
        if ($(this).hasClass('collapsed')) {
            $('#nav__basemenu').addClass('fill');
            $('.main-nav').removeClass('white');
            $('body').addClass('no-scroll');
            isMenuOpen = true;
        } else if (!isScrolled) {
            $('#nav__basemenu').removeClass('fill');
            $('body').removeClass('no-scroll');
            isMenuOpen = false;
        } else {
            $('body').removeClass('no-scroll');
            isMenuOpen = false;
        }
    });

    /* Fixed Header Background */
    $(document).on('scroll', function () {
        if ($(this).scrollTop() > 30 && !isMenuOpen) {
            $('#nav__basemenu').addClass('fill');
            $('.main-nav').removeClass('white');
            isScrolled = true;
        } else if (isWhite && !isMenuOpen) {
            $('#nav__basemenu').removeClass('fill');
            $('.main-nav').addClass('white');
            isScrolled = false;
        } else if (!isMenuOpen) {
            $('#nav__basemenu').removeClass('fill');
            isScrolled = false;
        }
    });

    /* Search */
    $('.button-search').bind('click', function () {
        url = $('base').attr('href') + 'search/';

        var filter_name = $('input[name=\'filter_name\']').val();

        if (filter_name) {
            url += encodeURIComponent(filter_name);
        }

        location = url;
    });

    $('.search-block input[name=\'filter_name\']').bind('keydown', function (e) {
        if (e.keyCode == 13) {
            url = $('base').attr('href') + 'search/';

            var filter_name = $('input[name=\'filter_name\']').val();

            if (filter_name) {
                url += encodeURIComponent(filter_name);
            }

            location = url;
        }
    });

    $('#selected-city-span').html($('#store option:selected').text());
    $('#city-confirm__selected-city').html($('#store option:selected').text());
    $('#store').select2({
        width: '100%',
        minimumResultsForSearch: Infinity
    });

    function fixedFooter() {
        if ($('body').hasClass('common-home')) {
            return;
        }

        if ($('#hf-container').outerHeight() + 50 < $(window).height() - $('footer').outerHeight()) {
            $('footer').addClass('fixed-bottom');
        } else {
            $('footer').removeClass('fixed-bottom');
        }
    }

    $(window).on('resize load', function () {
        fixedFooter();
    });

    $('body').on('click', '.notification-close', function () {
        var notification = $('.add-to-cart-notification');
        notification.stop().fadeOut(300, function () {
            notification.remove();
        });
    });

    // Ajax Call Me
    $('#callme-submit').click(function () {
        var name = $('#callme-name').val();
        var phone = $('#callme-phone').val();

        if (name != "" && phone != "") {
            $.ajax({
                type: 'POST',
                url: '/catalog/controller/tool/callme.php',
                data: {
                    name: name,
                    phone: phone
                }
            }).done(function (data) {
                $('.callme-result').html(data);
            })
        } else {
            $('.callme-result').html('<span style="color: red;">Пожалуйста, заполните все поля!</span>');
        }
    });

    // Ajax Buy in one click
    $('#buyinoneclick-submit').click(function () {
        var name = $('#buyinoneclick-name').val();
        var phone = $('#buyinoneclick-phone').val();
        var product = $('#buyinoneclick-product').val();
        var link = $('#buyinoneclick-link').val();
        var price = $('#buyinoneclick-price').val();

        if (name != "" && phone != "") {
            $.ajax({
                type: 'POST',
                url: '/catalog/controller/tool/buyinoneclick.php',
                data: {
                    name: name,
                    phone: phone,
                    product: product,
                    link: link,
                    price: price
                }
            }).done(function (data) {
                $('.buyinoneclick-result').html(data);
            })
        } else {
            $('.buyinoneclick-result').html('<span style="color: red;">Пожалуйста, заполните все поля!</span>');
        }
    });

    // Highlight any found errors
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();

        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });

    // Currency
    $('#form-currency .currency-select').on('click', function (e) {
        e.preventDefault();

        $('#form-currency input[name=\'code\']').val($(this).attr('name'));

        $('#form-currency').submit();
    });

    // Language
    $('#form-language .language-select').on('click', function (e) {
        e.preventDefault();

        $('#form-language input[name=\'code\']').val($(this).attr('name'));

        $('#form-language').submit();
    });



    // Menu
    $('#menu .dropdown-menu').each(function () {
        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();

        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 10) + 'px');
        }
    });

    // Product List
    $('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');

    // $('#list-view').click(function () {
    // 	$('#content .product-grid > .clearfix').remove();

    // 	$('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');
    // 	$('#grid-view').removeClass('active');
    // 	$('#list-view').addClass('active');

    // 	localStorage.setItem('display', 'list');
    // });

    // // Product Grid
    // $('#grid-view').click(function () {
    // 	// What a shame bootstrap does not take into account dynamically loaded columns
    // 	var cols = $('#column-right, #column-left').length;

    // 	if (cols == 2) {
    // 		$('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
    // 	} else if (cols == 1) {
    // 		$('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
    // 	} else {
    // 		$('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
    // 	}

    // 	$('#list-view').removeClass('active');
    // 	$('#grid-view').addClass('active');

    // 	localStorage.setItem('display', 'grid');
    // });

    // if (localStorage.getItem('display') == 'list') {
    // 	$('#list-view').trigger('click');
    // 	$('#list-view').addClass('active');
    // } else {
    // 	$('#grid-view').trigger('click');
    // 	$('#grid-view').addClass('active');
    // }

    // Checkout
    $(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function (e) {
        if (e.keyCode == 13) {
            $('#collapse-checkout-option #button-login').trigger('click');
        }
    });

    // tooltips on hover
    $('[data-toggle=\'tooltip\']').tooltip({
        container: 'body',
        trigger: 'hover'
    });

    // Makes tooltips work on ajax generated content
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({
            container: 'body'
        });
    });

    // На странице Ткани прокрутка вниз при клике на название
    $('.tkani-link').click(function (e) {
        e.preventDefault();
        var id = $(this).attr('href');
        var offset = 80;
        $('html, body').animate({
            scrollTop: $(id).offset().top - offset
        }, 1000);
    });

    $('.scandic-gallery').magnificPopup({
        type: 'image',
        delegate: 'a',
        gallery: {
            enabled: true
        }
    });
});

// Cart add remove functions
var cart = {
    'add': function (product_id, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('.add-to-cart-notification').remove();
                    $('<div class="add-to-cart-notification">' + json['success'] + '<span class="notification-close">&times;</span></div>').appendTo('body').hide().fadeIn(300).delay(5000).fadeOut(300, function () {
                        $(this).remove();
                    });

                    $('.cart-block .cart-button img').attr('src', 'img/icons/shopping-cart-items.png');

                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        //$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                        $('#cart > button').html('<div class="cart-text"><div class="cart-name">Корзина</div><div id="cart-total">' + json['total'] + '</div></div>');
                    }, 100);

                    //$('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function (key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    //$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                    $('#cart > button').html('<div class="cart-text"><div class="cart-name">Корзина</div><div id="cart-total">' + json['total'] + '</div></div>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    //$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                    $('#cart > button').html('<div class="cart-text"><div class="cart-name">Корзина</div><div id="cart-total">' + json['success'] + '</div></div>');
                }, 100);

                var now_location = String(document.location.pathname);
                console.log(now_location);

                if ((now_location == '/cart') || (now_location == '/checkout') || (getURLVar('route') == 'checkout/cart') || (getURLVar('route') == 'checkout/checkout')) {
                    location = 'cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function () {

    },
    'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    //$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                    $('#cart > button').html('<div class="cart-text"><div class="cart-name">Корзина</div></div>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                //$('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function () {

    }
}

var compare = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    //$('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function () {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function (e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function (data) {
            html = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});

// Autocomplete */
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('autocomplete', 'off');

            // Focus
            $(this).on('focus', function () {
                this.request();
            });

            // Blur
            $(this).on('blur', function () {
                setTimeout(function (object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function (event) {
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }

            // Show
            this.show = function () {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });

                $(this).siblings('ul.dropdown-menu').show();
            }

            // Hide
            this.hide = function () {
                $(this).siblings('ul.dropdown-menu').hide();
            }

            // Request
            this.request = function () {
                clearTimeout(this.timer);

                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function (json) {
                html = '';

                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu').html(html);
            }

            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

        });
    }
})(window.jQuery);

$(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
    });

});