// Загрузка списка салонов для страницы "Города"
function loadSalonsContacts() {
    $.ajax({
        url: "index.php?route=information/contact/getDealers",
        dataType: "html",
        async: false,
        data: {
            city: $('#citySelect').text()
        },
        success: function(data) {
            salonsData = data;
        }
    });
    return salonsData;
}

loadSalonsContacts();

function yMaps(center, zoom) {
    return function() {
        map = new ymaps.Map('map', {
            center: center,
            zoom: zoom
        });

        for(var i = 0; i < salonsData.length; i++) {
            var coords = [salonsData[i]['lat'], salonsData[i]['lon']];
            var placemark = new ymaps.Placemark(coords, {
                balloonContentHeader: salonsData[i]['dealer'],
                balloonContentBody: '<b>Время работы:</b> ' + salonsData[i]['working_time'] + '<br><b>Адрес:</b> ' + salonsData[i]['adress'] + '<br><b>Телефон: </b>' + salonsData[i]['phone_number'],
                hintContent: salonsData[i]['dealer']
            });
            map.geoObjects.add(placemark);
        }
    }
}

ymaps.ready(yMaps([63.22274100654749,86.76818556125302], 3));
for(var i = 0; i < salonsData.length; i++) {
    $('.contactsSalonsList').append('<div class="salons-card"><h3><b>' + salonsData[i]['dealer'] + '</b></h3><b>Время работы:</b> ' + salonsData[i]['working_time'] + '<br><b>Адрес:</b> ' + salonsData[i]['adress'] + '<br><b>Телефон: </b>' + salonsData[i]['phone_number'] + '</div>');
}

$('#citySelect').on('change', function() {
    var selectedCity = $('#citySelect').val();
    $('.city-title').text(selectedCity);
    loadSalonsContacts();
    map.destroy();
    if(selectedCity == 'Все города') {ymaps.ready(yMaps([63.22274100654749,86.76818556125302], 3));}
    if(selectedCity == 'Санкт-Петербург') {ymaps.ready(yMaps([59.95496365506069,30.330172330679154], 10));}
    if(selectedCity == 'Москва') {ymaps.ready(yMaps([55.80739534912656,37.619346417968764], 10));}
    if(selectedCity == 'Нижний Новгород') {ymaps.ready(yMaps([56.30617205095612,44.032650697265524], 11));}
    if(selectedCity == 'Волгоград') {ymaps.ready(yMaps([48.71142692739163,44.521024545898406], 11));}
    if(selectedCity == 'Екатеринбург') {ymaps.ready(yMaps([56.84448397256108,60.604317582031115], 11));}
    if(selectedCity == 'Ижевск') {ymaps.ready(yMaps([56.85407166023875,53.20583940478508], 12));}
    if(selectedCity == 'Минск') {ymaps.ready(yMaps([53.934318714614434,27.50110980517579], 12));}
    if(selectedCity == 'Пермь') {ymaps.ready(yMaps([58.014088307362314,56.22804720898427], 11));}

    $('.contactsSalonsList').html('');

    for(var i = 0; i < salonsData.length; i++) {
        $('.contactsSalonsList').append('<div class="salons-card"><h3>' + salonsData[i]['dealer'] + '</h3><b>Время работы:</b> ' + salonsData[i]['working_time'] + '<br><b>Адрес:</b> ' + salonsData[i]['adress'] + '<br><b>Телефон: </b>' + salonsData[i]['phone_number'] + '</div>');
    }
});