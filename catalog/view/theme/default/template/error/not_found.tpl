<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div class="container-block">
    <div class="container-inner">
        <div id="content"><?php echo $content_top; ?>
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb">
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h1><?php echo $heading_title; ?></h1>
                    <div class="content"><?php echo $text_error; ?></div>
                    <div class="buttons">
                        <div class="right"><a href="<?php echo $continue; ?>" class="button btn btn-default"><?php echo $button_continue; ?></a></div>
                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?>
        </div>
    </div>
</div>
<?php echo $footer; ?>