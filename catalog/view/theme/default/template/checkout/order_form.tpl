<div class="alert alert-danger" id="validation-alert-error" style="display:none;">
    <?php echo $text_order_form_validation_error; ?>
</div>
<div class="alert alert-danger" id="order-pay-error" style="display: none;">
    Пожалуйста, выберите способ оплаты!
</div>
<div class="alert alert-danger" id="privacy-policy-error" style="display: none;">
    Необходимо дать согласие на обработку персональных данных!
</div>
<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_common; ?></h4>
        </div>
        <div class="panel-collapse" id="collapse-checkout-option">
            <div class="panel-body">
                <form class="form-horizontal">
                    <div>
                        <!-- Person name -->
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="lastname" value="" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="firstname" value="" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-middlename"><?php echo $entry_middlename; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="middlename" value="" placeholder="<?php echo $entry_middlename; ?>" id="input-middlename" class="form-control" />
                            </div>
                        </div>

                        <!-- Person contact/payment info -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-contact-phone"><?php echo $entry_contact_phone; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="contact_phone" value="" placeholder="<?php echo $entry_contact_phone; ?>" id="input-contact-phone" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-card-number">Комментарии к заказу</label>
                            <div class="col-sm-10">
                                <input type="text" name="card_number" value="" placeholder="Комментарии к заказу" id="input-card-number" class="form-control" />
                            </div>
                        </div>

                        <div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="order_pay">
                                    Способ оплаты
                                </label>
                                <div class="col-sm-10">
                                    <select id="order_pay_select" class="form-control" name="order_pay">
                                        <option value="">
                                            Способ оплаты не выбран
                                        </option>

                                        <option value="Наличными в пункте выдачи товара">Наличными в пункте выдачи товара</option>
                                        <option value="Безналичный расчет">Безналичный расчет</option>
                                        <option value="Банковской картой в пункте выдачи товара">Банковской картой в пункте выдачи товара</option>
                                        <!-- <option value="Банковской картой на сайте онлайн">Банковской картой на сайте онлайн</option> -->

                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><?php echo $text_checkout_shipping; ?></h4>
        </div>
        <div class="panel-collapse">
            <div class="panel-body">
                <form class="form-horizontal">
                    <!--Delivery method selector-->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo $text_shipping_method; ?></label>
                        <div class="col-sm-10">
                            <div>
                                <label class="radio-inline">
                                    <input type="radio" name="shipping_method" id="pickup" value="pickup" checked>
                                    <?php echo $text_pickup_shipping; ?>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="shipping_method" id="delivery" value="delivery">
                                    <?php echo $text_delivery_shipping; ?>
                                </label>
                            </div>
                        </div>
                    </div>

                    <hr />

                    <!--Pickup method controls-->
                    <div id="method-pickup">
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="pickup_point">
                                <?php echo $text_pickup_point; ?>
                            </label>
                            <div class="col-sm-10">
                                <select id="pickup_point_select" class="form-control" name="pickup_point">
                                    <option value="">
                                        <?php echo $text_pickup_point_select_placeholder; ?>
                                    </option>

                                    <?php foreach ($pickup_points as $pickup_point) { ?>
                                        <option value="<?php echo $pickup_point['value'] ?>">
                                            <?php echo $pickup_point['name']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--Delivery method controls-->
                    <div id="method-delivery" style="display: none">
                        <!--Delivery street-->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-street">
                                <?php echo $text_delivery_street; ?>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" name="street" value="" placeholder="<?php echo $text_delivery_street; ?>" id="input-street" class="form-control" />
                            </div>
                        </div>
                        <!--Delivery subway-->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-subway">
                                <?php echo $text_delivery_subway; ?>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" name="subway" value="" placeholder="<?php echo $text_delivery_subway; ?>" id="input-subway" class="form-control" />
                            </div>
                        </div>
                        <!--Delivery postal code-->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-postal-code">
                                <?php echo $text_delivery_postal_code; ?>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" name="postal_code" value="" placeholder="<?php echo $text_delivery_postal_code; ?>" id="input-postal-code" class="form-control" />
                            </div>
                        </div>
                        <!--Delivery house number-->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-house-number">
                                <?php echo $text_delivery_house_number; ?>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" name="house_number" value="" placeholder="<?php echo $text_delivery_house_number; ?>" id="input-house-number" class="form-control" />
                            </div>
                        </div>
                        <!--Delivery building number-->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-building-number">
                                <?php echo $text_delivery_building_number; ?>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" name="building_number" value="" placeholder="<?php echo $text_delivery_building_number; ?>" id="input-building-number" class="form-control" />
                            </div>
                        </div>
                        <!--Delivery building entrance-->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-building-entrance">
                                <?php echo $text_delivery_building_entrance; ?>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" name="building_entrance" value="" placeholder="<?php echo $text_delivery_building_entrance; ?>" id="input-building-entrance" class="form-control" />
                            </div>
                        </div>
                        <!--Delivery building floor-->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-building-floor">
                                <?php echo $text_delivery_building_floor; ?>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" name="building_floor" value="" placeholder="<?php echo $text_delivery_building_floor; ?>" id="input-building-floor" class="form-control" />
                            </div>
                        </div>
                        <!--Delivery flat number-->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-flat-number">
                                <?php echo $text_delivery_flat_number; ?>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" name="flat" value="" placeholder="<?php echo $text_delivery_flat_number; ?>" id="input-flat-number" class="form-control" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="buttons clearfix">
        <div class="checkbox" style="margin-top: 0; padding-left: 10px;">
            <label>
                <input type="checkbox" id="private_policy_check">
                Я подтверждаю, что я старше 18 лет, принимаю условия работы сайта и даю добровольное согласие на
                обработку своих персональных данных.
                <strong>
                    <a href="<?php echo $policy_link; ?>" target="_blank">Соглашение на обработку персональных данных</a>
                </strong>
            </label>
        </div>
        <div>
            <div style="padding-top: 20px; padding-left: 10px;">
                <input type="button" value="<?php echo $button_do_checkout; ?>" id="button-do-checkout" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
            </div>

            <!-- <div class="col-sm-2" style="padding: 20px;">
                <div class="pull-right">
                    <input onclick="alert('Функция заказа временно отключена администратором сайта, Вы можете связаться с нами: zakaz@scandicstyle.ru'); return false;" type="button" value="<?php echo $button_do_checkout; ?>"  data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"/>
                </div>
            </div> -->


        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        function shippingMethod() {
            return $("input:radio[name ='shipping_method']:checked").val();
        }

        $("input:radio[name ='shipping_method']").change(function() {
            if (shippingMethod() === 'pickup') {
                $('#method-pickup').css({
                    display: 'block'
                });
                $('#method-delivery').css({
                    display: 'none'
                });
            } else {
                $('#method-pickup').css({
                    display: 'none'
                });
                $('#method-delivery').css({
                    display: 'block'
                });
            }
        });

        function Validator() {
            var validators = [];
            var phoneRegex = /^\+{0,1}\d[\d() -]{4,14}\d$/;
            var emailRegex = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            function check(query, validate) {
                var element = $(query);
                var container = element.closest('.form-group');

                if (!validate(element)) {
                    container.addClass('has-error');
                } else {
                    container.removeClass('has-error');
                }
            }

            function registerValidator(query, validate) {
                var e = $(query);
                var checkFunc = check.bind(this, query, validate);

                e.change(checkFunc);
                e.focusout(checkFunc);
                validators.push({
                    query: query,
                    validator: validate
                });
            }

            this.validate = registerValidator;
            this.notEmpty = function(query) {
                registerValidator(query, function(e) {
                    return !!e.val();
                });
            };
            this.isPhone = function(text) {
                return phoneRegex.test(text)
            };
            this.isEmail = function(text) {
                return emailRegex.test(text)
            };
            this.validateAll = function() {
                for (var i = 0; i < validators.length; ++i) {
                    var context = validators[i];
                    check(context.query, context.validator);
                }
            };
            this.isValid = function() {
                for (var i = 0; i < validators.length; ++i) {
                    var context = validators[i];
                    if (!context.validator($(context.query))) {
                        return false;
                    }
                }
                return true;
            }
        }

        var v = new Validator();
        v.notEmpty("input[name='lastname']");
        v.notEmpty("input[name='firstname']");
        v.validate("input[name='contact_phone']", function(e) {
            return !!e.val() && v.isPhone(e.val());
        });
        v.validate("input[name='email']", function(e) {
            return !e.val() || v.isEmail(e.val());
        });
        v.validate("select[name='pickup_point']", function(e) {
            return shippingMethod() !== 'pickup' || e.val() !== '';
        });
        v.validate("input[name='email']", function(e) {
            return !e.val() || v.isEmail(e.val());
        });

        $('#button-do-checkout').click(function() {
            var alert = $('#validation-alert-error');
            var privacy_alert = $('#privacy-policy-error');
            var order_pay_alert = $('#order-pay-error');
            alert.css({
                display: 'none'
            });
            privacy_alert.css({
                display: 'none'
            });
            order_pay_alert.css({
                display: 'none'
            });
            if (!v.isValid()) {
                v.validateAll();
                alert.css({
                    display: 'block'
                });
                $('html, body').animate({
                    scrollTop: alert.offset().top - 10
                }, 'slow');
            } else {
                if ($('#order_pay_select').val() == "") {
                    order_pay_alert.css({
                        display: 'block'
                    });
                    $('html, body').animate({
                        scrollTop: order_pay_alert.offset().top - 10
                    }, 'slow');
                    return;
                }

                if (!$('#private_policy_check').prop('checked')) {
                    privacy_alert.css({
                        display: 'block'
                    });
                    $('html, body').animate({
                        scrollTop: privacy_alert.offset().top - 10
                    }, 'slow');
                    return;
                }

                alert.css({
                    display: 'none'
                });
                var formData = {
                    lastname: $("input[name='lastname']").val(),
                    middlename: $("input[name='middlename']").val(),
                    firstname: $("input[name='firstname']").val(),
                    email: $("input[name='email']").val(),
                    telephone: $("input[name='contact_phone']").val(),
                    comment: $("input[name='card_number']").val(),
                    order_pay: $('select[name=order_pay]').val(),
                    shipping_method: $("input[name='shipping_method']:checked").val(),
                    pickup_point: $('select[name=pickup_point]').val(),
                    street: $("input[name='street']").val(),
                    subway: $("input[name='subway']").val(),
                    postal_code: $("input[name='postal_code']").val(),
                    house_number: $("input[name='house_number']").val(),
                    building_number: $("input[name='building_number']").val(),
                    building_entrance: $("input[name='building_entrance']").val(),
                    building_floor: $("input[name='building_floor']").val(),
                    flat: $("input[name='flat']").val(),
                };

                $.ajax({
                    type: "POST",
                    url: '<?php echo $data_submission_url; ?>',
                    data: formData,
                    beforeSend: function() {
                        $('#button-do-checkout').button('loading');
                    },
                    complete: function() {
                        $('#button-do-checkout').button('reset');
                    },
                    success: function(data) {
                        window.location.href = data;
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        window.alert(
                            'Произошла ошибка при отправке заказа, ' +
                            'попробуйте отправить заказ позже '
                        );
                    }
                });
            }
        });
    })
</script>