<?php echo $header; ?>
<div class="container-block">
    <div class="container-inner">
        <div class="row">
            <div class="col-xs-12">
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php if (isset($breadcrumb['href'])) { ?>
                                <a href="<?php echo $breadcrumb['href']; ?>">
                                    <?php echo $breadcrumb['text']; ?>
                                </a>
                            <?php } else { ?>
                                <?php echo $breadcrumb['text']; ?>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="row">
            <?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="result-page <?php echo $class; ?>">
                <?php echo $content_top; ?>
                <style>
                    @media print {

                        /* Стиль для печати */
                        #feedback,
                        .row-top,
                        .modal,
                        footer,
                        .row-top,
                        #nav__basemenu,
                        .noprint {
                            display: none;
                        }

                        /* Блок который нужно отобразить */
                        .printobl {
                            display: block !important;
                        }
                    }
                </style>
                <div class="printobl">
                    <h2 class="info-page-header">Спасибо!
                        <br /> Ваш заказ
                        <span style="color: #cda44a;">№
                            <?php echo $order_num; ?>
                        </span> принят
                    </h2>
                    <!-- <div class="noprint">
                        <img src="/img/bricks/print.png" />
                        <a href="javascript:window.print()">распечатать
                        </a>
                    </div> -->
                    <?php foreach ($products as $product) { ?>
                        <div class="result-page__product">
                            <?php if ($product['thumb']) { ?>
                                <a href="<?php echo $product['href']; ?>">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
                                </a>
                            <?php } ?>
                            <div class="result-page__product-info">
                                <a href="<?php echo $product['href']; ?>">
                                    <h3><?php echo $product['name']; ?></h3>
                                </a>
                                <p><span class="product-header"><?php echo $column_model; ?>:</span> <?php echo $product['model']; ?></p>
                                <p><span class="product-header"><?php echo $column_quantity; ?>:</span> <?php echo $product['quantity']; ?></p>
                                <p><span class="product-header"><?php echo $column_price; ?>:</span> <?php echo number_format($product['price'], 0, '', ' '); ?> руб.</p>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="pull-left result-page__total">
                        Сумма заказа:
                        <span class="total-price"><?php echo number_format($total, 0, '', ' '); ?> руб.</span>
                    </div>
                    <br />
                    <h3>Информация о заказе
                    </h3>
                    <div class="mainsite">
                        <p class="text-left">Номер заказа:
                            <?php echo $order_num; ?>
                        </p>
                        <p class="text-left">Ваше ФИО:
                            <?php echo $full_name; ?>
                        </p>
                        <p class="text-left">Ваш e-mail:
                            <?php echo $email; ?>
                        </p>
                        <p class="text-left">Контактный телефон:
                            <?php echo $contact_phone; ?>
                        </p>
                        <p class="text-left">Форма оплаты:
                            <?php echo $order_pay; ?>
                        </p>
                        <p class="text-left">Комментарий к заказу:
                            <?php echo $comment; ?>
                        </p>
                        <p class="text-left">Способ доставки:
                            <?php echo $shipping_method_name; ?>
                        </p>
                        <?php if ($shipping_method == 'pickup') { ?>
                            <p class="text-left">Выдача товара:
                                <?php echo $pickup_point; ?>
                            </p>
                        <?php } ?>
                        <?php if ($shipping_method != 'pickup') { ?>
                            <p class="text-left">Адрес доставки:
                            </p>
                            <p class="text-left">
                                <form class="form-horizontal">
                                    <div id="method-delivery">
                                        <!--Delivery street-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-street">
                                                <?php echo $text_delivery_street; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="street" value="<?php echo $street; ?>" disabled id="input-street" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery subway-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-subway">
                                                <?php echo $text_delivery_subway; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="subway" value="<?php echo $subway; ?>" disabled id="input-subway" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery postal code-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-postal-code">
                                                <?php echo $text_delivery_postal_code; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="postal_code" value="<?php echo $postal_code; ?>" disabled id="input-postal-code" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery house number-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-house-number">
                                                <?php echo $text_delivery_house_number; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="house_number" value="<?php echo $house_number; ?>" disabled id="input-house-number" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery building number-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-building-number">
                                                <?php echo $text_delivery_building_number; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="building_number" value="<?php echo $building_number; ?>" disabled id="input-building-number" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery building entrance-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-building-entrance">
                                                <?php echo $text_delivery_building_entrance; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="building_entrance" value="<?php echo $building_entrance; ?>" disabled id="input-building-entrance" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery building floor-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-building-floor">
                                                <?php echo $text_delivery_building_floor; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="building_floor" value="<?php echo $building_floor; ?>" disabled id="input-building-floor" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery flat number-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-flat-number">
                                                <?php echo $text_delivery_flat_number; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="flat" value="<?php echo $flat; ?>" disabled id="input-flat-number" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </p>
                        <?php } ?>
                    </div>
                    <div class="mobsite">
                        <p class="text-left">Номер заказа:
                            <?php echo $order_num; ?>
                        </p>
                        <p class="text-left">Ваше ФИО:
                            <?php echo $full_name; ?>
                        </p>
                        <p class="text-left">Ваш e-mail:
                            <?php echo $email; ?>
                        </p>
                        <p class="text-left">Контактный телефон:
                            <?php echo $contact_phone; ?>
                        </p>
                        <p class="text-left">Форма оплаты:
                            <?php echo $order_pay; ?>
                        </p>
                        <p class="text-left">Комментарий к заказу:
                            <?php echo $comment; ?>
                        </p>
                        <p class="text-left">Способ доставки:
                            <?php echo $shipping_method_name; ?>
                        </p>
                        <?php if ($shipping_method == 'pickup') { ?>
                            <p class="text-left">Выдача товара:
                                <?php echo $pickup_point; ?>
                            </p>
                        <?php } ?>
                        <?php if ($shipping_method != 'pickup') { ?>
                            <p class="text-left">Адрес доставки:
                            </p>
                            <p class="text-left">
                                <form class="form-horizontal">
                                    <div id="method-delivery">
                                        <!--Delivery street-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-street">
                                                <?php echo $text_delivery_street; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="street" value="<?php echo $street; ?>" disabled id="input-street" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery subway-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-subway">
                                                <?php echo $text_delivery_subway; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="subway" value="<?php echo $subway; ?>" disabled id="input-subway" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery postal code-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-postal-code">
                                                <?php echo $text_delivery_postal_code; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="postal_code" value="<?php echo $postal_code; ?>" disabled id="input-postal-code" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery house number-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-house-number">
                                                <?php echo $text_delivery_house_number; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="house_number" value="<?php echo $house_number; ?>" disabled id="input-house-number" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery building number-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-building-number">
                                                <?php echo $text_delivery_building_number; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="building_number" value="<?php echo $building_number; ?>" disabled id="input-building-number" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery building entrance-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-building-entrance">
                                                <?php echo $text_delivery_building_entrance; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="building_entrance" value="<?php echo $building_entrance; ?>" disabled id="input-building-entrance" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery building floor-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-building-floor">
                                                <?php echo $text_delivery_building_floor; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="building_floor" value="<?php echo $building_floor; ?>" disabled id="input-building-floor" class="form-control" />
                                            </div>
                                        </div>
                                        <!--Delivery flat number-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-flat-number">
                                                <?php echo $text_delivery_flat_number; ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="flat" value="<?php echo $flat; ?>" disabled id="input-flat-number" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </p>
                        <?php } ?>
                    </div>
                    <h3>Дополнительная информация
                    </h3>
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Способ оплаты
                                </h4>
                            </div>
                            <div class="panel-collapse" id="collapse-checkout-option">
                                <div class="panel-body">
                                    <p>
                                        <i class="fa fa-money">
                                        </i>
                                        Наличными в пункте выдачи товара
                                    </p>
                                    <p>
                                        <i class="fa fa-arrow-circle-right">
                                        </i>
                                        Безналичный расчет
                                    </p>
                                    <p>
                                        <i class="fa fa-credit-card">
                                        </i>
                                        Банковской картой в пункте выдачи товара
                                    </p>
                                    <p style="color: #cda44a;">* Перед оплатой безналичным расчетом обязательно дождитесь звонка менеджера
                                        интернет-ресурса для уточнения наличия и сроков поставки товара!
                                        <br />
                                        ** Доставка товара осуществляется только при сумме заказа от 100000 руб.
                                    </p>
                                    <p>
                                        При заказе товаров с признаком
                                        <span style="color: #cda44a;">"под заказ"
                                        </span>
                                        Вам необходимо дождаться звонка оператора
                                        интернет-ресурса для уточнения условий и сроков поставки товаров.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Возврат
                                </h4>
                            </div>
                            <div class="panel-collapse" id="collapse-checkout-option">
                                <div class="panel-body">
                                    Условия возврата:
                                    <ul>
                                        <li>
                                            Возвращаемый товар не должен иметь следов использования,
                                            должны быть сохранены ярлыки, пломбы, упаковка.
                                        </li>
                                        <li>Нельзя вернуть товар, надлежащего качества, изготовленный на заказ.
                                        </li>
                                    </ul>
                                    Возврат денег осуществляется одним из следующих способов:
                                    <ul>
                                        <li>Наличными денежными средствами по месту нахождения продавца.
                                        </li>
                                        <li>Путем перечисления соответствующей суммы на банковский или иной счет.
                                        </li>
                                    </ul>
                                    <p>
                                        <span style="color: #cda44a;">* Продавец имеет право удержать расходы на доставку товара от покупателя!
                                        </span>
                                        <br />
                                        <span style="color: #cda44a;">* При совершении покупки через интернет-ресурс, мы увеличили льготный период возврата или обмена товара до 60 дней.
                                        </span>
                                    </p>
                                    <p>Возврат товара регулируются статьей 26.1 ФЗ «О защите прав потребителей», а также «Правилами
                                        продажи товаров дистанционным способом», утвержденных Постановлением Правительства РФ от
                                        27.09.2007 г. №612.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3>Телефон для связи с менеджером интернет-ресурса
                    </h3>
                    <h1 class="contact-us-phone">
                        <?php echo $contact_us_phone; ?>
                    </h1>
                    scandic@elisanet.fi
                </div>
                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?>
        </div>
    </div>
</div>
<?php echo $footer; ?>