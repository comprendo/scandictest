<div class="container-block">
    <div class="container-inner">
        <div class="row p-0">
            <div class="col-xs-12">
                <div class="breadcrumb">
                    <div class="page">
                        <div class="page__demo">
                            <div class="page__container">
                                <?php foreach ($gallimages as $gallimage) { ?>
                                    <div class="photobox photobox_type16">
                                        <div class="photobox__previewbox">
                                            <img src="<?php echo $gallimage['image']; ?>" class="photobox__preview" alt="Preview">
                                            <span class="photobox__label"><?php echo $gallimage['title']; ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>