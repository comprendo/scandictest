<div class="box text-right left-on-mobile">

    <button class="city-select-modal-button">
        <img сlass="city-select__icon" src="img/icons/marker.png" alt="Регион">
        <select name="store" id="store" onchange="location = this.value">
            <?php foreach ($stores as $store) { ?>
                <?php if ($store['store_id'] == $store_id) { ?>
                    <option value="<?php echo $store['url']; ?>" selected="selected"><?php echo $store['name']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $store['url']; ?>"><?php echo $store['name']; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </button>

    <?php if (!isset($_COOKIE['region'])) : ?>
        <div class="city-confirm text-left">
            <div class="city-confirm__inner">
                <form method="POST">
                    <p>Ваш регион <span id="city-confirm__selected-city"></span>?</p>
                    <div class="city-confirm__buttons">
                        <button class="city-confirm__button" id="button_yes">Да</button>
                        <button class="city-confirm__button" id="button_no">Нет</button>
                    </div>
                </form>
            </div>
        </div>
    <?php endif; ?>

</div>

<script>
    function saveRegion() {
        $.ajax({
            type: 'POST',
            url: 'index.php?route=module/store/setRegion'
        })
    }

    $('#button_yes').click(function(e) {
        e.preventDefault();
        $('.city-confirm').addClass('hidden');
        saveRegion();
    });

    $('#button_no').click(function(e) {
        e.preventDefault();
        $('.city-confirm').addClass('hidden');
        $('#store').select2('open');
    });
</script>