<?php echo $header; ?>

<div class="row">
    <div id="content" class="col-sm-12 slider-block">
        <div id="slideshow0" class="main-slider"></div>

        <script type="text/javascript">
            $('#slideshow0').vegas({
                delay: 7000,
                timer: false,
                shuffle: false,
                animation: 'random',
                transitionDuration: 2000,
                slides: [{
                        src: 'img/scd_slider/sld_new_1.jpg'
                    },
                    {
                        src: 'img/scd_slider/sld_new_2.jpg'
                    },
                    {
                        src: 'img/scd_slider/sld_new_3.jpg'
                    },
                    {
                        src: 'img/scd_slider/sld_new_4.jpg'
                    },
                    {
                        src: 'img/scd_slider/sld_new_5.jpg'
                    },
                    {
                        src: 'img/scd_slider/sld_new_6.jpg'
                    },
                    {
                        src: 'img/scd_slider/sld_new_7.jpg'
                    },
                    {
                        src: 'img/scd_slider/sld_new_9.jpg'
                    }
                ]
            });
        </script>
    </div>
</div>

<div class="container-block">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="col-sm-12"><?php echo $content_top; ?></div>
        <?php echo $column_right; ?></div>
</div>

<div class="container-block">
    <div class="row">
        <div class="col-xs-12 col-md-8 big-block-wrapper-left">
            <a href="interiors">
                <div class="lookbook-block big-block" style="background-image: url('img/catalog_cover/cover_interior.jpg');">
                    <div class="big-block__cover">
                        <p class="big-block__title">Интерьеры</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-4 big-block-wrapper-right">
            <a href="stocks">
                <div class="akcii-block big-block" style="background-image: url('img/catalog_cover/cover_sale.jpg');">
                    <div class="big-block__cover">
                        <p class="big-block__title">Акции</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="container-block background-block">
    <div class="main_about">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="main_h2 text-center">Интернет - магазин финской мягкой мебели и мебели для спальни Scandic Style!</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="advantage">
                    <div class="head">Scandic Style – больше, чем просто мебель</div>
                    <div class="body">Это европейские стандарты качества и гарантия производителя. Вся мебель производится в Финляндии в соответствии с европейскими стандартами производства. Мы лично протестировали каждую модель и уверены, что предлагаем Вам действительно удобную мебель.</div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="advantage">
                    <div class="head">Scandic Style – это уютный дом и стильный интерьер</div>
                    <div class="body">Диваны, кресла и кровати выполнены в лаконичном скандинавском стиле, который не боится капризов моды и остается неизменно популярным. Большой выбор обивочных материалов позволит реализовать любые дизайнерские решения.</div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="advantage">
                    <div class="head">Scandic Style – это продуманная система логистики</div>
                    <div class="body">Вы можете выбрать диван, кресло или кровать из наличия. Или заказать свой уникальный проект. И мы привезем его в любой город России. Срок поставки на склад в Санкт-Петербург 10 недель. В Ваш город до 12 недель.</div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="advantage">
                    <div class="head">Цена - качество</div>
                    <div class="body">В интернет - магазине Scandic Style вы можете выбрать европейскую мебель с превосходным соотношением «цена – качество». Уверены, Вы найдете то, что нужно и останетесь довольны!</div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>