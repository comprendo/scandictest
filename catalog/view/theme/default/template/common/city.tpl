<div class="pull-up">
    <div class="btn-group">
        <button class="btn btn-link" data-toggle="modal" data-target="#city-select-modal">
					<span class="hidden-sm">
					    <!-- <span class="mobsite"><i class="fa fa-map-marker"></i></span> -->
                        <?php echo "<span style='font-size: 12pt; margin-right: 0px;'>" . ($selected_city ? $cities[$selected_city]['name'] : 'Город') . "</span>"; ?>
                    </span>
        </button>

        <span class="mobsite">
            <button class="btn btn-link" data-toggle="modal" data-target="#city-select-modal-mobile">
                        <span class="hidden-sm">
                            <!-- <i class="fa fa-map-marker"></i> -->
                            <?php echo "<span style='font-size: 12pt; margin-right: 0px;'>" . ($selected_city ? $cities[$selected_city]['name'] : 'Город') . "</span>"; ?>
                        </span>
            </button>
        </span>
    </div>

    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-city">
        <!--Select city dialog-->
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="city-select-modal">
            <div class="modal-dialog modal-md">
                <div class="modal-content mwidth">
                    <center>
                    <div class="modal-header">

                        <?php //if ($selected_city) { ?>
                        <!-- <button
                                type="button"
                                class="close"
                                data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button> -->
                        <?php //} else { ?>
                        <!-- <script>
                            $('.bs-example-modal-sm').modal({show: true});
                        </script> -->
                        <?php //} ?>

                        <h4 class="modal-title" id="mySmallModalLabel">Выберите интересующий Вас регион: </h4>

                    </div>
                    <div class="modal-body">
                        <?php foreach ($cities as $key => $value) { ?>
                       <input 
                                type="button"
                                class="btn btn-default fontmodal"
                                onclick="
                                    $('#city-id').val('<?php echo $key; ?>');
                                    $('#form-city').submit();"
                                value="<?php echo $value["name"]; ?>"
                        />
                        <?php } ?>
                    </div>
                    </center>
            </div>
            </div>
        </div>

        <input id="city-id" type="hidden" name="cityId" value=""/>
        <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
    </form>
</div>