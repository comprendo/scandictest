</main>
<footer>
    <div class="container-block">
        <div class="container-inner">
            <div class="scrollup"></div>
            <div class="row">
                <div class="col-md-3 offset-md-3"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-3 footer-contact-block">
                    <img class="footer-logo" src="img/logo_new.svg" alt="ScandicStyle">
                    <p class="footer-address"><?php echo str_replace('\n', '<br>', $address); ?></p>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="footer-header">Информация для клиентов</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled">
                                <li><a href="shipping-and-payment">Доставка и оплата</a></li>
                                <li><a href="how-to-place-an-order">Как оформить заказ</a></li>
                                <li><a href="warranty">Гарантия</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 navigation-continue">
                            <ul class="list-unstyled">
                                <li><a href="articles">Статьи</a></li>
                                <li><a href="tissue">Ткани</a></li>
                                <li><a href="sitemap">Карта сайта</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="footer-header">Контакты</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 footer-contact">
                            <p><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a></p>
                            <p><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
                            <p>Время работы: <?php echo $working_time; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="container-block">
        <div class="container-inner">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 footer-copyright">
                            &copy; Scandic Style, <?php echo date('Y'); ?> год.<br>Вся информация, представленная на сайте и касающаяся технических характеристик, цветовых сочетаний, стоимости мебели и её наборов, а также вариаций и гарантийного и послегарантийного обслуживания носит исключительно информационный характер. Ни при каких условиях эта информация или её производные не являются публичной офертой, которая может определяться положениями статьи 437 (2) Гражданского кодекса Российской Федерации.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 footer-politics">
                            <a href="legal-information">Правовая информация</a><br>
                            <a href="privacy-policy">Политика конфиденциальности</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Scripts -->
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/common.js?v5.5" type="text/javascript"></script>
<script type="text/javascript" src="catalog/view/javascript/menu_fixed.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="catalog/view/javascript/priceview.js" type="text/javascript"></script>
<script src="https://static.yandex.net/kassa/pay-in-parts/ui/v1/"></script>
<script src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>
<script>
    (function(w, d, u) {
        var s = d.createElement('script');
        s.async = true;
        s.src = u + '?' + (Date.now() / 60000 | 0);
        var h = d.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s, h);
    })(window, document, 'https://cdn-ru.bitrix24.ru/b1637913/crm/site_button/loader_3_egxhrl.js');
</script>
<!-- / Scripts -->
</body>

</html>