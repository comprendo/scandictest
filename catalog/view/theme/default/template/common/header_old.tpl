<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
  <?php } ?>
  <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
  <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
  <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
  <link href="catalog/view/theme/default/stylesheet/stylesheet.css?v2.5" rel="stylesheet">
  <link href="catalog/view/theme/default/stylesheet/menu_fixed.css" rel="stylesheet">
  <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/firefox_fix.css">
  <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
  <?php } ?>
  <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/owl.transitions.css">
  <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/owl.carousel.css">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js"></script>
  <script src="catalog/view/javascript/common.js?v2.1" type="text/javascript"></script>
  <script src="catalog/view/javascript/priceview.js" type="text/javascript"></script>
  <?php foreach ($links as $link) { ?>
  <?php } ?>
  <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
  <?php } ?>
  <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
  <?php } ?>
  <?php if ($city_id == 'spb') { ?>
    <link href="catalog/view/theme/default/stylesheet/spb.css" rel="stylesheet">
  <?php } else if ($city_id == 'msk') { ?>
    <link href="catalog/view/theme/default/stylesheet/msk.css" rel="stylesheet">
  <?php } else if ($city_id == 'sam') { ?>
    <link href="catalog/view/theme/default/stylesheet/sam.css" rel="stylesheet">
  <?php } ?>
  <script src="https://static.yandex.net/kassa/pay-in-parts/ui/v1/"></script>
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBKn6XSShwapDXrtpAMZigzrVrC7khoP2w"></script>
  <script type="text/javascript" src="catalog/view/javascript/map_mag.js"></script>
  <script type="text/javascript" src="catalog/view/javascript/menu_fixed.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <script src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>
  <link rel="stylesheet" href="catalog/view/javascript/jquery/magnific/magnific-popup.css">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#cda44a">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <script src="//code-ya.jivosite.com/widget.js" data-jv-id="1dR6VEemuB" async></script>
  <!-- Yandex.Metrika counter -->
  <script type="text/javascript">
    (function(m, e, t, r, i, k, a) {
      m[i] = m[i] || function() {
        (m[i].a = m[i].a || []).push(arguments)
      };
      m[i].l = 1 * new Date();
      k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(57373848, "init", {
      clickmap: true,
      trackLinks: true,
      accurateTrackBounce: true,
      webvisor: true
    });
  </script>
  <noscript>
    <div><img src="https://mc.yandex.ru/watch/57373848" style="position:absolute; left:-9999px;" alt="" /></div>
  </noscript>
  <!-- /Yandex.Metrika counter -->
  <!-- Yandex.Metrika counter -->
  <script type="text/javascript">
    (function(m, e, t, r, i, k, a) {
      m[i] = m[i] || function() {
        (m[i].a = m[i].a || []).push(arguments)
      };
      m[i].l = 1 * new Date();
      k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(56785456, "init", {
      clickmap: true,
      trackLinks: true,
      accurateTrackBounce: true,
      webvisor: true
    });
  </script>
  <noscript>
    <div><img src="https://mc.yandex.ru/watch/56785456" style="position:absolute; left:-9999px;" alt="" /></div>
  </noscript>
  <!-- /Yandex.Metrika counter -->
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154938839-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-154938839-1');
  </script>
</head>

<body onload="initialize()" class="<?php echo $class; ?>">
  <main id="hf-container">
    <div class="left-menu__compressed">
      <span>&equiv;</span>
    </div>
    <aside class="left-menu">

      <div class="left-menu__top">
        <div class="left-menu__logo">
          <a href="index.php">
            <img src="img/logo.svg" alt="">
          </a>
        </div>
      </div><!-- /.left-menu__top -->

      <div class="left-menu__content">

        <a class="left-menu__row card_trigger" data-card="#pryamye_divany">
          <div class="left-menu__icon">
            <img src="img/icons/new/pryamye_divany.png" alt="">
          </div>
          <div class="left-menu__text">Прямые диваны</div>
        </a>

        <a class="left-menu__row card_trigger" data-card="#uglovie_divany">
          <div class="left-menu__icon">
            <img src="img/icons/new/uglovie_divany.png" alt="">
          </div>
          <div class="left-menu__text">Угловые диваны</div>
        </a>

        <a class="left-menu__row card_trigger" data-card="#divany_s_kushetkoi">
          <div class="left-menu__icon">
            <img src="img/icons/new/divany_s_kushetkoi.png" alt="">
          </div>
          <div class="left-menu__text">Диваны с кушеткой</div>
        </a>

        <!-- <a class="left-menu__row card_trigger" data-card="#modulnie_divany">
        <div class="left-menu__icon">
          <img src="img/icons/pr_divan.png" alt="">
        </div>
        <div class="left-menu__text">Модульные диваны</div>
      </a> -->

        <a class="left-menu__row card_trigger" data-card="#kresla">
          <div class="left-menu__icon">
            <img src="img/icons/new/kresla.png" alt="">
          </div>
          <div class="left-menu__text">Кресла</div>
        </a>

        <a class="left-menu__row card_trigger" data-card="#kreslo-reklainer">
          <div class="left-menu__icon">
            <img src="img/icons/new/kreslo-reklainer.png" alt="">
          </div>
          <div class="left-menu__text">Кресла-реклайнеры</div>
        </a>

        <a class="left-menu__row card_trigger" data-card="#krovati">
          <div class="left-menu__icon">
            <img src="img/icons/new/krovat.png" alt="">
          </div>
          <div class="left-menu__text">Кровати</div>
        </a>

        <!-- <a class="left-menu__row card_trigger" data-card="#matrasi">
        <div class="left-menu__icon">
          <img src="img/icons/matras.png" alt="">
        </div>
        <div class="left-menu__text">Матрасы</div>
      </a>

      <a class="left-menu__row card_trigger" data-card="#namatrasniki">
        <div class="left-menu__icon">
          <img src="img/icons/namatrasniki.png" alt="">
        </div>
        <div class="left-menu__text">Наматрасники</div>
      </a>

      <a class="left-menu__row card_trigger" data-card="#izgoloviya">
        <div class="left-menu__icon">
          <img src="img/icons/izgolovie.png" alt="">
        </div>
        <div class="left-menu__text">Изголовья</div>
      </a> -->

        <a class="left-menu__row card_trigger" data-card="#aksessyary">
          <div class="left-menu__icon">
            <img src="img/icons/cat-acessories.png" alt="">
          </div>
          <div class="left-menu__text">Аксессуары</div>
        </a>

        <a class="left-menu__row" href="/index.php?route=information/information&information_id=52">
          <div class="left-menu__icon">
            <img src="img/icons/sales.png" alt="">
          </div>
          <div class="left-menu__text">Акции</div>
        </a>
      </div><!-- /.left-menu__content -->

      <div class="left-menu__contacts">
        <p><?php echo $address;
            if ($additional_contacts) {
              echo '<br>(<a data-toggle="modal" data-target="#additional-contacts-modal" class="scandic-link">другие адреса</a>)';
            } ?></p>
        <p><?php echo $working_time; ?></p>
        <p><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a></p>
        <p><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
      </div>

      <div class="left-menu__footer">
        <a href="/index.php?route=information/delivery" class="left-menu__row">
          <div class="left-menu__icon">
            <img src="img/icons/info.png" alt="">
          </div>
          <span class="left-menu__text">
            Доставка и оплата
          </span>
        </a>
      </div>
      <!--/.left-menu__footer-->
    </aside>

    <div class="cards">

      <div class="card" id="pryamye_divany">
        <div class="card__header">
          <h3>Прямые диваны</h3>
          <a class="card-close">&#10006;</a>
        </div>
        <div class="card__content">
          <div class="card__good">
            <a href="/index.php?route=product/category&path=88">
              <h4 class="card__good-name">ANNA</h4><img src="img/cards_tovar/pr_divan/anna.jpg" alt="Anna" title="Anna" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=62">
              <h4 class="card__good-name">ANNIKA</h4><img src="img/cards_tovar/pr_divan/annika.jpg" alt="Annika" title="Annika" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=63">
              <h4 class="card__good-name">ARTTU</h4><img src="img/cards_tovar/pr_divan/arttu.jpg" alt="Arttu" title="Arttu" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=67">
              <h4 class="card__good-name">CAMILA</h4><img src="img/cards_tovar/pr_divan/camila3.jpg" alt="Camila" title="Camila" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=90">
              <h4 class="card__good-name">DOMINA</h4><img src="img/cards_tovar/pr_divan/domina.jpg" alt="Domina" title="Domina" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=69">
              <h4 class="card__good-name">EETU</h4><img src="img/cards_tovar/pr_divan/eetu.jpg" alt="Eetu" title="Eetu" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=70">
              <h4 class="card__good-name">GABRIEL</h4><img src="img/cards_tovar/pr_divan/gabriel.jpg" alt="Gabriel" title="Gabriel" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=71">
              <h4 class="card__good-name">RAIMO</h4><img src="img/cards_tovar/pr_divan/raimo2.jpg" alt="Raimo" title="Raimo" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=73">
              <h4 class="card__good-name">RENNI</h4><img src="img/cards_tovar/pr_divan/renny.jpg" alt="Renni" title="Renni" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=74">
              <h4 class="card__good-name">ROLLY</h4><img src="img/cards_tovar/pr_divan/rolly.jpg" alt="Rolly" title="Rolly" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=75">
              <h4 class="card__good-name">SELLA</h4><img src="img/cards_tovar/pr_divan/sella.jpg" alt="Sella" title="Sella" class="card__good-image">
            </a>
          </div>
          <!-- <?php foreach ($products_pryamye_divany as $product) { ?>
        <div class="card__good">
          <h4 class="card__good-name"><?php echo $product['name']; ?></h4>
          <a href="/index.php?route=product/category&path=<?php echo $product['collection']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="card__good-image"></a>
        </div>
        <?php } ?> -->
        </div>
      </div>

      <div class="card" id="uglovie_divany">
        <div class="card__header">
          <h3>Угловые диваны</h3>
          <a class="card-close">&#10006;</a>
        </div>
        <div class="card__content">
          <div class="card__good">
            <a href="/index.php?route=product/category&path=66">
              <h4 class="card__good-name">BETTY</h4><img src="img/cards_tovar/uglovie/betty.jpg" alt="Betty" title="Betty" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=67">
              <h4 class="card__good-name">CAMILA</h4><img src="img/cards_tovar/uglovie/camila.jpg" alt="Camila" title="Camila" class="card__good-image">
            </a>
          </div>
          <!-- <?php foreach ($products_uglovie_divany as $product) { ?>
        <div class="card__good">
          <h4 class="card__good-name"><?php echo $product['name']; ?></h4>
          <a href="/index.php?route=product/category&path=<?php echo $product['collection']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="card__good-image"></a>
        </div>
        <?php } ?> -->
        </div>
      </div>

      <div class="card" id="divany_s_kushetkoi">
        <div class="card__header">
          <h3>Диваны с кушеткой</h3>
          <a class="card-close">&#10006;</a>
        </div>
        <div class="card__content">
          <div class="card__good">
            <a href="/index.php?route=product/category&path=67">
              <h4 class="card__good-name">CAMILA</h4><img src="img/cards_tovar/kushetka/camila.jpg" alt="Camila" title="Camila" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=71">
              <h4 class="card__good-name">RAIMO</h4><img src="img/cards_tovar/kushetka/raimo.jpg" alt="Raimo" title="Raimo" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=72">
              <h4 class="card__good-name">RAUMA</h4><img src="img/cards_tovar/kushetka/rauma.jpg" alt="Rauma" title="Rauma" class="card__good-image">
            </a>
          </div>
          <!-- <?php foreach ($products_divany_s_kushetkoi as $product) { ?>
        <div class="card__good">
          <h4 class="card__good-name"><?php echo $product['name']; ?></h4>
          <a href="/index.php?route=product/category&path=<?php echo $product['collection']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="card__good-image"></a>
        </div>
        <?php } ?> -->
        </div>
      </div>

      <!-- <div class="card" id="modulnie_divany">
      <div class="card__header">
        <h3>Модульные диваны</h3>
        <a class="card-close">&#10006;</a>
      </div>
      <div class="card__content">
        <div class="card__good">
          <h4 class="card__good-name">Rauma</h4>
          <a href="/index.php?route=product/category&path=1557"><img src="img/cards_tovar/mod_divany/rauma.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Camila</h4>
          <a href="/index.php?route=product/category&path=1549"><img src="img/cards_tovar/mod_divany/camila.jpg" alt="" class="card__good-image"></a>
        </div>
      </div>
    </div> -->

      <div class="card" id="kresla">
        <div class="card__header">
          <h3>Кресла</h3>
          <a class="card-close">&#10006;</a>
        </div>
        <div class="card__content">
          <div class="card__good">
            <a href="/index.php?route=product/category&path=71">
              <h4 class="card__good-name">RAIMO</h4><img src="img/cards_tovar/kreslo/raimo.jpg" alt="Raimo" title="Raimo" class="card__good-image">
            </a>
          </div>
          <div class="card__good">
            <a href="/index.php?route=product/category&path=89">
              <h4 class="card__good-name">JENNY</h4><img src="img/cards_tovar/kreslo/jenny.jpg" alt="Jenny" title="Jenny" class="card__good-image">
            </a>
          </div>
          <!-- <?php foreach ($products_kresla as $product) { ?>
        <div class="card__good">
          <h4 class="card__good-name"><?php echo $product['name']; ?></h4>
          <a href="/index.php?route=product/category&path=<?php echo $product['collection']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="card__good-image"></a>
        </div>
        <?php } ?> -->
        </div>
      </div>

      <div class="card" id="kreslo-reklainer">
        <div class="card__header">
          <h3>Кресла-реклайнеры</h3>
          <a class="card-close">&#10006;</a>
        </div>
        <div class="card__content">
          <div class="card__good">
            <a href="/index.php?route=product/category&path=91">
              <h4 class="card__good-name">DANY</h4><img src="img/cards_tovar/kreslo-reklainer/dany.jpg" alt="Dany" title="Dany" class="card__good-image">
            </a>
          </div>
        </div>
      </div>

      <div class="card" id="krovati">
        <div class="card__header">
          <h3>Кровати</h3>
          <a class="card-close">&#10006;</a>
        </div>
        <div class="card__content">
          <div class="card__good">
            <a href="/index.php?route=product/category&path=68">
              <h4 class="card__good-name">COMFORT</h4><img src="img/cards_tovar/krovati/comfort.jpg" alt="Comfort" title="Comfort" class="card__good-image">
            </a>
          </div>
          <!-- <?php foreach ($products_krovati as $product) { ?>
        <div class="card__good">
          <h4 class="card__good-name"><?php echo $product['name']; ?></h4>
          <a href="/index.php?route=product/category&path=<?php echo $product['collection']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="card__good-image"></a>
        </div>
        <?php } ?> -->
        </div>
      </div>

      <!-- <div class="card" id="matrasi">
      <div class="card__header">
        <h3>Матрасы</h3>
        <a class="card-close">&#10006;</a>
      </div>
      <div class="card__content">
        <div class="card__good">
          <h4 class="card__good-name">Astoria</h4>
          <a href="/index.php?route=product/category&path=1556"><img src="img/cards_tovar/matras/astoria.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Basic</h4>
          <a href="/index.php?route=product/category&path=1554"><img src="img/cards_tovar/matras/basic.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Comfort I</h4>
          <a href="/index.php?route=product/category&path=1555"><img src="img/cards_tovar/matras/comfort1.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Comfort II</h4>
          <a href="/index.php?route=product/category&path=1555"><img src="img/cards_tovar/matras/comfort2.jpg" alt="" class="card__good-image"></a>
        </div>
      </div>
    </div> -->

      <!-- <div class="card" id="namatrasniki">
      <div class="card__header">
        <h3>Наматрасники</h3>
        <a class="card-close">&#10006;</a>
      </div>
      <div class="card__content">
        <div class="card__good">
          <h4 class="card__good-name">Наматрасник I</h4>
          <a href="/index.php?route=product/category&path=1538"><img src="img/cards_tovar/namatrasniki/1.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Наматрасник II</h4>
          <a href="/index.php?route=product/category&path=1538"><img src="img/cards_tovar/namatrasniki/2.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Наматрасник III</h4>
          <a href="/index.php?route=product/category&path=1538"><img src="img/cards_tovar/namatrasniki/3.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Наматрасник IV</h4>
          <a href="/index.php?route=product/category&path=1538"><img src="img/cards_tovar/namatrasniki/4.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Наматрасник V</h4>
          <a href="/index.php?route=product/category&path=1538"><img src="img/cards_tovar/namatrasniki/5.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Наматрасник VI</h4>
          <a href="/index.php?route=product/category&path=1538"><img src="img/cards_tovar/namatrasniki/6.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Наматрасник VII</h4>
          <a href="/index.php?route=product/category&path=1538"><img src="img/cards_tovar/namatrasniki/7.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Наматрасник VIII</h4>
          <a href="/index.php?route=product/category&path=1538"><img src="img/cards_tovar/namatrasniki/8.jpg" alt="" class="card__good-image"></a>
        </div>
      </div>
    </div> -->

      <!-- <div class="card" id="izgoloviya">
      <div class="card__header">
        <h3>Изголовья</h3>
        <a class="card-close">&#10006;</a>
      </div>
      <div class="card__content">
        <div class="card__good">
          <h4 class="card__good-name">Изголовье I</h4>
          <a href="/index.php?route=product/category&path=1539"><img src="img/cards_tovar/izgolovie/1.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Изголовье II</h4>
          <a href="/index.php?route=product/category&path=1539"><img src="img/cards_tovar/izgolovie/2.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Изголовье III</h4>
          <a href="/index.php?route=product/category&path=1539"><img src="img/cards_tovar/izgolovie/3.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Изголовье IV</h4>
          <a href="/index.php?route=product/category&path=1539"><img src="img/cards_tovar/izgolovie/4.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Изголовье V</h4>
          <a href="/index.php?route=product/category&path=1539"><img src="img/cards_tovar/izgolovie/5.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Изголовье VII</h4>
          <a href="/index.php?route=product/category&path=1539"><img src="img/cards_tovar/izgolovie/6.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Изголовье VIII</h4>
          <a href="/index.php?route=product/category&path=1539"><img src="img/cards_tovar/izgolovie/7.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Изголовье XI</h4>
          <a href="/index.php?route=product/category&path=1539"><img src="img/cards_tovar/izgolovie/8.jpg" alt="" class="card__good-image"></a>
        </div>
        <div class="card__good">
          <h4 class="card__good-name">Изголовье XII</h4>
          <a href="/index.php?route=product/category&path=1539"><img src="img/cards_tovar/izgolovie/9.jpg" alt="" class="card__good-image"></a>
        </div>
      </div>
    </div> -->

      <div class="card" id="aksessyary">
        <div class="card__header">
          <h3>Аксессуары</h3>
          <a class="card-close">&#10006;</a>
        </div>
        <div class="card__content">
          <?php foreach ($products_aksessyary as $product) { ?>
            <div class="card__good">
              <h4 class="card__good-name"><?php echo $product['name']; ?></h4>
              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="card__good-image"></a>
            </div>
          <?php } ?>
        </div>
      </div>

    </div>

    <div class="close-menu-btn">
      <button type="button" title="Скрыть меню">&#10006;</button>
    </div>
    <!------------------->
    <nav id="nav__basemenu" class="navbar navbar-default">
      <!-- <div class="dots"></div> -->
      <div class="container">
        <div class="menu_base">
          <div class="navbar-header">
            <span class="mobsite">
              <div class="row">
                <div class="col-xs-8 logo-wrapper">
                  <a href="/">
                    <img src="img/logo.svg" class="mobile-logo" alt="">
                  </a>
                </div>
                <div class="col-xs-4">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>
              </div>
            </span>
          </div>
          <div id="navbar" class="navbar-collapse collapse nav-shadow-inset navbar-container">
            <div class="nav navbar-nav row">

              <div class="col-xs-12 col-sm-4 col-lg-4 search-block"><?php echo $search; ?></div>
              <div class="col-xs-12 col-sm-3 col-lg-3">
                <a href="/index.php?route=checkout/cart">
                  <div id="cart" class="btn-group">
                    <button type="button" class="btn btn-inverse btn-block btn-lg">
                      <div class="cart-text">
                        <div class="cart-name">Корзина</div>
                        <div id="cart-total"><?php echo $text_items; ?></div>
                      </div>
                    </button>
                  </div>
                </a>
              </div>
              <div class="col-xs-12 col-sm-2 col-lg-2 callme-block">
                <button class="btn" data-toggle="modal" data-target="#callme-modal">
                  <img src="img/telephone.png" alt="Позвоните нам">
                  <span>Позвоните нам</span>
                </button>
              </div>
              <div class="col-xs-12 col-sm-3 col-lg-3 city-select-block">
                <?php echo $city_select; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>

    </div>

    <div id="city-select-modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="box-content text-center">
            <select name="store" id="store" onchange="location = this.value">
              <?php foreach ($stores as $store) { ?>
                <?php if ($store['store_id'] == $store_id) { ?>
                  <option value="<?php echo $store['url']; ?>" selected="selected"><?php echo $store['name']; ?></option>
                <?php } else { ?>
                  <option value="<?php echo $store['url']; ?>"><?php echo $store['name']; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
      </div>
    </div>

    <div id="feedback" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myLargeModalLabel">Обратная связь</h4>
          </div>
          <div class="modal-body">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="ФИО" aria-describedby="sizing-addon2">
            </div>
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Почта" aria-describedby="sizing-addon2">
            </div>
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Телефон" aria-describedby="sizing-addon2">
            </div>
            <div class="input-group">
              <textarea></textarea>
            </div>

            <button type="button" class="btn btn-primary">Отправить</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="callme-modal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Позвоните нам</h3>
          </div>
          <div class="modal-body">
            <form class="callme-form">
              <input type="text" placeholder="Имя" class="form-control" id="callme-name">
              <input type="text" placeholder="Телефон" class="form-control" id="callme-phone" id="callme-phone">
            </form>
            <p class="callme-result"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="callme-submit">Отправить</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="additional-contacts-modal">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Другие адреса</h3>
          </div>
          <div class="modal-body">
            <div class="modal-contact-wrapper">
              <?php foreach ($additional_contacts as $additional_contact) { ?>
                <div class="modal-contact-card">
                  <div class="modal-contact-card-content">
                    <p class="name"><?php echo $additional_contact['name']; ?></p>
                    <p class="work-time"><i class="fa fa-calendar-o" aria-hidden="true"></i> <?php echo $additional_contact['work_time']; ?></p>
                    <p class="email"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $additional_contact['email']; ?></p>
                    <p class="address"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $additional_contact['address']; ?></p>
                  </div>
                </div>
              <?php } ?>
            </div>
            <?php echo $map_script; ?>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-city">
      <!--Select city dialog-->
      <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="city-select-modal-mobile">
        <div class="modal-dialog modal-md">
          <div class="modal-content mwidth">
            <center>
              <div class="modal-header">

                <?php //if ($selected_city) { 
                ?>
                <!-- <button
                            type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button> -->
                <?php //} else { 
                ?>
                <!-- <script>
                        $('.bs-example-modal-sm').modal({show: true});
                    </script> -->
                <?php //} 
                ?>

                <h4 class="modal-title" id="mySmallModalLabel">Выберите интересующий Вас регион: </h4>

              </div>
              <div class="modal-body">
                <?php foreach ($cities as $key => $value) { ?>
                  <input type="button" class="btn btn-default fontmodal" onclick="
                                $('#city-id').val('<?php echo $key; ?>');
                                $('#form-city').submit();" value="<?php echo $value["name"]; ?>" />
                <?php } ?>
              </div>
            </center>
          </div>
        </div>
      </div>

      <input id="city-id" type="hidden" name="cityId" value="" />
      <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
    </form>

    <div class="row">
      <div id="content" class="col-sm-12 slider-block">
        <div id="slideshow0" class="main-slider"></div>

        <script type="text/javascript">
          $('#slideshow0').vegas({
            delay: 7000,
            timer: true,
            shuffle: true,
            animation: 'random',
            transitionDuration: 2000,
            slides: [{
                src: '/img/scd_slider/sld1.jpg'
              },
              {
                src: '/img/scd_slider/sld2.jpg'
              },
              {
                src: '/img/scd_slider/sld3.jpg'
              },
              {
                src: '/img/scd_slider/sld4.jpg'
              },
              {
                src: '/img/scd_slider/sld5.jpg'
              },
              {
                src: '/img/scd_slider/sld6.jpg'
              }
            ]
          });
        </script>
      </div>