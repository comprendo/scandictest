<div class="owl-carousel" id="owl-product-carousel">
    <div class="owl-product">
        <div class="product-layout product-grid">
            <div class="product-thumb">
                <div class="image"><a href="index.php?route=product/product&amp;path=1527&amp;product_id=66"><img src="image/cache/catalog/arttu-1/6395_1arttufame120%283%29-500x500.jpg" alt="Arttu" title="Arttu" class="img-responsive" /></a></div>
                <div>
                    <div class="caption">
                        <h4>
                            <a href="index.php?route=product/product&amp;path=1527&amp;product_id=66"><b>Arttu</b></a>
                        </h4>
                        <!-- <p>Модель RAIMO 2&nbsp;компактна и лаконична в дизайне, гармонично дополнит любой интерьер. Модель буде..</p> -->
                        <p class="price">
                            <span style="font-size: 18px"><b>86 700&nbsp; руб.</b></span><br>
                            <span>Доступность: <span>предзаказ</span></span>                                                                        
                        </p>
                    </div>
                    <div class="button-group" style="background: #cda44a;">
                        <!--<button class="button__cart__but" type="button" onclick="cart.add('4', $(this).parent().parent().find('.quantity_input').val())"><span class="hidden-xs hidden-sm hidden-md">Купить</span></button>-->
                        <button class="add-to-cart-button" type="button" onclick="cart.add('66', 1)"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">Купить</span></button>
                        <!-- <input type="number" name="quantity" value="1" size="2" min="1" id="input-quantity" class="quantity_input p_q_<b>Notice</b>: Undefined variable: product_id in <b>/home/r/ryzhovvay2/scandicstyle/public_html/catalog/view/theme/default/template/product/category.tpl</b> on line <b>173</b>" />  -->
                        <!--<button type="button" data-toggle="tooltip" title="В закладки" onclick="wishlist.add('4');"><i class="fa fa-heart"></i></button>
                            <button type="button" data-toggle="tooltip" title="В сравнение" onclick="compare.add('4');"><i class="fa fa-exchange"></i></button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-product">
        <div class="product-layout product-grid">
            <div class="product-thumb">
                <div class="image"><a href="index.php?route=product/product&amp;path=1527&amp;product_id=113"><img src="image/cache/catalog/annika/annika-500x500.jpg" alt="Annika" title="Annika" class="img-responsive" /></a></div>
                <div>
                    <div class="caption">
                        <h4>
                            <a href="index.php?route=product/product&amp;path=1527&amp;product_id=113"><b>Annika</b></a>
                        </h4>
                        <!-- <p>Модель&nbsp;GABRIEL&nbsp;компактна и лаконична в дизайне, гармонично дополнит любой интерьер. Модель..</p> -->
                        <p class="price">
                            <span style="font-size: 18px"><b>73 575&nbsp; руб.</b></span><br>
                            <span >Доступность: <span>предзаказ</span></span>                                                                        
                        </p>
                    </div>
                    <div class="button-group" style="background: #cda44a;">
                        <!--<button class="button__cart__but" type="button" onclick="cart.add('6', $(this).parent().parent().find('.quantity_input').val())"><span class="hidden-xs hidden-sm hidden-md">Купить</span></button>-->
                        <button class="add-to-cart-button" type="button" onclick="cart.add('113', 1)"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">Купить</span></button>
                        <!-- <input type="number" name="quantity" value="1" size="2" min="1" id="input-quantity" class="quantity_input p_q_<b>Notice</b>: Undefined variable: product_id in <b>/home/r/ryzhovvay2/scandicstyle/public_html/catalog/view/theme/default/template/product/category.tpl</b> on line <b>173</b>" />  -->
                        <!--<button type="button" data-toggle="tooltip" title="В закладки" onclick="wishlist.add('6');"><i class="fa fa-heart"></i></button>
                            <button type="button" data-toggle="tooltip" title="В сравнение" onclick="compare.add('6');"><i class="fa fa-exchange"></i></button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-product">
        <div class="product-layout product-grid">
            <div class="product-thumb">
                <div class="image"><a href="index.php?route=product/product&amp;path=1527&amp;product_id=69"><img src="image/cache/catalog/eetu/6395_6_eetuthomas20%282%29-500x500.jpg" alt="Eetu" title="Eetu" class="img-responsive" /></a></div>
                <div>
                    <div class="caption">
                        <h4>
                            <a href="index.php?route=product/product&amp;path=1527&amp;product_id=69"><b>Eetu</b></a>
                        </h4>
                        <!-- <p>Модель&nbsp;GABRIEL&nbsp;компактна и лаконична в дизайне, гармонично дополнит любой интерьер. Модель..</p> -->
                        <p class="price">
                            <span style="font-size: 18px"><b>136 200&nbsp; руб.</b></span><br>
                            <span >Доступность: <span>предзаказ</span></span>                                                                        
                        </p>
                    </div>
                    <div class="button-group" style="background: #cda44a;">
                        <!--<button class="button__cart__but" type="button" onclick="cart.add('6', $(this).parent().parent().find('.quantity_input').val())"><span class="hidden-xs hidden-sm hidden-md">Купить</span></button>-->
                        <button class="add-to-cart-button" type="button" onclick="cart.add('69', 1)"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">Купить</span></button>
                        <!-- <input type="number" name="quantity" value="1" size="2" min="1" id="input-quantity" class="quantity_input p_q_<b>Notice</b>: Undefined variable: product_id in <b>/home/r/ryzhovvay2/scandicstyle/public_html/catalog/view/theme/default/template/product/category.tpl</b> on line <b>173</b>" />  -->
                        <!--<button type="button" data-toggle="tooltip" title="В закладки" onclick="wishlist.add('6');"><i class="fa fa-heart"></i></button>
                            <button type="button" data-toggle="tooltip" title="В сравнение" onclick="compare.add('6');"><i class="fa fa-exchange"></i></button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-product">
        <div class="product-layout product-grid">
            <div class="product-thumb">
                <div class="image"><a href="index.php?route=product/product&amp;path=1531&amp;product_id=81"><img src="image/cache/catalog/raimo/6395_16raimoetna25%281%29-500x500.jpg" alt="Raimo 3N с кушеткой" title="Raimo 3N с кушеткой" class="img-responsive" /></a></div>
                <div>
                    <div class="caption">
                        <h4>
                            <a href="index.php?route=product/product&amp;path=1531&amp;product_id=81"><b>Raimo 3N с кушеткой</b></a>
                        </h4>
                        <!-- <p>Модель&nbsp;GABRIEL&nbsp;компактна и лаконична в дизайне, гармонично дополнит любой интерьер. Модель..</p> -->
                        <p class="price">
                            <span style="font-size: 18px"><b>93 900&nbsp; руб.</b></span><br>
                            <span >Доступность: <span>предзаказ</span></span>                                                                        
                        </p>
                    </div>
                    <div class="button-group" style="background: #cda44a;">
                        <!--<button class="button__cart__but" type="button" onclick="cart.add('6', $(this).parent().parent().find('.quantity_input').val())"><span class="hidden-xs hidden-sm hidden-md">Купить</span></button>-->
                        <button class="add-to-cart-button" type="button" onclick="cart.add('81', 1)"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">Купить</span></button>
                        <!-- <input type="number" name="quantity" value="1" size="2" min="1" id="input-quantity" class="quantity_input p_q_<b>Notice</b>: Undefined variable: product_id in <b>/home/r/ryzhovvay2/scandicstyle/public_html/catalog/view/theme/default/template/product/category.tpl</b> on line <b>173</b>" />  -->
                        <!--<button type="button" data-toggle="tooltip" title="В закладки" onclick="wishlist.add('6');"><i class="fa fa-heart"></i></button>
                            <button type="button" data-toggle="tooltip" title="В сравнение" onclick="compare.add('6');"><i class="fa fa-exchange"></i></button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-product">
        <div class="product-layout product-grid">
            <div class="product-thumb">
                <div class="image"><a href="index.php?route=product/product&amp;path=1533&amp;product_id=96"><img src="image/cache/catalog/raimo/kreslo%281%29-500x500.jpg" alt="Кресло Raimo" title="Кресло Raimo" class="img-responsive" /></a></div>
                <div>
                    <div class="caption">
                        <h4>
                            <a href="index.php?route=product/product&amp;path=1533&amp;product_id=96"><b>Кресло Raimo</b></a>
                        </h4>
                        <!-- <p>Модель&nbsp;GABRIEL&nbsp;компактна и лаконична в дизайне, гармонично дополнит любой интерьер. Модель..</p> -->
                        <p class="price">
                            <span style="font-size: 18px"><b>65 625&nbsp; руб.</b></span><br>
                            <span >Доступность: <span>предзаказ</span></span>                                                                        
                        </p>
                    </div>
                    <div class="button-group" style="background: #cda44a;">
                        <!--<button class="button__cart__but" type="button" onclick="cart.add('6', $(this).parent().parent().find('.quantity_input').val())"><span class="hidden-xs hidden-sm hidden-md">Купить</span></button>-->
                        <button class="add-to-cart-button" type="button" onclick="cart.add('96', 1)"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">Купить</span></button>
                        <!-- <input type="number" name="quantity" value="1" size="2" min="1" id="input-quantity" class="quantity_input p_q_<b>Notice</b>: Undefined variable: product_id in <b>/home/r/ryzhovvay2/scandicstyle/public_html/catalog/view/theme/default/template/product/category.tpl</b> on line <b>173</b>" />  -->
                        <!--<button type="button" data-toggle="tooltip" title="В закладки" onclick="wishlist.add('6');"><i class="fa fa-heart"></i></button>
                            <button type="button" data-toggle="tooltip" title="В сравнение" onclick="compare.add('6');"><i class="fa fa-exchange"></i></button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-product">
        <div class="product-layout product-grid">
            <div class="product-thumb">
                <div class="image"><a href="index.php?route=product/product&amp;path=1527&amp;product_id=116"><img src="image/cache/catalog/sella/sella-500x500.jpg" alt="Sella" title="Sella" class="img-responsive" /></a></div>
                <div>
                    <div class="caption">
                        <h4>
                            <a href="index.php?route=product/product&amp;path=1527&amp;product_id=116"><b>Sella</b></a>
                        </h4>
                        <!-- <p>Модель&nbsp;GABRIEL&nbsp;компактна и лаконична в дизайне, гармонично дополнит любой интерьер. Модель..</p> -->
                        <p class="price">
                            <span style="font-size: 18px"><b>103 950&nbsp; руб.</b></span><br>
                            <span >Доступность: <span>предзаказ</span></span>                                                                        
                        </p>
                    </div>
                    <div class="button-group" style="background: #cda44a;">
                        <!--<button class="button__cart__but" type="button" onclick="cart.add('6', $(this).parent().parent().find('.quantity_input').val())"><span class="hidden-xs hidden-sm hidden-md">Купить</span></button>-->
                        <button class="add-to-cart-button" type="button" onclick="cart.add('116', 1)"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">Купить</span></button>
                        <!-- <input type="number" name="quantity" value="1" size="2" min="1" id="input-quantity" class="quantity_input p_q_<b>Notice</b>: Undefined variable: product_id in <b>/home/r/ryzhovvay2/scandicstyle/public_html/catalog/view/theme/default/template/product/category.tpl</b> on line <b>173</b>" />  -->
                        <!--<button type="button" data-toggle="tooltip" title="В закладки" onclick="wishlist.add('6');"><i class="fa fa-heart"></i></button>
                            <button type="button" data-toggle="tooltip" title="В сравнение" onclick="compare.add('6');"><i class="fa fa-exchange"></i></button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-product">
        <div class="product-layout product-grid">
            <div class="product-thumb">
                <div class="image"><a href="index.php?route=product/product&amp;path=1528&amp;product_id=118"><img src="image/cache/catalog/camila/camila_ugol-500x500.jpg" alt="Угловой диван Camila" title="Угловой диван Camila" class="img-responsive" /></a></div>
                <div>
                    <div class="caption">
                        <h4>
                            <a href="index.php?route=product/product&amp;path=1528&amp;product_id=118"><b>Угловой диван Camila</b></a>
                        </h4>
                        <!-- <p>Модель&nbsp;GABRIEL&nbsp;компактна и лаконична в дизайне, гармонично дополнит любой интерьер. Модель..</p> -->
                        <p class="price">
                            <span style="font-size: 18px"><b>128 250&nbsp; руб.</b></span><br>
                            <span >Доступность: <span>предзаказ</span></span>                                                                        
                        </p>
                    </div>
                    <div class="button-group" style="background: #cda44a;">
                        <!--<button class="button__cart__but" type="button" onclick="cart.add('6', $(this).parent().parent().find('.quantity_input').val())"><span class="hidden-xs hidden-sm hidden-md">Купить</span></button>-->
                        <button class="add-to-cart-button" type="button" onclick="cart.add('118', 1)"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">Купить</span></button>
                        <!-- <input type="number" name="quantity" value="1" size="2" min="1" id="input-quantity" class="quantity_input p_q_<b>Notice</b>: Undefined variable: product_id in <b>/home/r/ryzhovvay2/scandicstyle/public_html/catalog/view/theme/default/template/product/category.tpl</b> on line <b>173</b>" />  -->
                        <!--<button type="button" data-toggle="tooltip" title="В закладки" onclick="wishlist.add('6');"><i class="fa fa-heart"></i></button>
                            <button type="button" data-toggle="tooltip" title="В сравнение" onclick="compare.add('6');"><i class="fa fa-exchange"></i></button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#owl-product-carousel').owlCarousel({
      navigation: true,
      navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
      pagination: false,
      autoPlay: 3000,
      rewindNav: false
    });
</script>