<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <meta property="og:title" content="<?php echo $title; ?>">
    <meta property="og:site_name" content="Scandicstyle">
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; ?>" />
        <meta property="og:description" content="<?php echo $description; ?>">
    <?php } ?>
    <?php if ($keywords) { ?>
        <meta name="keywords" content="<?php echo $keywords; ?>" />
    <?php } ?>
    <meta property="og:image" content="https://scandicstyle.ru/img/ogimage.png">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#ffffff">
    <!-- / Favicon -->

    <!-- Styles -->
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css?v6.7" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/menu_fixed.css" rel="stylesheet">
    <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/firefox_fix.css">
    <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <!-- / Styles -->

    <!-- Scripts -->
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <!-- / Scripts -->

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function(m, e, t, r, i, k, a) {
            m[i] = m[i] || function() {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(57373848, "init", {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true
        });
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/57373848" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function(m, e, t, r, i, k, a) {
            m[i] = m[i] || function() {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(56785456, "init", {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true
        });
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/56785456" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154938839-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-154938839-1');
    </script>
</head>

<body class="<?php echo $class; ?>">
    <main id="hf-container">
        <aside class="left-menu">

            <div class="left-menu__content">

                <a class="left-menu__row left-menu__trigger" href="direct-sofas">
                    <div class="left-menu__icon">
                        <img src="img/icons/menu_svg/straight_sofa.svg" alt="">
                    </div>
                    <div class="left-menu__text">Прямые диваны</div>
                </a>

                <a class="left-menu__row left-menu__trigger" href="corner-sofas">
                    <div class="left-menu__icon">
                        <img src="img/icons/menu_svg/corner_sofa.svg" alt="">
                    </div>
                    <div class="left-menu__text">Угловые диваны</div>
                </a>

                <a class="left-menu__row left-menu__trigger" href="sofas-with-daybed">
                    <div class="left-menu__icon">
                        <img src="img/icons/menu_svg/sofa_with_daybed.svg" alt="">
                    </div>
                    <div class="left-menu__text">Диваны с кушеткой</div>
                </a>

                <a class="left-menu__row left-menu__trigger" href="armchairs">
                    <div class="left-menu__icon">
                        <img src="img/icons/menu_svg/armchair.svg" alt="">
                    </div>
                    <div class="left-menu__text">Кресла</div>
                </a>

                <a class="left-menu__row left-menu__trigger" href="recliners">
                    <div class="left-menu__icon">
                        <img src="img/icons/menu_svg/recliner.svg" alt="">
                    </div>
                    <div class="left-menu__text">Кресла-реклайнеры</div>
                </a>
                
                <a class="left-menu__row left-menu__trigger" href="daybeds">
                    <div class="left-menu__icon">
                        <img src="img/icons/menu_svg/daybed.svg" alt="">
                    </div>
                    <div class="left-menu__text">Кушетки</div>
                </a>

                <a class="left-menu__row left-menu__trigger" href="beds">
                    <div class="left-menu__icon">
                        <img src="img/icons/new/krovat.png" alt="">
                    </div>
                    <div class="left-menu__text">Кровати</div>
                </a>

                <a class="left-menu__row left-menu__trigger" href="accessories">
                    <div class="left-menu__icon">
                        <img src="img/icons/cat-acessories.png" alt="">
                    </div>
                    <div class="left-menu__text">Аксессуары</div>
                </a>

                <a class="left-menu__row left-menu__trigger" href="stocks">
                    <div class="left-menu__icon">
                        <img src="img/icons/menu_svg/sale.svg" alt="">
                    </div>
                    <div class="left-menu__text">Акции</div>
                </a>

            </div><!-- /.left-menu__content -->

            <div class="left-menu__footer">
                <a class="left-menu__row left-menu__trigger" href="https://www.instagram.com/scandicstyle_/">
                    <div class="left-menu__icon">
                        <img src="img/icons/social_svg/instagram.svg" alt="Instagram">
                    </div>
                    <div class="left-menu__text">Instagram</div>
                </a>
                <a class="left-menu__row left-menu__trigger" href="https://vk.com/scandicstyle">
                    <div class="left-menu__icon">
                        <img src="img/icons/social_svg/vk.svg" alt="ВКонтакте">
                    </div>
                    <div class="left-menu__text">ВКонтакте</div>
                </a>
                <a class="left-menu__row left-menu__trigger" href="https://www.facebook.com/stylescandic">
                    <div class="left-menu__icon">
                        <img src="img/icons/social_svg/facebook.svg" alt="Facebook">
                    </div>
                    <div class="left-menu__text">Facebook</div>
                </a>
            </div>
            <!--/.left-menu__footer-->
        </aside>

        <nav id="nav__basemenu" class="navbar navbar-default">
            <div class="container">
                <div class="menu_base">
                    <div class="navbar-header">
                        <span class="mobsite">
                            <div class="row">
                                <div class="col-xs-6 logo-wrapper">
                                    <a href="<?php echo $home; ?>">
                                        <img class="mobile-logo" src="img/logo_new.svg" alt="">
                                    </a>
                                </div>
                                <div class="col-xs-2 text-center">
                                    <div class="callme-block">
                                        <button class="btn" data-toggle="modal" data-target="#callme-modal">
                                            <img class="callme-icon" src="img/icons/callme-phone.png" alt="Позвоните нам">
                                            <img class="callme-icon-hover" src="img/icons/callme-phone-hover.png" alt="Позвоните нам">
                                        </button>
                                    </div>
                                </div>
                                <div class="col-xs-2 text-center">
                                    <div class="cart-block">
                                        <a class="cart-button" href="cart">
                                            <?php if ($products) { ?>
                                                <img src="img/icons/shopping-cart-items.png" alt="">
                                            <?php } else { ?>
                                                <img src="img/icons/shopping-cart-empty.png" alt="">
                                            <?php } ?>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                            </div>
                        </span>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse nav-shadow-inset navbar-container">
                        <div class="nav navbar-nav row main-nav">
                            <div class="catalog-block-mobile hidden-sm hidden-md hidden-lg">
                                <a class="catalog-block__item catalog-item" href="direct-sofas">
                                    <div class="catalog-item__icon">
                                        <img src="img/icons/mobile-catalog/straight_sofa.png" alt="">
                                    </div>
                                    <div class="catalog-item__text">Прямые диваны</div>
                                </a>

                                <a class="catalog-block__item catalog-item" href="corner-sofas">
                                    <div class="catalog-item__icon">
                                        <img src="img/icons/mobile-catalog/corner_sofa.png" alt="">
                                    </div>
                                    <div class="catalog-item__text">Угловые диваны</div>
                                </a>

                                <a class="catalog-block__item catalog-item" href="sofas-with-daybed">
                                    <div class="catalog-item__icon">
                                        <img src="img/icons/mobile-catalog/sofa_with_daybed.png" alt="">
                                    </div>
                                    <div class="catalog-item__text">Диваны с кушеткой</div>
                                </a>

                                <a class="catalog-block__item catalog-item" href="armchairs">
                                    <div class="catalog-item__icon">
                                        <img src="img/icons/mobile-catalog/armchair.png" alt="">
                                    </div>
                                    <div class="catalog-item__text">Кресла</div>
                                </a>

                                <a class="catalog-block__item catalog-item" href="recliners">
                                    <div class="catalog-item__icon">
                                        <img src="img/icons/mobile-catalog/recliner.png" alt="">
                                    </div>
                                    <div class="catalog-item__text">Кресла-реклайнеры</div>
                                </a>

                                <a class="catalog-block__item catalog-item" href="beds">
                                    <div class="catalog-item__icon">
                                        <img src="img/icons/mobile-catalog/krovat.png" alt="">
                                    </div>
                                    <div class="catalog-item__text">Кровати</div>
                                </a>

                                <a class="catalog-block__item catalog-item" href="accessories">
                                    <div class="catalog-item__icon">
                                        <img src="img/icons/mobile-catalog/cat-acessories.png" alt="">
                                    </div>
                                    <div class="catalog-item__text">Аксессуары</div>
                                </a>

                                <a class="catalog-block__item catalog-item" href="stocks">
                                    <div class="catalog-item__icon">
                                        <img src="img/icons/mobile-catalog/sale.png" alt="">
                                    </div>
                                    <div class="catalog-item__text">Акции</div>
                                </a>
                            </div>
                            <div class="mobile-menu-line"></div>
                            <div class="city-select-block">
                                <?php echo $city_select; ?>
                            </div>
                            <div class="search-block">
                                <div id="search">
                                    <div class="button-search btn-lg"><img src="img/icons/search.png" alt="Поиск"></div>
                                    <input class="input-lg" type="text" name="filter_name" placeholder="Поиск товаров по сайту" value="<?php echo $filter_name; ?>" />
                                </div>
                            </div>
                            <div class="logo-block hidden-xs">
                                <a href="<?php echo $home; ?>">
                                    <img src="img/logo_new.svg" alt="">
                                </a>
                            </div>
                            <div class="menu-block">
                                <a href="contacts">Контакты</a>
                            </div>
                            <div class="social-block">
                                <a class="instagram-link" href="https://www.instagram.com/scandicstyle_/"><img src="img/icons/social_svg/instagram.svg" alt="Instagram"></a>
                                <a class="vk-link" href="https://vk.com/scandicstyle"><img src="img/icons/social_svg/vk.svg" alt="ВКонтакте"></a>
                                <a class="facebook-link" href="https://www.facebook.com/stylescandic"><img src="img/icons/social_svg/facebook.svg" alt="Facebook"></a>
                            </div>
                            <div class="callme-block hidden-xs">
                                <button class="btn" data-toggle="modal" data-target="#callme-modal">
                                    <img class="callme-icon" src="img/icons/callme-phone.png" alt="Позвоните нам">
                                    <img class="callme-icon-hover" src="img/icons/callme-phone-hover.png" alt="Позвоните нам">
                                </button>
                            </div>
                            <div class="cart-block hidden-xs">
                                <a class="cart-button" href="cart">
                                    <?php if ($products) { ?>
                                        <img src="img/icons/shopping-cart-items.png" alt="">
                                    <?php } else { ?>
                                        <img src="img/icons/shopping-cart-empty.png" alt="">
                                    <?php } ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        </div>

        <div class="modal fade" tabindex="-1" role="dialog" id="callme-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title">Позвоните нам</h3>
                    </div>
                    <div class="modal-body">
                        <form class="callme-form">
                            <input type="text" placeholder="Имя" class="form-control" id="callme-name">
                            <input type="text" placeholder="Телефон" class="form-control" id="callme-phone" id="callme-phone">
                        </form>
                        <p class="callme-result"></p>
                    </div>
                    <div class="modal-footer modal-privacy">
                        <p>Нажимая на кнопку "Отправить" Вы даёте <a href="files/ur_info/soglashenie.pdf" target="_blank">согласие на обработку персональных данных</a>.</p>
                        <button type="button" class="btn btn-primary" id="callme-submit">Отправить</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->