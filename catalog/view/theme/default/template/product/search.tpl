<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div class="container-block search-page">
    <div class="container-inner">
        <div id="content"><?php echo $content_top; ?>
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb">
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                        <?php } ?>
                    </ul>
                    <h1><?php echo $heading_title; ?></h1>
                </div>
            </div>
            <?php if ($products) { ?>
                <div class="row">
                    <?php foreach ($products as $product) { ?>
                        <?php if ($product['price'] > 0) { ?>
                            <div class="product-list product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="product-thumb">
                                    <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                                    <div>
                                        <div class="caption">
                                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                            <?php if ($product['price']) { ?>
                                                <p class="price">
                                                    <?php if (!$product['special']) { ?>
                                                        <span style="color: black; font-size: 18px; font-weight: bold;"><?php echo $product['price']; ?></span><br>
                                                    <?php } else { ?>
                                                        <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                                    <?php } ?>
                                                </p>
                                            <?php } ?>
                                            <?php if ($product['rating']) { ?>
                                                <div class="rating">
                                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                        <?php if ($product['rating'] < $i) { ?>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <?php } else { ?>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="button-group" style="background: #cda44a;">
                                            <button class="add-to-cart-button" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', 1)"><i class="fa fa-shopping-cart"></i> <span><?php echo $button_cart; ?></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pagination"><?php echo $pagination; ?></div>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="content"><?php echo $text_empty; ?></div>
                        </div>
                    </div>
                <?php } ?>
                <?php echo $content_bottom; ?>
                </div>
                <script type="text/javascript">
                    $('#content input[name=\'filter_name\']').keydown(function(e) {
                        if (e.keyCode == 13) {
                            $('#button-search').trigger('click');
                        }
                    });

                    $('#button-search').bind('click', function() {
                        url = 'search';

                        var filter_name = $('#content input[name=\'filter_name\']').attr('value');

                        if (filter_name) {
                            url += '&filter_name=' + encodeURIComponent(filter_name);
                        }

                        var filter_category_id = $('#content select[name=\'filter_category_id\']').attr('value');

                        if (filter_category_id > 0) {
                            url += '&filter_category_id=' + encodeURIComponent(filter_category_id);
                        }

                        var filter_sub_category = $('#content input[name=\'filter_sub_category\']:checked').attr('value');

                        if (filter_sub_category) {
                            url += '&filter_sub_category=true';
                        }

                        var filter_description = $('#content input[name=\'filter_description\']:checked').attr('value');

                        if (filter_description) {
                            url += '&filter_description=true';
                        }

                        location = url;
                    });
                </script>
        </div>
    </div>
    <?php echo $footer; ?>