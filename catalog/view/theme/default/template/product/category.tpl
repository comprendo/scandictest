<?php
setcookie('display', "grid", time()+604800, '/');
?>
<?php echo $header; ?>
<div class="container-block">
    <div class="container-inner">
        <div class="row">
            <div class="col-xs-12">
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    <div class="row">
        <div id="content" class="<?php echo $class; ?> catalog-block"><?php echo $content_top; ?>
        <div class="col-xs-12"><h1 class="category-title"><?php echo $heading_title; ?></h1></div>
        <?php if ($thumb || $description) { ?>
        <div class="row">
            <?php if ($thumb) { ?>
            <div class="col-sm-2 catalog-description"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail big-thumbnail" /></div>
            <?php } ?>
            <?php if ($description) { ?>
            <div class="col-sm-12 catalog-description"><?php echo $description; ?></div>
            <?php } ?>
        </div>
        <hr>
        <?php } ?>
        <?php if ($categories) { ?>
            <div style="margin-bottom: 10px">
                <summary> Подкатегории:</summary>
                <?php if (count($categories) <= 5) { ?>
                <div class="row">
                    <div>
                        <ul class="podr">
                            <?php foreach ($categories as $category) { ?>
                            <? if ($category['goodsamount'] != 0) {  ?>
                            <li><a class="btn btn-primary hoverred"  href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                            <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <?php } else { ?>
                <div class="row">
                    <?php foreach (array_chunk($categories, ceil(count($categories) / 1)) as $categories)  { ?>
                    <div>
                        <ul class="podr">
                            <?php foreach ($categories as $category) { ?>
                            <? if ($category['goodsamount'] != 0) {  ?>
                            <li><a class="btn btn-primary hoverred" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                            <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if ($products) { ?>
        <div class="row">
            <?php foreach ($products as $product) { ?>
            <?php if ($product['price'] > 0) { ?>
            <div class="product-list product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12"> <!-- Если display - list <div class="product-layout product-list col-xs-12"> -->
            <div class="product-thumb">
                <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                <div>
                <div class="caption">
                    <h4>
                    <?php if ($product['stock'] > 0) { ?>
                    <?php if ($product['salespb'] > 0) { ?>
                    <span id="sale_spb"><img src="/image/main/sale_icon.gif" /></span>
                    <?php } ?>
                    <?php if ($product['salemsk'] > 0) { ?>
                    <span id="sale_msk"><img src="/image/main/sale_icon.gif" /></span>
                    <?php } ?>
                    <?php if ($product['salesmr'] > 0) { ?>
                    <span id="sale_smr"><img src="/image/main/sale_icon.gif" /></span>
                    <?php } ?>
                    <?php } ?>
                    <?php if ($product['newspb'] > 0) { ?>
                    <span id="new_spb"><img src="/image/main/new_icon.gif" /></span>
                    <?php } ?>
                    <?php if ($product['newmsk'] > 0) { ?>
                    <span id="new_msk"><img src="/image/main/new_icon.gif" /></span>
                    <?php } ?>
                    <?php if ($product['newsmr'] > 0) { ?>
                    <span id="new_smr"><img src="/image/main/new_icon.gif" /></span>
                    <?php } ?>
                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                    <p><?php echo $product['description']; ?></p>
                    <?php if ($product['price']) { ?>
                    <p class="price">
                    <?php if (!$product['special']) { ?>
                    <?php if (!empty($product['sku'])) { ?>
                        <span>Артикул: <?php echo $product['sku'] ?></span><br>
                    <?php } ?>
                    <span style="color: black; font-size: 18px; font-weight: bold;"><?php echo $product['price']; ?></span><br>
                    <span>Доступность: <?php echo $product['stock']; ?></span>

                    <?php //if ($product['stock'] <= 0) { ?> <?php //echo "<span>Доступность: <span style='color: #5F3241;'>привезем</span></span>"; } ?>
                    <?php //if ($product['stock'] > 0 && $product['stock'] <= 10 && $product['price'] < 1000) { ?> <?php //echo "<span>Доступность: <span style='color: #5F3241;'>мало</span></span>"; } ?>
                    <?php //if ($product['stock'] > 0 && $product['stock'] <= 10 && $product['price'] > 1000) { ?> <?php //echo "<span>Доступность: <span style='color: #5F3241;'>много</span></span>"; } ?>
                    <?php //if ($product['stock'] > 10) { ?> <?php //echo "<span>Доступность: <span style='color: #5F3241;'>много</span></span>"; } ?>
                    
                    <?php } else { ?>
                    <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>


                    <?php //if ($product['tax']) { ?>
                    <!-- <span class="price-tax"><?php //echo $text_tax; ?> <?php //echo $product['tax']; ?></span> -->
                    <?php //} ?>
                    </p>
                    <?php } ?>
                    <?php if ($product['rating']) { ?>
                    <div class="rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($product['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } ?>
                    <?php } ?>
                    </div>
                    <?php } ?>
                </div>
                <div class="button-group" style="background: #5F3241;">
                    <!--<button class="button__cart__but" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', $(this).parent().parent().find('.quantity_input').val())"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>-->
                    <button class="add-to-cart-button" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', 1)"><i class="fa fa-shopping-cart"></i> <span><?php echo $button_cart; ?></span></button>
                    <!-- <input type="number" name="quantity" value="1" size="2" min="1" id="input-quantity" class="quantity_input p_q_<?php echo $product_id; ?>" />  -->
                    <!--<button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                    <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>-->
                </div>
                </div>
            </div>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-sm-6 text-left"><span style="color: #5F3241;">* цены указаны на модели в обивке 1 категории, без учета стоимости доставки</span></div><br>
        </div>
        <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
        <?php } ?>
        <?php if (!$products) { ?>
        <!-- <p><?php echo 'Данный товар отсутствует в выбранном регионе.' ?></p> -->
        <!-- <div class="buttons">
            <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
        </div> -->
        <?php } ?>
        <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
    </div>
</div>
<?php echo $footer; ?>
