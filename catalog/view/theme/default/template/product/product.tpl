<?php echo $header; ?>
<div class="container-block">
    <div class="container-inner">
        <div class="row">
            <div class="col-xs-12">
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="row"><?php //echo $column_left; 
                            ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="col-sm-12"><?php echo $content_top; ?>
                <div class="row">
                    <?php if ($column_left || $column_right) { ?>
                        <?php $class = 'col-sm-3'; ?>
                    <?php } else { ?>
                        <?php $class = 'col-sm-8'; ?>
                    <?php } ?>
                    <div class="col-sm-6">
                        <?php if ($thumb || $images) { ?>
                            <ul class="thumbnails">
                                <?php if ($thumb) { ?>
                                    <li><a class="thumbnail_product" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
                                <?php } ?>
                                <?php if ($images) { ?>
                                    <?php foreach ($images as $image) { ?>
                                        <li class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </div>
                    <?php if ($column_left || $column_right) { ?>
                        <?php $class = 'col-sm-6'; ?>
                    <?php } else { ?>
                        <?php $class = 'col-sm-4'; ?>
                    <?php } ?>
                    <div class="col-sm-6">
                        <h1 class="product-title"><?php echo $heading_title; ?></h1>
                        <div class="description"><?php echo $description; ?></div>
                        <div class="float-none"></div>
                        <!-- <div class="tissue">
                            <a class="tissue__block tissue__block--first" style="background-image: url('img/tkani/Thomas/thomas01.JPG')">
                                <div class="tissue__information">
                                    <div class="tissue__name">Thomas 01</div>
                                    <div class="tissue__description">
                                        <p>Martindale 40000</p>
                                        <p>Полиэстер 92%, NYL 8%</p>
                                        <p>Цветостойкость 4...5</p>
                                    </div>
                                </div>
                            </a>
                            <a class="tissue__block" style="background-image: url('img/tkani/Norn/norn 18.jpg')">
                                <div class="tissue__information">
                                    <div class="tissue__name">Norn 18</div>
                                </div>
                            </a>
                            <a class="tissue__block" style="background-image: url('img/tkani/Jasmine/jasmine_45_sq.jpg')">
                                <div class="tissue__information">
                                    <div class="tissue__name">Jasmine 45</div>
                                    <div class="tissue__description">
                                        <p>Martindale 100000</p>
                                        <p>Полиэстер 100%</p>
                                        <p>Цветостойкость 5</p>
                                    </div>
                                </div>
                            </a>
                            <a class="tissue__block" style="background-image: url('img/tkani/Sawana/sawana 72.jpg')">
                                <div class="tissue__information">
                                    <div class="tissue__name">Sawana 72</div>
                                    <div class="tissue__description">
                                        <p>Martindale 100000</p>
                                        <p>Полиэстер 100%</p>
                                        <p>Цветостойкость 4</p>
                                    </div>
                                </div>
                            </a>
                            <a class="tissue__block tissue__block--last" style="background-image: url('img/tkani/Yuca/yuca 11.jpg')">
                                <div class="tissue__information">
                                    <div class="tissue__name">Yuca 11</div>
                                </div>
                            </a>
                            <div class="tissue__wrapper">
                                <a class="tissue__all" href="index.php?route=information/information&information_id=42">Все ткани</a>
                            </div>
                        </div> -->

                        <div class="mechanism">
                            <!-- <img src="img/mechanism.png" alt="Механизм">
                            <p>Механизма нет</p> -->
                        </div>

                        <ul class="nav nav-tabs">
                            <?php if ($attribute_groups) { ?>
                                <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                            <?php } ?>
                            <?php if ($downloads) { ?>
                                <li><a href="#tab-documentation" data-toggle="tab"><?php echo $tab_documentation; ?></a></li>
                            <?php } ?>
                            <?php if ($tabstock) { ?>
                                <li><a href="#tab-stock" data-toggle="tab">Наличие в магазинах</a></li>
                            <?php } ?>
                            <?php if ($review_status) { ?>
                                <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php if ($attribute_groups) { ?>
                                <div class="tab-pane" id="tab-specification">
                                    <table class="table table-bordered">
                                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                                            <thead>
                                                <tr>
                                                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                    <tr>
                                                        <td><?php echo $attribute['name']; ?></td>
                                                        <td><?php echo $attribute['text']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        <?php } ?>
                                    </table>
                                </div>
                            <?php } ?>
                            <div class="tab-pane tab-content" id="tab-stock">
                                <div id="shop_spb">
                                    <ol class="rounded69">
                                        <li><a>Санкт-Петербург, ул. Кантемировская, д. 37, МЦ "Мебель Сити 2"</a><span><?php echo $spb1; ?></span></li>
                                        <li><a>Санкт-Петербург, ул. Большая Пушкарская, д. 60</a><span><?php echo $spb2; ?></span></li>
                                        <li><a>Санкт-Петербург, ул. Полевая Сабировская, д. 54 А, ТВК "Интерио"</a><span><?php echo $spb3; ?></span></li>
                                        <li><a>Санкт-Петербург, ул. Шостаковича, д. 8/1 А, МЦ "Гранд Каньон"</a><span><?php echo $spb4; ?></span></li>
                                        <li><a>Санкт-Петербург, ул. Варшавская, д. 3, корп 3, МЦ "Мебельный Континент"</a><span><?php echo $spb5; ?></span></li>
                                        <li><a>Нижний Новгород, ул. Ларина, д. 7/3, ТЦ "Открытый Материк"</a><span><?php echo $spb6; ?></span></li>
                                        <li><a>Москва, ЮАО, ул. Ленинская Слобода, д. 26</a><span><?php echo $spb7; ?></span></li>
                                        <li><a>Москва, 1-й Щипковский переулок, д. 4</a><span><?php echo $spb8; ?></span></li>
                                        <li><a>Химки, ул. Бутакова, д. 4, МЦ "Гранд 2"</a><span><?php echo $spb9; ?></span></li>
                                        <li><a>Белгород, ул. Белгородского полка, д. 62</a><span><?php echo $spb10; ?></span></li>
                                        <li><a>Барнаул, пр. Космонавтов, д. 6 Г, ТВК "Республика"</a><span><?php echo $spb11; ?></span></li>
                                        <li><a>Великий Новгород, ул. Большая Московская, д. 59</a><span><?php echo $spb12; ?></span></li>
                                        <li><a>Волгоград, пр. Ленина, д. 65 К, ТЦ "Стройград"</a><span><?php echo $spb13; ?></span></li>
                                        <li><a>Вологда, Окружное шоссе, д. 11А, ТЦ "Мебель Маркет"</a><span><?php echo $spb14; ?></span></li>
                                        <li><a>Геленджик, ул. Киевская, д. 45Б</a><span><?php echo $spb15; ?></span></li>
                                        <li><a>Екатеринбург, МЦ Галерея 11, 5 этаж, ул. Студенческая, д. 11</a><span><?php echo $spb16; ?></span></li>
                                        <li><a>Иваново, ул. Красногвардейская д. 13/7</a><span><?php echo $spb17; ?></span></li>
                                        <li><a>Ижевск, ул. Пушкинская, д. 116, ТЦ "Пушкинская 116"</a><span><?php echo $spb18; ?></span></li>
                                        <li><a>Иркутск, ул. Партизанская, д. 63</a><span><?php echo $spb19; ?></span></li>
                                        <li><a>Кемерово, ул. Терешковой, д. 41/6 ТВК «Сити Дом»</a><span><?php echo $spb20; ?></span></li>
                                        <li><a>Красноярск, ул. Авиаторов, д. 41</a><span><?php echo $spb21; ?></span></li>
                                        <li><a>Курск, ул. Кати Зеленко, д. 26</a><span><?php echo $spb22; ?></span></li>
                                        <li><a>Мурманск, ул. Скальная, д. 4</a><span><?php echo $spb23; ?></span></li>
                                        <li><a>Мурманск, ул. Воровского, д. 17</a><span><?php echo $spb24; ?></span></li>
                                        <li><a>Нижний Тагил, ул. Октябрьской революции, д. 66, ЦУМ</a><span><?php echo $spb25; ?></span></li>
                                        <li><a>Новороссийск, ул. Пионерская, д. 23 А, МЦ "Дриада"</a><span><?php echo $spb26; ?></span></li>
                                        <li><a>Новосибирск, ул. Светлановская, д. 50, ТВК "Большая Медведица"</a><span><?php echo $spb27; ?></span></li>
                                        <li><a>Новый Уренгой, ул. Промысловая, д. 2, ТВЗ "Опилки"</a><span><?php echo $spb28; ?></span></li>
                                        <li><a>Орел, ул. 8 марта, д. 8</a><span><?php echo $spb29; ?></span></li>
                                        <li><a>Первоуральск, ул. Данилова, д. 2-а</a><span><?php echo $spb30; ?></span></li>
                                        <li><a>Пермь, ул. Чернышевского, д. 28, Галерея Мебели "АртЭго"</a><span><?php echo $spb31; ?></span></li>
                                        <li><a>Самара, Московское шоссе 16 км, здание 1 В, корпус 2</a><span><?php echo $spb32; ?></span></li>
                                        <li><a>Саратов, ул. Большая Горная, д. 336, ТЦ "Viva Decor"</a><span><?php echo $spb33; ?></span></li>
                                        <li><a>Сочи, ТЦ "Эксперт Хоум", ул. Виноградная, д. 55/1</a><span><?php echo $spb34; ?></span></li>
                                        <li><a>Старый Оскол, мкр. Восточный, д. 13, Мебельный салон Нефертити</a><span><?php echo $spb35; ?></span></li>
                                        <li><a>Сургут, пр. Ленина, д. 23</a><span><?php echo $spb36; ?></span></li>
                                        <li><a>Уфа, ул. Менделеева, д. 158, ВДНХ ЭКСПО</a><span><?php echo $spb37; ?></span></li>
                                        <li><a>Хабаровск, Восточное шоссе, д. 16, Авто-сити</a><span><?php echo $spb38; ?></span></li>
                                        <li><a>Чебоксары, ул. Пирогова, д. 1 корпус 1, Салон Финской мебели "Pohjanmaan"</a><span><?php echo $spb39; ?></span></li>
                                        <li><a>Челябинск, МЦ "На Чичерина", ул. Чичерина, д. 22, секция 45</a><span><?php echo $spb40; ?></span></li>
                                        <li><a>Череповец, ул. К.Белова, д. 36 , "Салон финской мебели "Pohjanmaan"</a><span><?php echo $spb41; ?></span></li>
                                        <li><a>Южно-Сахалинск, ул. Дзержинского, д. 40/1</a><span><?php echo $spb42; ?></span></li>
                                    </ol>
                                </div>
                            </div>
                            <div class="tab-pane tab-content" id="tab-documentation">
                                <?php if ($downloads) { ?>
                                    <ul style="list-style:none;">
                                        <?php foreach ($downloads as $download) { ?>
                                            <li><i class="<?php echo $download['icon']; ?>"></i> <a href="<?php echo $download['href']; ?>" title="<?php echo $download['name']; ?>" target="_blank"><?php echo $download['name']; ?><?php echo ($download['size']) ? " (" . $download['size'] . ")" : ''; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </div>

                            <?php if ($review_status) { ?>
                                <div class="tab-pane" id="tab-review">
                                    <form class="form-horizontal" id="form-review">
                                        <div id="review"></div>
                                        <h2><?php echo $text_write; ?></h2>
                                        <?php if ($review_guest) { ?>
                                            <div class="form-group required">
                                                <div class="col-sm-12">
                                                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                                    <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <div class="col-sm-12">
                                                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                                                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                                                    <div class="help-block"><?php echo $text_note; ?></div>
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <div class="col-sm-12">
                                                    <label class="control-label"><?php echo $entry_rating; ?></label>
                                                    &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                                                    <input type="radio" name="rating" value="1" />
                                                    &nbsp;
                                                    <input type="radio" name="rating" value="2" />
                                                    &nbsp;
                                                    <input type="radio" name="rating" value="3" />
                                                    &nbsp;
                                                    <input type="radio" name="rating" value="4" />
                                                    &nbsp;
                                                    <input type="radio" name="rating" value="5" />
                                                    &nbsp;<?php echo $entry_good; ?></div>
                                            </div>
                                            <?php echo $captcha; ?>
                                            <div class="buttons clearfix">
                                                <div class="pull-right">
                                                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <?php echo $text_login; ?>
                                        <?php } ?>
                                    </form>
                                </div>
                            <?php } ?>
                        </div>



                        <ul class="list-unstyled">
                            <?php //if ($manufacturer) { 
                            ?>
                            <!-- <li><?php //echo $text_manufacturer; 
                                        ?> <a class="red69" href="<?php //echo $manufacturers; 
                                                                    ?>"><?php //echo $manufacturer; 
                                                                        ?></a></li> -->
                            <?php //} 
                            ?>
                            <!-- <?php if ($model) { ?>
                <li><?php echo $text_model; ?> <?php echo $model; ?></li>
                <?php } ?> -->
                            <?php if ($reward) { ?>
                                <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
                            <?php } ?>
                            <li>
                                <div class="product-stock"><?php echo $text_stock; ?> <span class="stock-span"><?php echo $stock; ?></span></div>
                            </li>
                        </ul>
                        <?php if ($price) { ?>
                            <ul class="list-unstyled">
                                <?php if (!$special) { ?>
                                    <li>
                                        <div class="product-cost"><?php echo $text_price; ?> <span class="price-numbers"><?php echo $price; ?></span></div>
                                    </li>
                                <?php } else { ?>
                                    <li><span style="text-decoration: line-through;"><?php echo $price; ?></span></li>
                                    <li>
                                        <h2><?php echo $special; ?></h2>
                                    </li>
                                <?php } ?>
                                <?php //if ($tax) { 
                                ?>
                                <!-- <li><?php //echo $text_tax; 
                                            ?> <?php //echo $tax; 
                                                ?></li> -->
                                <?php //} 
                                ?>
                                <?php //if ($points) { 
                                ?>
                                <!-- <li><?php //echo $text_points; 
                                            ?> <?php //echo $points; 
                                                ?></li> -->
                                <?php //} 
                                ?>
                                <?php if ($discounts) { ?>
                                    <li>
                                        <hr>
                                    </li>
                                    <?php foreach ($discounts as $discount) { ?>
                                        <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                        <!-- <div class="btn-group">
                <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i></button>
                <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i></button>
            </div> -->
                        <div id="product">
                            <?php if ($options) { ?>
                                <hr>
                                <h3><?php echo $text_option; ?></h3>
                                <?php foreach ($options as $option) { ?>
                                    <?php if ($option['type'] == 'select') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'radio') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                            <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'checkbox') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                            <?php if ($option_value['image']) { ?>
                                                                <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                                                            <?php } ?>
                                                            <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'image') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'text') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'textarea') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'file') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                            <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'date') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group date">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'datetime') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group datetime">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                </span></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'time') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group time">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                </span></div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($recurrings) { ?>
                                <hr>
                                <h3><?php echo $text_payment_recurring; ?></h3>
                                <div class="form-group required">
                                    <select name="recurring_id" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($recurrings as $recurring) { ?>
                                            <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="help-block" id="recurring-description"></div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                <br />
                                <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
                                <a id="button-buyinoneclick" data-toggle="modal" data-target="#buyinoneclick-modal"><?php echo $button_buyinoneclick; ?></a>
                            </div>

                            <div class="modal fade" tabindex="-1" role="dialog" id="buyinoneclick-modal">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button>
                                            <h3 class="modal-title">Купить в один клик</h3>
                                        </div>
                                        <div class="modal-body">
                                            <form class="buyinoneclick-form">
                                                <input type="text" placeholder="Имя" class="form-control" id="buyinoneclick-name">
                                                <input type="text" placeholder="Телефон" class="form-control" id="buyinoneclick-phone">
                                                <input type="hidden" value="<?php echo $heading_title; ?>" id="buyinoneclick-product">
                                                <input type="hidden" value="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" id="buyinoneclick-link">
                                                <input type="hidden" value="<?php echo $price; ?>" id="buyinoneclick-price">
                                            </form>
                                            <p class="buyinoneclick-result"></p>
                                        </div>
                                        <div class="modal-footer modal-privacy">
                                            <p>Нажимая на кнопку "Отправить" Вы даёте <a href="files/ur_info/soglashenie.pdf" target="_blank">согласие на обработку персональных данных</a>.</p>
                                            <button type="button" class="btn btn-primary" id="buyinoneclick-submit">Отправить</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->

                            <?php if ($minimum > 1) { ?>
                                <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                            <?php } ?>

                        </div>

                        <?php if ($review_status) { ?>
                            <!-- <div class="rating">
                <p>
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($rating < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } ?>
                <?php } ?>
                <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a></p> -->
                            <hr>
                            <!-- AddThis Button BEGIN -->
                            <!-- <div class="addthis_toolbox addthis_default_style" data-url="<?php echo $share; ?>"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> -->
                            <!-- AddThis Button END -->
                            <!-- </div> -->
                        <?php } ?>
                        <!----CUSTOM BLOCK---------->



                        <?php
                        $actual_link = 'http://' . $_SERVER['HTTP_HOST'];
                        ?>

                    </div>



                </div>

                <!-- Small modal -->


                <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel_1">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>

                                <h4 class="modal-title" id="mySmallModalLabel_1">Способы оплаты интернет заказа:</h4>

                            </div>
                            <div class="modal-body">
                                <ul>
                                    <li>банковской картой или наличными средствами в любом из магазинов розничной сети ВестаТрейдинг</li>
                                    <li>банковским переводом, распечатав или заполнив самостоятельно квитанцию, уточнив сумму и реквизиты у менеджеров компании</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if ($products) { ?>
                    <h3><?php echo $text_related; ?></h3>
                    <div class="row">
                        <?php $i = 0; ?>
                        <?php foreach ($products as $product) { ?>
                            <?php if ($column_left && $column_right) { ?>
                                <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
                            <?php } elseif ($column_left || $column_right) { ?>
                                <?php $class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
                            <?php } else { ?>
                                <?php $class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
                            <?php } ?>
                            <div class="<?php echo $class; ?>">
                                <div class="product-thumb transition">
                                    <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                                    <div class="caption">
                                        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                        <p><?php echo $product['description']; ?></p>
                                        <?php if ($product['rating']) { ?>
                                            <div class="rating">
                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                    <?php if ($product['rating'] < $i) { ?>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    <?php } else { ?>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($product['price']) { ?>
                                            <p class="price">
                                                <?php if (!$product['special']) { ?>
                                                    <?php echo $product['price']; ?>
                                                <?php } else { ?>
                                                    <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                                <?php } ?>
                                                <?php if ($product['tax']) { ?>
                                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                <?php } ?>
                                            </p>
                                        <?php } ?>
                                    </div>
                                    <div class="button-group" style="background: #5F3241;">
                                        <!--<button class="button__cart__but" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', $(this).parent().parent().find('.quantity_input').val())"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>-->
                                        <button class="add-to-cart-button" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', 1)"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                                        <!-- <input type="number" name="quantity" value="1" size="2" min="1" id="input-quantity" class="quantity_input p_q_<?php echo $product_id; ?>" />  -->
                                        <!--<button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                    <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>-->
                                    </div>
                                    <!-- <div class="button-group">
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span> <i class="fa fa-shopping-cart"></i></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                </div> -->
                                </div>
                            </div>
                            <?php if (($column_left && $column_right) && ($i % 2 == 0)) { ?>
                                <div class="clearfix visible-md visible-sm"></div>
                            <?php } elseif (($column_left || $column_right) && ($i % 3 == 0)) { ?>
                                <div class="clearfix visible-md"></div>
                            <?php } elseif ($i % 4 == 0) { ?>
                                <div class="clearfix visible-md"></div>
                            <?php } ?>
                            <?php $i++; ?>
                        <?php } ?>
                    </div>
                <?php } ?>

                <?php //if ($tags) { 
                ?>
                <!-- <p><?php //echo $text_tags; 
                        ?>
            <?php //for ($i = 0; $i < count($tags); $i++) { 
            ?>
            <?php //if ($i < (count($tags) - 1)) { 
            ?>
            <a href="<?php //echo $tags[$i]['href']; 
                        ?>"><?php //echo $tags[$i]['tag']; 
                            ?></a>,
            <?php //} else { 
            ?>
            <a href="<?php //echo $tags[$i]['href']; 
                        ?>"><?php //echo $tags[$i]['tag']; 
                            ?></a>
            <?php //} 
            ?>
            <?php //} 
            ?>
        </p> -->
                <?php //} 
                ?>
                <?php //echo $content_bottom; 
                ?>
            </div>
            <?php //echo $column_right; 
            ?></div>

        <span style="color: #5F3241;">&nbsp&nbsp* цена модели в обивке 1 категории, без учета стоимости доставки</span><br />

    </div>
</div>

<script type="text/javascript">
    <!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function() {
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function() {
                $('#recurring-description').html('');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //
    -->
</script>
<script type="text/javascript">
    <!--
    $('#button-cart').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-cart').button('loading');
            },
            complete: function() {
                $('#button-cart').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('.add-to-cart-notification').remove();
                    $('<div class="add-to-cart-notification">' + json['success'] + '<span class="notification-close">&times;</span></div>').appendTo('body').hide().fadeIn(300).delay(5000).fadeOut(300, function() {
                        $(this).remove();
                    });

                    $('.cart-block .cart-button img').attr('src', 'img/icons/shopping-cart-items.png');

                    $('#cart > button').html('<div class="cart-text"><div class="cart-name">Корзина</div><div id="cart-total">' + json['total'] + '</div></div>');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //
    -->
</script>

<script type="text/javascript">
    <!--
    $('#review').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function() {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function() {
                $('#button-review').button('loading');
            },
            complete: function() {
                $('#button-review').button('reset');
            },
            success: function(json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });

    $(document).ready(function() {
        $('.thumbnails').magnificPopup({
            type: 'image',
            delegate: 'a',
            gallery: {
                enabled: true
            }
        });
    });
    //
    -->
</script>

<?php echo $footer; ?>