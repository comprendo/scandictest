<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
    <div class="container-block">
        <div class="container-inner">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb">
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                        <?php } ?>
                    </ul>
                    <h1><?php echo $heading_title; ?></h1>
                </div>

                <div class="hidden-xs">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Способы доставки</h2>
                            <p><i class="fa fa-car fa-3x article-icon" aria-hidden="true"></i><br></p>
                            <h3 class="article-h3">Самовывоз</h3>
                            <p></p>
                            <p>Адрес самовывоза в вашем регионе: <?php echo $address; ?></p>
                        </div>
                        <div class="col-sm-6">
                            <h2>Способы оплаты</h2>
                            <p><i class="fa fa-money fa-3x article-icon" aria-hidden="true"></i><br></p>
                            <h3 class="article-h3">Банковский перевод</h3>
                            <p></p>
                            <p>Для осуществления платежа банковским переводом менеджер выставит счет с выбранными товарами и вышлет на Вашу электронную почту. Данный способ оплаты по банковским реквизитам предусмотрен как для физических, так и для юридических лиц.</p>
                            <!-- <p><a class="article-link" href="files/ur_info/rekviziti_ooo_skandinavia.doc">Загрузить реквизиты</a>
      </p> -->
                        </div>
                    </div>

                    <!-------------->

                    <div class="row">
                        <!--<p>Онлайн-оплата</p><p>Для удобной оплаты банковской картой используется система электронных платежей ROBOKASSA. При оформлении заказа выберите способ оплаты Онлайн-оплата (ROBOKASSA). После оформления заказа с Вами свяжется менеджер для подтверждения заказа и вышлет на Вашу почту ссылку для оплаты заказа.</p>-->
                        <div class="col-sm-6">
                            <p><i class="fa fa-truck fa-3x article-icon" aria-hidden="true"></i><br></p>
                            <h3 class="article-h3">Доставка</h3>
                            <p></p>
                            <p>Доступна доставка во все города России.<br>Стоимость доставки рассчитывается индивидуально.</p>
                        </div>
                    </div>
                </div>


                <div class="visible-xs-block">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2>Способы доставки</h2>
                            <p><i class="fa fa-car fa-3x article-icon" aria-hidden="true"></i><br></p>
                            <h3 class="article-h3">Самовывоз</h3>
                            <p></p>
                            <p>Адрес самовывоза в вашем регионе: <?php echo $address; ?></p>
                        </div>
                        <div class="col-xs-12">
                            <p><i class="fa fa-truck fa-3x article-icon" aria-hidden="true"></i><br></p>
                            <h3 class="article-h3">Доставка</h3>
                            <p></p>
                            <p>Доступна доставка во все города России.<br>Стоимость доставки рассчитывается индивидуально.</p>
                        </div>
                    </div>

                    <!-------------->

                    <div class="row">
                        <!--<p>Онлайн-оплата</p><p>Для удобной оплаты банковской картой используется система электронных платежей ROBOKASSA. При оформлении заказа выберите способ оплаты Онлайн-оплата (ROBOKASSA). После оформления заказа с Вами свяжется менеджер для подтверждения заказа и вышлет на Вашу почту ссылку для оплаты заказа.</p>-->
                        <div class="col-xs-12">
                            <h2>Способы оплаты</h2>
                            <p><i class="fa fa-money fa-3x article-icon" aria-hidden="true"></i><br></p>
                            <h3 class="article-h3">Банковский перевод</h3>
                            <p></p>
                            <p>Для осуществления платежа банковским переводом менеджер выставит счет с выбранными товарами и вышлет на Вашу электронную почту. Данный способ оплаты по банковским реквизитам предусмотрен как для физических, так и для юридических лиц.</p>
                            <p><a class="article-link" href="files/ur_info/rekviziti_ooo_skandinavia.doc">Загрузить реквизиты</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p>Задать вопрос: <a class="article-link" href="mailto:scandicstyle@yandex.ru">scandicstyle@yandex.ru</a>, <a class="article-link" href="tel:+79217574256">+7 (921) 757-42-56</a></p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>