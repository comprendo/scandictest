<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
    <div class="container-block">
        <div class="container-inner">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb">
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h1><?php echo $heading_title; ?></h1>
                    <div class="sitemap-info">
                        <div class="left">
                            <ul>
                                <?php foreach ($categories as $category_1) { ?>
                                    <li><a href="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?></a>
                                        <?php if ($category_1['children']) { ?>
                                            <ul>
                                                <?php foreach ($category_1['children'] as $category_2) { ?>
                                                    <li><a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
                                                        <?php if ($category_2['children']) { ?>
                                                            <ul>
                                                                <?php foreach ($category_2['children'] as $category_3) { ?>
                                                                    <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="right">
                            <ul>
                                <li><?php echo $text_information; ?>
                                    <ul>
                                        <?php foreach ($informations as $information) { ?>
                                            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?>
        </div>
    </div>
</div>
<?php echo $footer; ?>