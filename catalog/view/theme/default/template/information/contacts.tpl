<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
    <div class="container-block">
        <div class="container-inner">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb">
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                        <?php } ?>
                    </ul>
                    <h1><?php echo $heading_title; ?></h1>
                    <p>Адрес: <?php echo str_replace('\n', ', ', $address); ?> <?php if ($additional_contacts) : ?><span>(<a class="scandic-link" data-toggle="modal" data-target="#additional-contacts-modal">другие адреса</a>)</span><?php endif; ?></p>

                    <p>Телефон: <?php echo $telephone; ?></p>

                    <p>Электронная почта: <?php echo $email; ?></p>

                    <div class="map" style="height: 500px;">
                        <?php echo $map; ?>
                    </div>

                    <p>Реквизиты:</p>

                    <p>ООО "Мебель Скандинавии"<br>
                        Юридический и фактический адрес: 197101, г. Санкт-Петербург, ул. Большая Пушкарская, д.60, лит. А, пом.12Н</p>

                    <p>ИНН 7813289127<br>
                        КПП 781301001</p>

                    <p>ОГРН 1177847327680<br>
                        ОКПО 19757266<br>
                        ОКТМО 40390000000<br>
                        ОКАТО 40288000000<br>
                        ОКОГУ 4210014<br>
                        ОКОПФ 12300<br>
                        ОКВЭД 47.19</p>

                    <p>Банковские реквизиты:<br>
                        р/с 40702810280050001009<br>
                        к/с 30101810200000000704<br>
                        Банк: Ф. ОПЕРУ Банка ВТБ (ПАО) в г. Санкт-Петербурге г. Санкт-Петербург<br>
                        БИК 044030704</p>
                </div>
            </div>
        </div>
    </div>
    <?php echo $content_bottom; ?>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="additional-contacts-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Другие адреса</h3>
            </div>
            <div class="modal-body">
                <div class="modal-contact-wrapper">
                    <?php foreach ($additional_contacts as $additional_contact) { ?>
                        <div class="modal-contact-card">
                            <div class="modal-contact-card-content">
                                <p class="name"><?php echo $additional_contact['name']; ?></p>
                                <p class="work-time"><i class="fa fa-calendar-o" aria-hidden="true"></i> <?php echo $additional_contact['work_time']; ?></p>
                                <p class="email"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $additional_contact['email']; ?></p>
                                <p class="address"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $additional_contact['address']; ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php echo $map_script; ?>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php echo $footer; ?>