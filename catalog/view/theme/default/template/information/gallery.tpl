<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
    <div class="container-block">
        <div class="container-inner">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb">
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                        <?php } ?>
                    </ul>
                    <h1><?php echo $heading_title; ?></h1>
                </div>
            </div>
        </div>
    </div>
    <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>